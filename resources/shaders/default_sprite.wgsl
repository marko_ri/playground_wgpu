
struct Camera {
 view_proj: mat4x4<f32>,
};
@group(0) @binding(0)
var<uniform> camera: Camera;

@group(1) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(1) @binding(1)
var s_diffuse: sampler;

struct UniformBag {
 buf1: vec4<f32>,
 buf2: vec4<f32>,
};
@group(2) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
  @location(2) tex_coords: vec2<f32>,
};

struct VertexOutput {
  @builtin(position) clip_position: vec4<f32>,
  @location(0) tex_coords: vec2<f32>,
};

@vertex
fn vs_main(@builtin(vertex_index) vert_idx: u32, in: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.tex_coords = in.tex_coords + vec2f(u_bag.buf1[vert_idx], u_bag.buf2[vert_idx]);
  out.clip_position = camera.view_proj * vec4<f32>(in.position, 1.0);
  return out;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  return textureSample(t_diffuse, s_diffuse, in.tex_coords);
}
