// ArtOfCode - Coding a Bezier curve from scratch: https://www.youtube.com/watch?v=a4zMX6dDVXI

struct Camera {
 view_proj: mat4x4<f32>,
};
@group(0) @binding(0)
var<uniform> camera: Camera;

struct UniformBag {
 win_width: f32,
 win_height: f32,
 dt: f32,
 mouse_circle_x: f32,
 mouse_circle_y: f32,
 padding1: f32,
 padding2: f32,
 padding3: f32,
};
@group(1) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
  @location(1) color: vec4<f32>,
};

struct VertexOutput {
  @builtin(position) clip_position: vec4<f32>,
  @location(0) color: vec4<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.color = model.color;
  out.clip_position = camera.view_proj * vec4<f32>(model.position, 1.0);
  return out;
}

fn circle(p: vec2f, c: vec2f) -> f32 {
  let d = length(p - c);
  return smoothstep(fwidth(d), 0.0, d - 0.02);
}

fn line(p: vec2f, a: vec2f, b: vec2f) -> f32 {
  let pa = p - a;
  let ba = b - a;
  let t = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
  let c = a + ba * t;
  let d = length(c - p);
  return smoothstep(fwidth(d), 0.0, d - 0.001);
}

fn bezier(a: vec2f, b: vec2f, c: vec2f, t: f32) -> vec2f {
  return mix(mix(a, c, t), mix(c, b, t), t);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  var uv = vec2f(in.clip_position.x / u_bag.win_width, 1.0 - in.clip_position.y / u_bag.win_height);

  var t = sin(u_bag.dt) * 0.5 + 0.5;
  var col = vec3f(0.0);
  let a = vec2f(0.6, 0.2);
  let b = vec2f(0.8, 0.6);
  let c = vec2f(u_bag.mouse_circle_x, u_bag.mouse_circle_y);
  let ac = mix(a, c, t);
  let cb = mix(c, b, t);
  let acb = mix(ac, cb, t);

  col += vec3f(1.0, 0.0, 0.0) * circle(uv, a);
  col += vec3f(0.0, 0.0, 1.0) * circle(uv, b);
  col += vec3f(0.0, 1.0, 0.0) * circle(uv, c);
  //col += vec3f(1.0, 1.0, 0.0) * circle(uv, ac);
  //col += vec3f(0.0, 1.0, 1.0) * circle(uv, cb);
  col += vec3f(1.0, 1.0, 1.0) * circle(uv, acb);

  //col += line(uv, a, c) + line(uv, c, b);
  //col += line(uv, ac, cb);

  let num_segs = 25;
  var p: vec2f;
  var pp = a;
  for (var i = 1; i <= num_segs; i += 1) {
    t = f32(i) / f32(num_segs);
    p = bezier(a, b, c, t);
    col += line(uv, p, pp);
    pp = p;
  }

  return vec4f(col, 1.0) + in.color;
}
