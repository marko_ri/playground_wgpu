
struct Camera {
 view_proj: mat4x4<f32>,
};
@group(0) @binding(0)
var<uniform> camera: Camera;

struct UniformBag {
 win_width: f32,
 win_height: f32,
 ray_start_x: f32,
 ray_start_y: f32,
 ray_end_x: f32,
 ray_end_y: f32,
 intersection_x: f32,
 intersection_y: f32,
};
@group(1) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
};

struct VertexOutput {
  @builtin(position) uv: vec4<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.uv = camera.view_proj * vec4<f32>(model.position, 1.0);
  return out;
}

fn circle(p: vec2f, c: vec2f, radius: f32) -> f32 {
  let d = length(p - c);
  return smoothstep(fwidth(d), 0.0, d - radius);
}

fn line(p: vec2f, a: vec2f, b: vec2f) -> f32 {
  let pa = p - a;
  let ba = b - a;
  let t = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
  let c = a + ba * t;
  let d = length(c - p);
  return smoothstep(fwidth(d), 0.0, d - 0.001);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  let uv = vec2f(in.uv.x / u_bag.win_width,
                 1.0 - in.uv.y / u_bag.win_height);

  var result = vec4f(0.0);
  let radius = 0.02;

  let ray_start_center = vec2f(u_bag.ray_start_x, u_bag.ray_start_y);
  let ray_start = circle(uv, ray_start_center, radius);
  result += ray_start * vec4f(1.0, 0.0, 0.0, 1.0);

  if u_bag.ray_end_x >= 0.0 {
      let ray_end_center = vec2f(u_bag.ray_end_x, u_bag.ray_end_y);
      let ray_end = circle(uv, ray_end_center, radius);
      result += ray_end * vec4f(0.11, 0.27, 0.91, 1.0);

      let ray = line(uv, ray_start_center, ray_end_center);
      result += ray *  vec4f(0.0, 1.0, 1.0, 1.0);
    }

  if u_bag.intersection_x >= 0.0 {
      let intersection =
        circle(uv, vec2f(u_bag.intersection_x, u_bag.intersection_y), radius);
      result += intersection * vec4f(1.0, 0.0, 1.0, 1.0);
    }

  return result;
}
