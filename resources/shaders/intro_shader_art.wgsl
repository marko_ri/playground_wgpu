// An introduction to Shader Art Coding: https://www.youtube.com/watch?v=f4s1h2YETNY

struct UniformBag {
 win_width: f32,
 win_height: f32,
 dt: f32,
 padding: f32,
};
@group(0) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
};

struct VertexOutput {
  @builtin(position) clip_position: vec4<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.clip_position = vec4<f32>(model.position, 1.0);
  return out;
}

fn palette(t: f32) -> vec3f {
  let a = vec3f(0.5, 0.5, 0.5);
  let b = vec3f(0.5, 0.5, 0.5);
  let c = vec3f(1.0, 1.0, 1.0);
  let d = vec3f(0.263, 0.416, 0.557);

  return a + b * cos(6.28318 * (c * t + d));
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  // y goes from top to bottom
  var uv = (in.clip_position.xy * 2.0 - vec2f(u_bag.win_width, u_bag.win_height)) / u_bag.win_height;
  let uv0 = uv;
  var final_color = vec3f(0.0);

  for (var i = 0.0; i < 4.0; i += 1.0) {
    uv = fract(uv * 1.5) - 0.5;

    var d = length(uv) * exp(-length(uv0));

    var col = palette(length(uv0) + i * 0.4 + u_bag.dt * 0.4);

    d = sin(d * 8.0 + u_bag.dt) / 8.0;
    d = abs(d);
    d = pow(0.01 / d, 2.0);

    final_color += col * d;
  }

  return vec4f(final_color, 1.0);
}
