struct Camera {
 view_proj_matrix: mat4x4<f32>,
 camera_pos: vec4<f32>,
};

struct UniformBag {
    model_matrix: mat4x4<f32>,
    elapsed_time: f32,
}

struct WaveParameters {
    length: f32,
    amplitude: f32,
    steepness: f32,    // Steepness of the peak of the wave. 0 <= S <= 1
    padding1: f32,
    @size(16) @align(8) direction: vec2<f32>,  // Normalized direction of the wave
}

struct UniformWaves {
    waves: array<WaveParameters, 5>,
    amplitude_sum: f32  // Sum of waves amplitudes
}

struct VertexOutput {
    @builtin(position) position: vec4<f32>,
    @location(0) normal: vec4<f32>,
    @location(1) uv: vec2<f32>,
    @location(2) world_pos: vec4<f32>
}

@group(0) @binding(0) var<uniform> camera: Camera;

@group(1) @binding(0) var sea_color: texture_2d<f32>;
@group(1) @binding(1) var sea_sampler: sampler;

@group(2) @binding(0) var<uniform> u_bag: UniformBag;
@group(2) @binding(1) var<uniform> u_waves: UniformWaves;

const pi = 3.14159;
const gravity = 9.8;
const wave_count = 5;

@vertex
fn vs_main(
    @location(0) position: vec3<f32>,
    // @location(1) normal: vec3<f32>,
    @location(2) uv: vec2<f32>,
) -> VertexOutput {
    var output: VertexOutput;
    var world_pos: vec4<f32> = u_bag.model_matrix * vec4<f32>(position, 1.0);

    var waves_sum: vec3<f32> = vec3<f32>(0.0);
    var waves_sum_normal: vec3<f32>;
    for(var i: i32 = 0; i < wave_count; i = i + 1) {
      var wave = u_waves.waves[i];
      var wave_vector_magnitude = 2.0 * pi / wave.length;
      var wave_vector = wave.direction * wave_vector_magnitude;
      var temporal_freq = sqrt(gravity * wave_vector_magnitude);
      var steepness_factor = wave.steepness / (wave.amplitude * wave_vector_magnitude * f32(wave_count));

      var pos = dot(wave_vector, world_pos.xz) - temporal_freq * u_bag.elapsed_time;
      var sin_pos_amplitude_dir = sin(pos) * wave.amplitude * wave.direction;

      var offset: vec3<f32>;
      offset.x = sin_pos_amplitude_dir.x * steepness_factor;
      offset.z = sin_pos_amplitude_dir.y * steepness_factor;
      offset.y = cos(pos) * wave.amplitude;

      var normal: vec3<f32>;
      normal.x = sin_pos_amplitude_dir.x * wave_vector_magnitude;
      normal.z = sin_pos_amplitude_dir.y * wave_vector_magnitude;
      normal.y = cos(pos) * wave.amplitude * wave_vector_magnitude * steepness_factor;

      waves_sum = waves_sum + offset;
      waves_sum_normal = waves_sum_normal + normal;
    }
    waves_sum_normal.y = 1.0 - waves_sum_normal.y;
    waves_sum_normal = normalize(waves_sum_normal);

    world_pos.x = world_pos.x - waves_sum.x;
    world_pos.z = world_pos.z - waves_sum.z;
    world_pos.y = waves_sum.y;

    output.world_pos = world_pos;
    output.position = camera.view_proj_matrix * world_pos;
    output.normal = vec4<f32>(waves_sum_normal, 0.0);
    output.uv = uv;
    return output;
}

@fragment
fn fs_main(
    data: VertexOutput,
) -> @location(0) vec4<f32> {
    let light_color = vec3<f32>(1.0, 0.8, 0.65);
    let sky_color = vec3<f32>(0.69, 0.84, 1.0);

    let light_pos = vec3<f32>(-10.0, 1.0, -10.0);
    var light = normalize(light_pos - data.world_pos.xyz);  // Vector from surface to light
    var eye = normalize(camera.camera_pos.xyz - data.world_pos.xyz);  // Vector from surface to camera
    var reflection = reflect(data.normal.xyz, -eye);  // I - 2.0 * dot(N, I) * N

    var halfway = normalize(eye + light);  // Vector between View and Light
    let shininess = 30.0;
    var specular = clamp(pow(dot(data.normal.xyz, halfway), shininess), 0.0, 1.0) * light_color;  // Blinn-Phong specular component

    var fresnel = clamp(pow(1.0 + dot(-eye, data.normal.xyz), 4.0), 0.0, 1.0);  // Cheap fresnel approximation

    // Normalize height to [0, 1]
    var normalized_height = (data.world_pos.y + u_waves.amplitude_sum) / (2.0 * u_waves.amplitude_sum);
    var underwater = textureSample(sea_color, sea_sampler, vec2<f32>(normalized_height, 0.0)).rgb;

    // Approximating Translucency (GPU Pro 2 article)
    let distortion = 0.1;
    let power = 4.0;
    let scale = 1.0;
    let ambient = 0.2;
    var thickness = smoothstep(0.0, 1.0, normalized_height);
    var distorted_light = light + data.normal.xyz * distortion;
    var translucency_dot = pow(clamp(dot(eye, -distorted_light), 0.0, 1.0), power);
    var translucency = (translucency_dot * scale + ambient) * thickness;
    var underwater_translucency = mix(underwater, light_color, translucency) * translucency;

    var color = mix(underwater + underwater_translucency, sky_color, fresnel) + specular;

    return vec4<f32>(color, 1.0);
}
