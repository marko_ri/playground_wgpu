// boids.wgsl + https://surma.dev/things/webgpu/

@vertex
fn vs_main(
    @location(0) circle_center: vec2<f32>,
    @location(1) circle_velocity: vec2<f32>,
    @location(2) position: vec2<f32>,
) -> @builtin(position) vec4<f32> {
  return vec4<f32>(position + circle_center, 0.0, 1.0);
}

@fragment
fn fs_main() -> @location(0) vec4<f32> {
    return vec4<f32>(1.0, 1.0, 1.0, 1.0);
}

struct Circle {
 center: vec2<f32>,
 velocity: vec2<f32>,
};

const PI: f32 = 3.14159;
const TIME_STEP: f32 = 0.016;

struct Params {
 radius: f32,
};

@group(0) @binding(0) var<uniform> params : Params;
@group(0) @binding(1) var<storage, read> src_circles : array<Circle>;
@group(0) @binding(2) var<storage, read_write> dst_circles : array<Circle>;

@compute @workgroup_size(64)
fn cs_main(
 @builtin(global_invocation_id) global_id: vec3<u32>,
 @builtin(local_invocation_id) local_id: vec3<u32>,
) {
  let index = global_id.x;
  let num_circles = arrayLength(&src_circles);
  if (index >= num_circles) {
    return;
  }

  var src_circle = src_circles[index];
  var dst_circle = Circle(src_circle.center, src_circle.velocity);

  // all circles have the same radius
  let dst_radius = params.radius;
  let src_circle_radius = params.radius;
  let other_circle_radius = params.radius;
  ////////

  // circle/circle collision
  for(var i = 0u; i < num_circles; i = i + 1u) {
    if (i == index) {
      continue;
    }
    var other_circle = src_circles[i];
    let n = src_circle.center - other_circle.center;
    let distance = length(n);
    if(distance >= src_circle_radius + other_circle_radius) {
      continue;
    }
    let overlap = src_circle_radius + other_circle_radius - distance;
    dst_circle.center = src_circle.center + normalize(n) * overlap / 2.0;

    // Details on the physics here:
    // https://physics.stackexchange.com/questions/599278/how-can-i-calculate-the-final-velocities-of-two-spheres-after-an-elastic-collisi
    let src_mass = pow(src_circle_radius, 2.0) * PI;
    let other_mass = pow(other_circle_radius, 2.0) * PI;
    let c = 2.0 * dot(n, (other_circle.velocity - src_circle.velocity)) / (dot(n, n) * (1.0 / src_mass + 1.0 / other_mass));
    dst_circle.velocity = src_circle.velocity + c / src_mass * n;
  }

  // apply velocity
  dst_circle.center = dst_circle.center + dst_circle.velocity * TIME_STEP;

  // circle/wall collision ("wall" is [-1, 1].. could be passed as params)
  if(dst_circle.center.x - dst_radius < -1.0) {
    dst_circle.center.x = -1.0 + dst_radius;
    dst_circle.velocity.x = -dst_circle.velocity.x;
  }
  if(dst_circle.center.y - dst_radius < -1.0) {
    dst_circle.center.y = -1.0 + dst_radius;
    dst_circle.velocity.y = -dst_circle.velocity.y;
  }
  if(dst_circle.center.x + dst_radius >= 1.0) {
    dst_circle.center.x = 1.0 - dst_radius;
    dst_circle.velocity.x = -dst_circle.velocity.x;
  }
  if(dst_circle.center.y + dst_radius >= 1.0) {
    dst_circle.center.y = 1.0 - dst_radius;
    dst_circle.velocity.y = -dst_circle.velocity.y;
  }

  dst_circles[index] = dst_circle;
}
