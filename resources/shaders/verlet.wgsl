
struct UniformBag {
 win_width: f32,
 win_height: f32,
};
@group(0) @binding(0)
var<uniform> u_bag: UniformBag;

struct StorageBag {
 num_particles: f32,
 num_constraints: f32,
 buf: array<f32>,
}
@group(1) @binding(0)
var<storage, read> s_bag: StorageBag ;

struct VertexInput {
  @location(0) position: vec3<f32>,
};

struct VertexOutput {
  @builtin(position) clip_position: vec4<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.clip_position = vec4<f32>(model.position, 1.0);
  return out;
}

fn line(p: vec2f, a: vec2f, b: vec2f) -> f32 {
  let pa = p - a;
  let ba = b - a;
  let t = clamp(dot(pa, ba) / dot(ba, ba), 0.0, 1.0);
  let c = a + ba * t;
  let d = length(c - p);
  return smoothstep(fwidth(d), 0.0, d - 0.001);
}

fn circle(p: vec2f, c: vec2f, radius: f32) -> f32 {
  let d = length(p - c);
  return smoothstep(fwidth(d), 0.0, d - radius);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  var uv = vec2f(in.clip_position.x / u_bag.win_width, 1.0 - in.clip_position.y / u_bag.win_height);
  let white = vec4f(1.0, 1.0, 1.0, 1.0);
  let red = vec4f(1.0, 0.0, 0.0, 1.0);
  var result = vec4f(0.0);

  let bottom_border = line(uv, vec2f(0.0, 0.0), vec2f(1.0, 0.0));
  result += bottom_border * red;
  let right_border = line(uv, vec2f(1.0, 0.0), vec2f(1.0, 1.0));
  result += right_border * red;
  let top_border = line(uv, vec2f(1.0, 1.0), vec2f(0.0, 1.0));
  result += top_border * red;
  let left_border = line(uv, vec2f(0.0, 1.0), vec2f(0.0, 0.0));
  result += left_border * red;

  let num_particles = u32(s_bag.num_particles);
  for(var i = 0u; i < num_particles; i = i + 1u) {
    let idx = 3u * i;
    let center = vec2f(s_bag.buf[idx], s_bag.buf[idx + 1u]);
    let radius = s_bag.buf[idx + 2u];
    let circle_val = circle(uv, center, radius);
    result += circle_val * white;
  }

  let num_constraints = u32(s_bag.num_constraints);
  for(var i = 0u; i < num_constraints; i = i + 1u) {
    let idx = 3u * num_particles + 4u * i;
    let start = vec2f(s_bag.buf[idx], s_bag.buf[idx + 1u]);
    let end = vec2f(s_bag.buf[idx + 2u], s_bag.buf[idx + 3u]);
    let line_val = line(uv, start, end);
    result += line_val * white;
  }

  return result;
}
