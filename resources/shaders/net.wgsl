
struct VsInput {
    @location(0) position: vec3f,
    @location(1) color: vec3f,
}
struct VsOutput {
    @builtin(position) clip_position: vec4f,
    @location(0) color: vec3f,
}

@vertex
fn vs_main(vs_in: VsInput) -> VsOutput {
    var vs_out: VsOutput;
    vs_out.color = vs_in.color;
    vs_out.clip_position = vec4f(vs_in.position, 1.0);
    return vs_out;
}

@fragment
fn fs_main(vs_out: VsOutput) -> @location(0) vec4f {
    return vec4f(vs_out.color, 1.0);
}
