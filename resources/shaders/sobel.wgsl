// Vertex shader

struct UniformBag {
 pos_x: f32,
 sobel: f32,
};
@group(1) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
  @location(2) tex_coords: vec2<f32>,
};

struct VertexOutput {
  @builtin(position) pos: vec4<f32>,
  @location(1) tex_coords: vec2<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.tex_coords = model.tex_coords;
  let position = vec4f(model.position.x + u_bag.pos_x, model.position.yz, 1.0);
  out.pos = position;
  return out;
}

// Fragment shader

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0) @binding(1)
var s_diffuse: sampler;

fn make_kernel(tex: texture_2d<f32>, coord: vec2i) -> array<vec4f, 9> {
  var n: array<vec4f, 9>;
  n[0] = textureLoad(tex, coord + vec2i(-1, -1), 0);
  n[1] = textureLoad(tex, coord + vec2i(0, -1),  0);
  n[2] = textureLoad(tex, coord + vec2i(1, -1),  0);
  n[3] = textureLoad(tex, coord + vec2i(-1, 0),  0);
  n[4] = textureLoad(tex, coord,                 0);
  n[5] = textureLoad(tex, coord + vec2i(1, 0),   0);
  n[6] = textureLoad(tex, coord + vec2i(-1, 1),  0);
  n[7] = textureLoad(tex, coord + vec2i(0, 1),   0);
  n[8] = textureLoad(tex, coord + vec2i(1, 1),   0);
  return n;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  if u_bag.sobel == 1.0 {
      let texelCoords = vec2<i32>(in.tex_coords * vec2<f32>(textureDimensions(t_diffuse)));
      // "out of bounds texels return a null color" (?)
      let n = make_kernel(t_diffuse, texelCoords);

      let sobel_edge_h = n[2] + (2.0 * n[5]) + n[8] -
      (n[0] + (2.0 * n[3]) + n[6]);
      let sobel_edge_v = n[0] + (2.0 * n[1]) + n[2] -
      (n[6] + (2.0 * n[7]) + n[8]);
      let sobel = sqrt((sobel_edge_h * sobel_edge_h) +
                       (sobel_edge_v * sobel_edge_v));
      return vec4f(1.0 - sobel.rgb, 1.0);
    } else {
    // let texelCoords =
    //   vec2<i32>(in.tex_coords * vec2<f32>(textureDimensions(t_diffuse)));
    // return vec4f(textureLoad(t_diffuse, texelCoords, 0).rgb, 1.0);

    return textureSample(t_diffuse, s_diffuse, in.tex_coords);
  }
}
