
struct UniformBag {
 center_x: f32,
 center_y: f32,
 radius: f32,
 win_width: f32,
 win_height: f32,
 padding1: f32,
 padding2: f32,
 padding3: f32,
};
@group(1) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
};

struct VertexOutput {
  @builtin(position) uv: vec4<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.uv = vec4<f32>(model.position, 1.0);
  return out;
}

fn circle(p: vec2f, c: vec2f, radius: f32) -> f32 {
  let d = length(p - c);
  return smoothstep(fwidth(d), 0.0, d - radius);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  var uv = in.uv.xy / vec2f(u_bag.win_width, u_bag.win_height);
  uv.y = 1.0 - uv.y;
  let circle_val = circle(uv, vec2f(u_bag.center_x, u_bag.center_y), u_bag.radius);
  return circle_val * vec4f(1.0, 0.0, 0.0, 1.0);
}
