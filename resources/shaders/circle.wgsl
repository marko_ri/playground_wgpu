
struct Camera {
 view_proj: mat4x4<f32>,
};
@group(0) @binding(0)
var<uniform> camera: Camera;

struct UniformBag {
 win_width: f32,
 win_height: f32,
 circle_x: f32,
 circle_y: f32,
};
@group(1) @binding(0)
var<uniform> u_bag: UniformBag;

struct VertexInput {
  @location(0) position: vec3<f32>,
};

struct VertexOutput {
  @builtin(position) clip_position: vec4<f32>,
};

@vertex
fn vs_main(model: VertexInput) -> VertexOutput {
  var out: VertexOutput;
  out.clip_position = vec4<f32>(model.position, 1.0);
  return out;
}

fn plot(uv: vec2f, f_x: f32, width: f32) -> f32 {
  return smoothstep(f_x - width, f_x, uv.y) - smoothstep(f_x, f_x + width, uv.y);
}

fn circle(uv: vec2f, center: vec2f, size: f32) -> f32 {
  let dist = distance(uv, center);
  return smoothstep(size, size + 0.003, 1.0 - dist);
}

fn rect(uv: vec2f, center: vec2f, size: vec2f) -> f32 {
  let s = abs(uv - center) - size;
  let square = length(max(s, vec2f(0.0, 0.0)));
  return 1.0 - smoothstep(0.0, 0.0001, square);
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
  var uv = in.clip_position.xy / vec2f(u_bag.win_width, u_bag.win_height); // * 2.0 - 1.0;
  uv.y = 1.0 - uv.y;

  // line
  let f_line = uv.x;
  let line_val = plot(uv, f_line, 0.005);
  let line_col = vec4f(0.0, 1.0, 0.0, 1.0);
  let line = line_val * line_col;

  // sin
  let uv_sin = 4.0 * uv - 1.5;
  let f_sin = sin(5.0 * uv_sin.x);
  let sin_val = plot(uv_sin, f_sin, 0.05);
  let sin_col = vec4f(1.0, 0.0, 0.0, 1.0);
  let sin = sin_val * sin_col;

  // circle
  let aspect = u_bag.win_width / u_bag.win_height;
  let uv_circle = vec2f(uv.x * aspect, uv.y);
  let center = vec2f(u_bag.circle_x * aspect, u_bag.circle_y);
  let circle_val = circle(uv_circle, center, 0.9);
  let circle_col = vec4f(0.8, 0.3, 0.2, 1.0);
  let circle = circle_val * circle_col;

  // rect
  let rect_val = rect(uv, vec2f(0.7), vec2f(0.1, 0.05));
  let rect_col = vec4f(0.0, 0.0, 1.0, 1.0);
  let rect = rect_val * rect_col;

  return line + sin + circle + rect;
}
