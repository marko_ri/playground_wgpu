
Learning wgpu by following tutorial:

- [Learn Wgpu](https://sotrh.github.io/learn-wgpu)

Other tutorials will be added later:

- [Webgpu C++ guide](https://eliemichel.github.io/LearnWebGPU)
- [Webgpu Fundamentals](https://webgpufundamentals.org)

Other examples will be added later:

- [Platformer](https://github.com/ShamylZakariya/Platformer)
- [webgpu-native-examples](https://github.com/samdauwe/webgpu-native-examples/)


_This is a playground in progress_

Run the tutorials: `cargo run --release -- NAME`, where NAME is one of: `learn_wgpu, sobel, circle, grid, font, boid, bouncy, bezier, verlet, video, dda, gerstner`
Run the client/server example: `cargo run --bin server`, and than create clients with: `cargo run -- net_client`.
