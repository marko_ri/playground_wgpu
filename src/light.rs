use super::{
    factory,
    model::{Model, Vertex, VertexTex},
    BindGroupKind, DEPTH_FORMAT,
};
use std::collections::HashMap;
use wgpu::util::DeviceExt;

pub struct Light {
    model: Model,
    uniform: LightUniform,
}

impl Light {
    pub fn new(model: Model, position: [f32; 3], color: [f32; 3]) -> Self {
        let uniform = LightUniform {
            position,
            _padding1: 0,
            color,
            _padding2: 0,
        };

        Light { model, uniform }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct LightUniform {
    position: [f32; 3],
    // Due to uniforms requiring 16 byte (4 float) spacing,
    // we need to use a padding field here
    _padding1: u32,
    color: [f32; 3],
    _padding2: u32,
}

struct LightResource {
    buffer: wgpu::Buffer,
    bind_group: wgpu::BindGroup,
    bind_group_layout: wgpu::BindGroupLayout,
}

impl LightResource {
    pub fn new(device: &wgpu::Device, data: LightUniform) -> Self {
        let bind_group_layout = factory::create_light_bg_layout(device);

        let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Light Buffer"),
            contents: bytemuck::cast_slice(&[data]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: buffer.as_entire_binding(),
            }],
            label: None,
        });

        Self {
            buffer,
            bind_group,
            bind_group_layout,
        }
    }

    fn update(&self, queue: &wgpu::Queue, uniform: LightUniform) {
        queue.write_buffer(&self.buffer, 0, bytemuck::cast_slice(&[uniform]));
    }
}

pub struct LightContainer {
    render_pipeline: wgpu::RenderPipeline,
    light: Light,
    bind_group_indices: HashMap<BindGroupKind, u8>,
    resource: LightResource,
}

impl LightContainer {
    pub fn new(
        device: &wgpu::Device,
        color_format: wgpu::TextureFormat,
        light: Light,
        camera_bg_layout: Option<&wgpu::BindGroupLayout>,
    ) -> Self {
        let resource = LightResource::new(device, light.uniform);

        let mut bind_group_indices = HashMap::new();
        bind_group_indices.insert(BindGroupKind::Light, 1_u8);
        let bg_layouts = if let Some(camera_bg_layout) = camera_bg_layout {
            bind_group_indices.insert(BindGroupKind::Camera, 0_u8);
            vec![camera_bg_layout, &resource.bind_group_layout]
        } else {
            vec![&resource.bind_group_layout]
        };

        let pipeline_layout =
            factory::create_pipeline_layout("Light Pipeline Layout", device, &bg_layouts);
        let shader_name = "light.wgsl";
        let render_pipeline = factory::create_render_pipeline(
            device,
            &pipeline_layout,
            color_format,
            Some(DEPTH_FORMAT),
            &[VertexTex::desc()],
            shader_name,
            wgpu::PrimitiveTopology::TriangleList,
        );

        Self {
            render_pipeline,
            light,
            bind_group_indices,
            resource,
        }
    }

    pub fn update(&self, queue: &wgpu::Queue) {
        self.resource.update(queue, self.light.uniform);
    }

    pub fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.resource.bind_group_layout
    }

    pub fn bind_group(&self) -> &wgpu::BindGroup {
        &self.resource.bind_group
    }

    pub fn render_pipeline(&self) -> &wgpu::RenderPipeline {
        &self.render_pipeline
    }

    pub fn model(&self) -> &Model {
        &self.light.model
    }

    pub fn bind_group_indices(&self) -> &HashMap<BindGroupKind, u8> {
        &self.bind_group_indices
    }

    pub fn old_position(&self) -> &[f32; 3] {
        &self.light.uniform.position
    }

    pub fn set_position(&mut self, position: [f32; 3]) {
        self.light.uniform.position = position;
    }
}
