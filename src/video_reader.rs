use ffmpeg_next::{
    codec::context::Context as CodecCtx,
    codec::decoder::video::Video as VideoDecoder,
    codec::packet::packet::Packet,
    format::context::input::Input,
    format::{self, Pixel},
    media::Type,
    software::scaling::context::Context as ScalingCtx,
    software::scaling::flag::Flags,
    util::frame::video::Video,
    util::time,
};

pub struct VideoDecodeState {
    input_ctx: Input,
    decoder: VideoDecoder,
    time_base: f64,
    stream_index: usize,
    duration_base: i64,
}

impl VideoDecodeState {
    pub fn open_file(path: &str) -> Self {
        ffmpeg_next::init().unwrap();

        let input_ctx = format::input(&path).unwrap();
        let video_stream = input_ctx.streams().best(Type::Video).unwrap();
        let video_stream_index = video_stream.index();

        let context_decoder = CodecCtx::from_parameters(video_stream.parameters()).unwrap();
        let video_decoder = context_decoder.decoder().video().unwrap();
        let video_time_base = {
            let time_base = video_stream.time_base();
            time_base.numerator() as f64 / time_base.denominator() as f64
        };

        Self {
            input_ctx,
            decoder: video_decoder,
            time_base: video_time_base,
            stream_index: video_stream_index,
            duration_base: 0,
        }
    }

    pub fn width(&self) -> u32 {
        self.decoder.width()
    }

    pub fn height(&self) -> u32 {
        self.decoder.height()
    }

    pub fn start_decode(mut self, tx: std::sync::mpsc::Sender<Vec<u8>>) {
        self.duration_base = time::current();

        let mut scaling_ctx = ScalingCtx::get(
            self.decoder.format(),
            self.decoder.width(),
            self.decoder.height(),
            Pixel::RGBA,
            self.decoder.width(),
            self.decoder.height(),
            Flags::FAST_BILINEAR,
        )
        .unwrap();

        for (stream, packet) in self.input_ctx.packets() {
            if stream.index() == self.stream_index {
                let decoder = &mut self.decoder;
                decoder.send_packet(&packet).unwrap();
                receive_and_process_decoded_frame(
                    decoder,
                    Some(&packet),
                    &self.duration_base,
                    &self.time_base,
                    &mut scaling_ctx,
                    tx.clone(),
                );
            }
        }

        // Sends a NULL packet to the decoder to signal end of stream
        // and enter draining mode
        self.decoder.send_eof().unwrap();
        receive_and_process_decoded_frame(
            &mut self.decoder,
            None,
            &self.duration_base,
            &self.time_base,
            &mut scaling_ctx,
            tx,
        );
    }
}

fn receive_and_process_decoded_frame(
    decoder: &mut VideoDecoder,
    packet: Option<&Packet>,
    duration_base: &i64,
    time_base: &f64,
    scaling_ctx: &mut ScalingCtx,
    tx: std::sync::mpsc::Sender<Vec<u8>>,
) {
    let mut decoded = Video::empty();
    while decoder.receive_frame(&mut decoded).is_ok() {
        let mut rgb_frame = Video::empty();
        scaling_ctx.run(&decoded, &mut rgb_frame).unwrap();
        sleep_to_pts(packet, duration_base, time_base);
        tx.send(rgb_frame.data(0).to_vec()).unwrap();
    }
}

fn sleep_to_pts(packet: Option<&Packet>, duration_base: &i64, time_base: &f64) {
    let mut pts = packet.and_then(|p| p.dts()).unwrap_or_default();
    pts *= f64::round(time_base * 1_000_000.0) as i64;

    let duration = time::current() - *duration_base;
    let delay = pts - duration;
    if delay > 0 {
        time::sleep(delay as u32).unwrap();
    }
}
