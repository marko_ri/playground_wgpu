use super::{Camera, WindowUpdateContext};
use cgmath::*;

#[derive(Debug)]
pub struct Aabb {
    pub bottom_left: Vector2<f32>,
    pub top_right: Vector2<f32>,
}

impl Aabb {
    pub fn center(&self) -> Vector2<f32> {
        vec2(
            self.bottom_left.x + self.width() / 2.0,
            self.bottom_left.y + self.height() / 2.0,
        )
    }

    pub fn width(&self) -> f32 {
        self.top_right.x - self.bottom_left.x
    }

    pub fn height(&self) -> f32 {
        self.top_right.y - self.bottom_left.y
    }
}

pub fn point_in_aabb(point: &Vector2<f32>, aabb: &Aabb) -> bool {
    aabb.bottom_left.x <= point.x
        && aabb.top_right.x >= point.x
        && aabb.bottom_left.y <= point.y
        && aabb.top_right.y >= point.y
}

pub fn aabb_in_aabb(r1: &Aabb, r2: &Aabb) -> bool {
    r1.bottom_left.x < r2.top_right.x
        && r1.top_right.x > r2.bottom_left.x
        && r1.bottom_left.y < r2.top_right.y
        && r1.top_right.y > r2.bottom_left.y
}

pub fn circle_aabb_resolution(potential_pos: &mut Vector2<f32>, radius: f32, aabb: &Aabb) {
    // nearest point to future circle position
    let nearest_point = vec2(
        f32::max(
            aabb.bottom_left.x,
            f32::min(potential_pos.x, aabb.top_right.x),
        ),
        f32::max(
            aabb.bottom_left.y,
            f32::min(potential_pos.y, aabb.top_right.y),
        ),
    );

    let ray_to_nearest = nearest_point - *potential_pos;

    let mut overlap = radius - ray_to_nearest.magnitude();
    if f32::is_nan(overlap) {
        overlap = 0.0;
    }

    if overlap > 0.0 {
        // statically resolve the collision
        // displace backwards by the overlap amount
        *potential_pos -= ray_to_nearest.normalize() * overlap;
    }
}

// ray casting in world space:
// ray origin is going to be the world x,y,z position of the camera
// ray has direction derived from mouse cursor coords (modified and normalized)
pub fn clicked_on_aabb(aabb: &Aabb, wuc: &WindowUpdateContext, camera: &Camera) -> bool {
    let mouse_world = mouse_screen_to_world(wuc, camera);
    mouse_world.x >= aabb.bottom_left.x
        && mouse_world.x <= aabb.top_right.x
        && mouse_world.y >= aabb.bottom_left.y
        && mouse_world.y <= aabb.top_right.y
}

// why does this work ??
pub fn mouse_screen_to_world(wuc: &WindowUpdateContext, camera: &Camera) -> Vector2<f32> {
    let ndc = vec2(
        2.0 * wuc.mouse_screen().x / wuc.size.width as f32 - 1.0,
        1.0 - 2.0 * wuc.mouse_screen().y / wuc.size.height as f32,
    );
    let res = camera.view_matrix().invert().unwrap()
        * camera.projection_matrix().invert().unwrap()
        * vec4(ndc.x, ndc.y, 0.0, 1.0);
    vec2(res.x, res.y)
}

// hm, this should work, but it does not...
fn mouse_screen_to_world_bad(wuc: &WindowUpdateContext, camera: &Camera) -> Vector3<f32> {
    // mouse center is top_left, move it to center of screen
    // move from range: [0:width, height:0] to range [-1:1, -1:1]
    let ray_ndc = vec2(
        2.0 * wuc.mouse_screen().x / wuc.size.width as f32 - 1.0,
        1.0 - 2.0 * wuc.mouse_screen().y / wuc.size.height as f32,
    );

    // ray.z should point forwards (negative z direction). add w so we have a 4d vector
    // range [-1:1, -1:1, -1:1, -1:1]
    let ray_clip = vec4(ray_ndc.x, ray_ndc.y, -1.0, 1.0);

    // to get into clip space from eye space we multiply the vector by a
    // projection matrix. We can go backwards by multiplying by the
    // inverse of this matrix
    // range [-x:x, -y:y, -z:z, -w:w]
    let ray_eye = camera.projection_matrix().invert().unwrap() * ray_clip;

    // we needed to un-project the x,y part, so let's set the z,w part
    // to mean "forwards, and not a point"
    let ray_eye = vec4(ray_eye.x, ray_eye.y, -1.0, 0.0);

    // multiply by the inverse of view matrix, plus:
    // we manually specified a -1 for the z component, which means
    // that our ray isn't normalised
    // range [-x:x, -y:y, -z:z, -w:w]
    let ray_world = camera.view_matrix().invert().unwrap() * ray_eye;
    vec3(ray_world.x, ray_world.y, ray_world.z).normalize()
}

pub fn dda(
    ray_start: &Vector2<f32>,
    ray_dir: &Vector2<f32>,
    tile_size: f32,
    map_size: f32,
    is_hit: impl Fn(&Vector2<i16>) -> bool,
) -> f32 {
    // at which particular tile are we located (top_left)
    let mut tl_curr_tile = vec2(
        (ray_start.x / tile_size).trunc(),
        (ray_start.y / tile_size + 1.0).trunc(),
    );

    let dy_dx = ray_dir.y / ray_dir.x;
    let dx_dy = ray_dir.x / ray_dir.y;
    let ray_unit_step_size = vec2((1.0 + dy_dx.powi(2)).sqrt(), (1.0 + dx_dy.powi(2)).sqrt());

    let mut ray_len = vec2(0.0, 0.0);
    let mut step = vec2(0.0, 0.0);

    // starting condition
    if ray_dir.x < 0.0 {
        step.x = -tile_size;
        // delta relative to top_left of current_tile and ray direction
        ray_len.x = (ray_start.x - tl_curr_tile.x) * ray_unit_step_size.x;
    } else {
        step.x = tile_size;
        // delta relative to top_left of current_tile and ray direction
        ray_len.x = ((tl_curr_tile.x + tile_size) as f32 - ray_start.x) * ray_unit_step_size.x;
    }

    if ray_dir.y < 0.0 {
        step.y = -tile_size;
        ray_len.y = (ray_start.y - (tl_curr_tile.y - tile_size) as f32) * ray_unit_step_size.y;
    } else {
        step.y = tile_size;
        ray_len.y = (tl_curr_tile.y - ray_start.y) * ray_unit_step_size.y;
    }

    // walk along the ray
    let mut distance = 0.0;
    while distance < map_size {
        if ray_len.x < ray_len.y {
            tl_curr_tile.x += step.x;
            distance = ray_len.x;
            ray_len.x += ray_unit_step_size.x * tile_size;
        } else {
            tl_curr_tile.y += step.y;
            distance = ray_len.y;
            ray_len.y += ray_unit_step_size.y * tile_size;
        }

        if 0.0 <= tl_curr_tile.x
            && tl_curr_tile.x < map_size
            && 0.0 <= tl_curr_tile.y
            && tl_curr_tile.y < map_size
        {
            let tl = vec2(tl_curr_tile.x as i16, tl_curr_tile.y as i16);
            if is_hit(&tl) {
                return distance;
            }
        }
    }

    0.0
}
