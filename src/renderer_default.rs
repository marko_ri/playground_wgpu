use super::{
    camera::CameraContainer,
    factory,
    gpu::Gpu,
    light::LightContainer,
    model::{InstanceRaw, Vertex, VertexCol, VertexLayoutKind, VertexTex},
    scene::{Pipeline, Scene, StorageResource, UniformResource},
    window::Window,
    BindGroupKind, EguiContext, Init, Renderer, Texture, DEPTH_FORMAT,
};
use std::collections::HashMap;

pub struct RendererDefault {
    scenes: Vec<Scene>,
    pub active_scene_idx: usize,
}

impl RendererDefault {
    pub fn new(device: &wgpu::Device, config: &wgpu::SurfaceConfiguration, init: Init) -> Self {
        let mut scenes = Vec::new();
        for init_scene in init.scenes.into_iter() {
            let camera_container = init_scene
                .camera
                .map(|camera| CameraContainer::new(device, camera));

            let light_container = init_scene.light.map(|light| {
                LightContainer::new(
                    device,
                    config.format,
                    light,
                    camera_container.as_ref().map(|c| c.bind_group_layout()),
                )
            });

            let mut pipelines = Vec::new();
            for init_pipeline in init_scene.render_pipelines.into_iter() {
                let vertex_layouts = if let Some(model) = init_pipeline.model.as_ref() {
                    let mut result = match model.vertex_layout_kind {
                        VertexLayoutKind::VertexColor => vec![VertexCol::desc()],
                        VertexLayoutKind::VertexTexture => vec![VertexTex::desc()],
                    };
                    if model.instance_buffer.is_some() {
                        result.push(InstanceRaw::desc());
                    }
                    result
                } else {
                    vec![]
                };

                let uniform_resource = if init_pipeline.uniforms.is_empty() {
                    None
                } else {
                    Some(UniformResource::new(device, &init_pipeline.uniforms))
                };

                let storage_resource = init_pipeline
                    .storage
                    .map(|s| StorageResource::new(device, &s));

                let mut bg_layouts = Vec::new();
                let mut bg_indices = HashMap::new();

                if let Some(camera_container) = camera_container.as_ref() {
                    bg_layouts.push(camera_container.bind_group_layout());
                    bg_indices.insert(BindGroupKind::Camera, (bg_layouts.len() - 1) as u8);
                }

                if let Some(model) = init_pipeline.model.as_ref() {
                    for material in model.materials.iter() {
                        bg_layouts.push(&material.bind_group_layout);
                        bg_indices.insert(BindGroupKind::Texture, (bg_layouts.len() - 1) as u8);
                    }
                }
                if let Some(light_container) = light_container.as_ref() {
                    bg_layouts.push(light_container.bind_group_layout());
                    bg_indices.insert(BindGroupKind::Light, (bg_layouts.len() - 1) as u8);
                }

                if let Some(uniform_resource) = uniform_resource.as_ref() {
                    bg_layouts.push(&uniform_resource.bind_group_layout);
                    bg_indices.insert(BindGroupKind::Uniform, (bg_layouts.len() - 1) as u8);
                }

                if let Some(storage_resource) = storage_resource.as_ref() {
                    bg_layouts.push(&storage_resource.bind_group_layout);
                    bg_indices.insert(BindGroupKind::Storage, (bg_layouts.len() - 1) as u8);
                }

                let primitive_topology = init_pipeline
                    .model
                    .as_ref()
                    .map(|model| model.primitive_topology)
                    .unwrap_or_default();

                let pipeline_layout =
                    factory::create_pipeline_layout("Render Pipeline Layout", device, &bg_layouts);

                let render_pipeline = factory::create_render_pipeline(
                    device,
                    &pipeline_layout,
                    config.format,
                    Some(DEPTH_FORMAT),
                    &vertex_layouts,
                    &init_pipeline.shader_name,
                    primitive_topology,
                );

                let pipeline = Pipeline {
                    name: init_pipeline.pipeline_name,
                    shader_name: init_pipeline.shader_name,
                    render_pipeline,
                    model: init_pipeline.model,
                    uniform_resource,
                    bind_group_indices: bg_indices,
                    is_visible: !init_pipeline.hide,
                    storage_resource,
                };

                pipelines.push(pipeline);
            }

            let egui_state = init_scene
                .egui_state
                .filter(|e| e.scene_name == init_scene.name);

            let scene = Scene {
                name: init_scene.name,
                pipelines,
                light_container,
                background_color: wgpu::Color::BLACK,
                camera_container,
                egui_state,
            };

            scenes.push(scene);
        }

        Self {
            scenes,
            active_scene_idx: 0,
        }
    }

    fn update(&mut self, gpu: &Gpu, window: &Window) {
        self.active_scene_mut().update(gpu.queue(), window);
    }

    fn draw(
        &mut self,
        gpu: &Gpu,
        depth_texture: Option<&Texture>,
        mut egui: Option<&mut EguiContext>,
        window: &Window,
    ) -> Result<(), wgpu::SurfaceError> {
        let output = gpu.surface().get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let mut encoder = gpu
            .device()
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });
        let mut egui_textures = None;
        {
            if self.active_scene().egui_state.is_some() {
                if let Some(egui) = egui.as_deref_mut() {
                    let res = egui.update(gpu, &mut encoder, window.win(), self.active_scene_mut());
                    egui_textures = Some(res);
                }
            }

            let background_color = self.active_scene().background_color;
            let depth_stencil_attachment =
                depth_texture
                    .as_ref()
                    .map(|dt| wgpu::RenderPassDepthStencilAttachment {
                        view: &dt.view,
                        depth_ops: Some(wgpu::Operations {
                            load: wgpu::LoadOp::Clear(1.0),
                            store: true,
                        }),
                        stencil_ops: None,
                    });

            let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Render Pass"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(background_color),
                        store: true,
                    },
                })],
                depth_stencil_attachment,
            });

            if self.active_scene().egui_state.is_some() {
                if let Some(egui) = egui.as_deref_mut() {
                    egui.draw(&mut render_pass);
                }
            }

            self.active_scene_mut().draw(&mut render_pass);
        }

        gpu.queue().submit(std::iter::once(encoder.finish()));
        output.present();

        if self.active_scene().egui_state.is_some() {
            if let Some(egui) = egui {
                egui.free(egui_textures);
            }
        }

        Ok(())
    }
}

impl Renderer for RendererDefault {
    fn set_active_scene(&mut self, idx: usize) {
        self.active_scene_idx = idx;
    }

    fn active_scene(&self) -> &Scene {
        &self.scenes[self.active_scene_idx]
    }

    fn active_scene_mut(&mut self) -> &mut Scene {
        &mut self.scenes[self.active_scene_idx]
    }

    fn resize(&mut self, width: u32, height: u32) {
        for scene in self.scenes.iter_mut() {
            scene.resize(width, height);
        }
    }

    fn show(
        &mut self,
        gpu: &Gpu,
        window: &Window,
        depth_texture: Option<&Texture>,
        egui: Option<&mut EguiContext>,
    ) {
        self.update(gpu, window);
        self.draw(gpu, depth_texture, egui, window).unwrap();
    }
}
