use cgmath::*;
use std::collections::HashMap;

#[derive(Debug, Clone)]
pub struct Tile {
    pub left_top: Vector2<f32>,
    pub right_bottom: Vector2<f32>,
}

impl Default for Tile {
    fn default() -> Self {
        Self {
            left_top: Vector2::zero(),
            right_bottom: Vector2::zero(),
        }
    }
}

#[derive(Debug)]
pub struct TileSet {
    image_width: f32,
    image_height: f32,
    tile_width: f32,
    tile_height: f32,
    spacing: f32,
    columns: u32,
    pub tiles: HashMap<u32, Tile>,
    glyphs: HashMap<u8, Vector2<u32>>,
}

impl TileSet {
    // mario: (224, 32, 26, 16, 16, 0),
    // font:  (128, 64, 94, 7, 9, 0)
    pub fn new(
        image_width: f32,
        image_height: f32,
        tile_count: u32,
        tile_width: f32,
        tile_height: f32,
        spacing: f32,
    ) -> Self {
        let mut tiles = HashMap::new();
        let mut x = 0.0;
        let mut y = 0.0;
        for i in 0..tile_count {
            let top_y = y / image_height;
            let bottom_y = (y + tile_height) / image_height;
            let left_x = x / image_width;
            let right_x = (x + tile_width) / image_width;

            tiles.insert(
                i,
                Tile {
                    left_top: vec2(left_x, top_y),
                    right_bottom: vec2(right_x, bottom_y),
                },
            );

            x += tile_width + spacing;
            if x > (image_width - tile_width) {
                x = 0.0;
                y += tile_height + spacing;
            }
        }

        Self {
            image_width,
            image_height,
            tile_width,
            tile_height,
            spacing,
            columns: f32::floor(image_width / (tile_width + spacing)) as u32,
            tiles,
            glyphs: HashMap::new(),
        }
    }

    pub fn default_font(&mut self) {
        let mut glyphs = HashMap::new();
        glyphs.insert(b'a', vec2(11, 3));
        glyphs.insert(b'b', vec2(12, 3));
        glyphs.insert(b'c', vec2(13, 3));
        glyphs.insert(b'd', vec2(14, 3));
        glyphs.insert(b'e', vec2(15, 3));
        glyphs.insert(b'f', vec2(16, 3));
        glyphs.insert(b'g', vec2(17, 3));
        glyphs.insert(b'h', vec2(0, 4));
        glyphs.insert(b'i', vec2(1, 4));
        glyphs.insert(b'j', vec2(2, 4));
        glyphs.insert(b'k', vec2(3, 4));
        glyphs.insert(b'l', vec2(4, 4));
        glyphs.insert(b'm', vec2(5, 4));
        glyphs.insert(b'n', vec2(6, 4));
        glyphs.insert(b'o', vec2(7, 4));
        glyphs.insert(b'p', vec2(8, 4));
        glyphs.insert(b'q', vec2(9, 4));
        glyphs.insert(b'r', vec2(10, 4));
        glyphs.insert(b's', vec2(11, 4));
        glyphs.insert(b't', vec2(12, 4));
        glyphs.insert(b'u', vec2(13, 4));
        glyphs.insert(b'v', vec2(14, 4));
        glyphs.insert(b'w', vec2(15, 4));
        glyphs.insert(b'x', vec2(15, 4));
        glyphs.insert(b'y', vec2(17, 4));
        glyphs.insert(b'z', vec2(0, 5));
        glyphs.insert(b'0', vec2(16, 0));
        glyphs.insert(b'1', vec2(17, 0));
        glyphs.insert(b'2', vec2(0, 1));
        glyphs.insert(b'3', vec2(1, 1));
        glyphs.insert(b'4', vec2(2, 1));
        glyphs.insert(b'5', vec2(3, 1));
        glyphs.insert(b'6', vec2(4, 1));
        glyphs.insert(b'7', vec2(5, 1));
        glyphs.insert(b'8', vec2(6, 1));
        glyphs.insert(b'9', vec2(7, 1));
        glyphs.insert(b'?', vec2(13, 1));
        glyphs.insert(b' ', vec2(0, 0));
        self.glyphs = glyphs;
    }

    pub fn tile_at_position(&self, position: &Vector2<u32>) -> Option<&Tile> {
        let idx = position.y * self.columns + position.x;
        self.tiles.get(&idx)
    }

    pub fn glyph(&self, c: u8) -> &Tile {
        let pos = self
            .glyphs
            .get(&c)
            .unwrap_or_else(|| self.glyphs.get(&b'?').unwrap());
        self.tile_at_position(pos).unwrap()
    }
}
