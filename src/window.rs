use cgmath::*;
use std::collections::HashMap;
use winit::{
    dpi::PhysicalSize,
    event::VirtualKeyCode,
    event_loop::EventLoop,
    window::{self, WindowBuilder},
};

pub struct Window {
    win: window::Window,
    pub wuc: WindowUpdateContext,
}

impl Window {
    pub fn new(event_loop: &EventLoop<()>, width: u32, height: u32, resizable: bool) -> Self {
        let size = PhysicalSize::new(width, height);
        let win = WindowBuilder::new().build(event_loop).unwrap();
        win.set_inner_size(size);
        win.set_resizable(resizable);
        // uf, this line does havoc :(
        //win.set_decorations(false);
        let wuc = WindowUpdateContext::new(size);

        Self { win, wuc }
    }

    pub fn win(&self) -> &window::Window {
        &self.win
    }

    pub fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.wuc.size = new_size;
        }
    }
}

pub struct WindowUpdateContext {
    pub total_time: f32,
    pub delta_time: f32,
    pub frame_count: u32,
    pub is_escape_pressed: bool,
    pub size: PhysicalSize<u32>,
    mouse_screen: Vector2<f32>,
    mouse_scroll: Vector2<f32>,
    pub mouse_buttons: [bool; 3], // left, middle, right
    previous_mouse_buttons: [bool; 3],
    pub keys_pressed: HashMap<VirtualKeyCode, bool>,
    previous_keys_pressed: HashMap<VirtualKeyCode, bool>,
}

impl WindowUpdateContext {
    pub fn new(size: PhysicalSize<u32>) -> Self {
        Self {
            total_time: 0.0,
            delta_time: 0.0,
            frame_count: 0,
            is_escape_pressed: false,
            size,
            mouse_screen: Vector2::zero(),
            mouse_scroll: Vector2::zero(),
            mouse_buttons: [false; 3],
            previous_mouse_buttons: [false; 3],
            keys_pressed: HashMap::new(),
            previous_keys_pressed: HashMap::new(),
        }
    }

    pub fn update(&mut self, delta_time: f32) {
        self.delta_time = delta_time;
        self.total_time += delta_time;
        self.frame_count += 1;
        self.previous_mouse_buttons = self.mouse_buttons;
        self.previous_keys_pressed = self.keys_pressed.clone();
    }

    pub fn is_mouse_pressed(&self, button_index: usize) -> bool {
        self.mouse_buttons[button_index] && !self.previous_mouse_buttons[button_index]
    }

    pub fn is_mouse_released(&self, button_index: usize) -> bool {
        !self.mouse_buttons[button_index] && self.previous_mouse_buttons[button_index]
    }

    pub fn is_key_pressed(&self, key: &VirtualKeyCode) -> bool {
        *self.keys_pressed.get(key).unwrap_or(&false)
            && !*self.previous_keys_pressed.get(key).unwrap_or(&false)
    }

    pub fn is_key_released(&self, key: &VirtualKeyCode) -> bool {
        !*self.keys_pressed.get(key).unwrap_or(&false)
            && *self.previous_keys_pressed.get(key).unwrap_or(&false)
    }

    pub fn set_mouse_screen(&mut self, x: f32, y: f32) {
        self.mouse_screen = vec2(x, y);
    }

    pub fn mouse_screen(&self) -> &Vector2<f32> {
        &self.mouse_screen
    }

    pub fn mouse_normalized(&self) -> Vector2<f32> {
        vec2(
            self.mouse_screen.x / self.size.width as f32,
            1.0 - self.mouse_screen.y / self.size.height as f32,
        )
    }

    pub fn mouse_world(&self, world_size: f32, aspect: f32) -> Vector2<f32> {
        vec2(
            self.mouse_screen.x * aspect / self.size.width as f32,
            1.0 - self.mouse_screen.y / self.size.height as f32,
        ) * world_size
    }

    pub fn set_mouse_scroll(&mut self, horizontal: f32, vertical: f32) {
        self.mouse_scroll = vec2(horizontal, vertical);
    }

    pub fn mouse_scroll(&self) -> &Vector2<f32> {
        &self.mouse_scroll
    }
}
