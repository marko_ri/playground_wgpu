use super::{factory, texture::Texture};
use cgmath::*;
use wgpu::util::DeviceExt;

pub type Index = u32;

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct VertexCol {
    pub positions: [f32; 3],
    pub color: [f32; 4],
}

#[repr(C)]
#[derive(Copy, Clone, Debug, bytemuck::Pod, bytemuck::Zeroable, Default)]
pub struct VertexTex {
    pub positions: [f32; 3],
    pub normals: [f32; 3],
    pub tex_coords: [f32; 2],
}

pub trait Vertex: bytemuck::Pod + bytemuck::Zeroable {
    fn desc() -> wgpu::VertexBufferLayout<'static>;
    fn transform(&mut self, matrix: Matrix4<f32>);
    fn vertex_layout_kind(&self) -> VertexLayoutKind;
}

impl Vertex for VertexTex {
    fn transform(&mut self, matrix: Matrix4<f32>) {
        let v4 = vec4(self.positions[0], self.positions[1], self.positions[2], 1.0);
        let new_pos = matrix * v4;
        self.positions = [new_pos.x, new_pos.y, new_pos.z];
    }

    fn desc() -> wgpu::VertexBufferLayout<'static> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<VertexTex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                // positions
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                // normals
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x3,
                },
                // tex_coords
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 6]>() as wgpu::BufferAddress,
                    shader_location: 2,
                    format: wgpu::VertexFormat::Float32x2,
                },
            ],
        }
    }

    fn vertex_layout_kind(&self) -> VertexLayoutKind {
        VertexLayoutKind::VertexTexture
    }
}

impl Vertex for VertexCol {
    fn transform(&mut self, matrix: Matrix4<f32>) {
        let v4 = vec4(self.positions[0], self.positions[1], self.positions[2], 1.0);
        let new_pos = matrix * v4;
        self.positions = [new_pos.x, new_pos.y, new_pos.z];
    }

    fn desc() -> wgpu::VertexBufferLayout<'static> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<VertexCol>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                // positions
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                // colors
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x4,
                },
            ],
        }
    }

    fn vertex_layout_kind(&self) -> VertexLayoutKind {
        VertexLayoutKind::VertexColor
    }
}

#[derive(Clone)]
pub struct Instance {
    pub translate: Vector3<f32>,
}

#[repr(C)]
#[derive(Debug, Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct InstanceRaw {
    model: [[f32; 4]; 4],
}

impl From<Instance> for InstanceRaw {
    fn from(input: Instance) -> Self {
        Self {
            model: Matrix4::from_translation(input.translate).into(),
        }
    }
}

impl InstanceRaw {
    pub fn desc() -> wgpu::VertexBufferLayout<'static> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<InstanceRaw>() as wgpu::BufferAddress,
            // We need to switch from using a step mode of Vertex to Instance
            // This means that our shaders will only change to use the next
            // instance when the shader starts processing a new instance
            step_mode: wgpu::VertexStepMode::Instance,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    // While our vertex shader only uses locations 0, 1, 2, 3
                    // We'll start at slot 5
                    shader_location: 5,
                    format: wgpu::VertexFormat::Float32x4,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
                    shader_location: 6,
                    format: wgpu::VertexFormat::Float32x4,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 8]>() as wgpu::BufferAddress,
                    shader_location: 7,
                    format: wgpu::VertexFormat::Float32x4,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 12]>() as wgpu::BufferAddress,
                    shader_location: 8,
                    format: wgpu::VertexFormat::Float32x4,
                },
            ],
        }
    }
}

#[derive(Debug)]
pub struct Material {
    pub name: String,
    pub diffuse_texture: Option<Texture>,
    pub normal_texture: Option<Texture>,
    pub bind_group: wgpu::BindGroup,
    pub bind_group_layout: wgpu::BindGroupLayout,
}

impl Material {
    pub fn new(
        device: &wgpu::Device,
        name: &str,
        diffuse_texture: Texture,
        normal_texture: Option<Texture>,
    ) -> Self {
        let (bind_group, bind_group_layout) = if let Some(normal_texture) = &normal_texture {
            let bind_group_layout = factory::create_texture_bg_layout_with_normals(device);
            let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: wgpu::BindingResource::TextureView(&normal_texture.view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 3,
                        resource: wgpu::BindingResource::Sampler(&normal_texture.sampler),
                    },
                ],
                label: Some(name),
            });
            (bind_group, bind_group_layout)
        } else {
            let bind_group_layout = factory::create_texture_bg_layout_without_normals(device);
            let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &bind_group_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: wgpu::BindingResource::TextureView(&diffuse_texture.view),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: wgpu::BindingResource::Sampler(&diffuse_texture.sampler),
                    },
                ],
                label: Some(name),
            });
            (bind_group, bind_group_layout)
        };

        Self {
            name: String::from(name),
            diffuse_texture: Some(diffuse_texture),
            normal_texture,
            bind_group,
            bind_group_layout,
        }
    }
}

#[derive(Debug, Default)]
pub enum VertexLayoutKind {
    #[default]
    VertexColor,
    VertexTexture,
}

#[derive(Default, Debug)]
pub struct Mesh {
    pub vertex_buffer: Option<wgpu::Buffer>,
    pub index_buffer: Option<wgpu::Buffer>,
    pub num_indices: u32,
    pub num_vertices: u32,
    pub material: Option<usize>,
}

#[derive(Debug, Default)]
pub struct Model {
    pub meshes: Vec<Mesh>,
    pub materials: Vec<Material>,
    pub instance_buffer: Option<(u32, wgpu::Buffer)>,
    pub primitive_topology: wgpu::PrimitiveTopology,
    pub vertex_layout_kind: VertexLayoutKind,
}

impl Model {
    pub fn add_instances(&mut self, device: &wgpu::Device, instances: Vec<Instance>) {
        let len = instances.len() as u32;
        let buf = instances
            .into_iter()
            .map(InstanceRaw::from)
            .collect::<Vec<_>>();

        self.instance_buffer = Some((
            len,
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Instance Buffer"),
                contents: bytemuck::cast_slice(&buf),
                usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
            }),
        ));
    }
}
