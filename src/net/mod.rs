pub mod client;

use nanorand::{Rng, WyRand};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Request {
    Register,
    Remove,
    Update(Player),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum Response {
    Allowed((Player, HashMap<u32, Player>)),
    Denied(String),
    Joined(Player),
    Removed(u32),
    Updated(Player),
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Player {
    pub id: u32,
    pub health: u32,
    pub ammo: u32,
    pub radius: f32,
    pub position: cgmath::Vector2<f32>,
    pub velocity: cgmath::Vector2<f32>,
}

impl Default for Player {
    fn default() -> Self {
        let mut rng = WyRand::new();
        Self {
            id: rng.generate_range(1_u32..=100),
            health: 100,
            ammo: 999,
            radius: 0.05,
            position: cgmath::vec2(0.0, 0.0),
            velocity: cgmath::vec2(0.0, 0.0),
        }
    }
}
