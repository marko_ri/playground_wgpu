use futures::{SinkExt, StreamExt};
use tokio::net::TcpStream;
use tokio_util::codec::{Framed, LengthDelimitedCodec};

use super::{Request, Response};

pub async fn run(
    port: String,
    tx_in: std::sync::mpsc::Sender<Response>,
    mut rx_out: tokio::sync::mpsc::Receiver<Request>,
) {
    let socket = TcpStream::connect(&format!("localhost:{}", port))
        .await
        .unwrap();
    let framed = Framed::new(socket, LengthDelimitedCodec::new());
    let (mut writer, mut reader) = framed.split();

    loop {
        tokio::select! {
            result = reader.next() => match result {
                Some(Ok(msg)) => {
                    let decoded_msg: Response = bincode::deserialize(&msg).expect("Deserialization Problem");
                    if let Err(_) = tx_in.send(decoded_msg) {
                        eprintln!("InChannel is Closed!");
                        break;
                    }
                },
                _ => {
                    eprintln!("Server disconnected!");
                    break;
                }
            },
            result = rx_out.recv() => match result {
                Some(msg) => {
                    let encoded_msg: Vec<u8> = bincode::serialize(&msg).expect("Serialization Problem");
                    if let Err(_) = writer.send(encoded_msg.into()).await {
                        eprintln!("Can not write to Sink!");
                        break;
                    }
                }
                None => {
                    eprintln!("OutChannel is Closed!");
                    break;
                }
            }
        }
    }
}
