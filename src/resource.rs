use super::{
    model::{Material, Mesh, Model, Vertex, VertexLayoutKind, VertexTex},
    shape::{self, TextureKind},
    GenericResult, Texture,
};
use std::io::{BufReader, Cursor};
use std::path::{Path, PathBuf};
use wgpu::util::DeviceExt;

const RESOURCES_DIR: &str = "resources/";

fn resource_path<P: AsRef<Path>>(file_name: P) -> GenericResult<PathBuf> {
    let mut res_dir = std::env::current_dir()?;
    res_dir.push(RESOURCES_DIR);
    Ok(res_dir.join(file_name))
}

pub fn load_string<P: AsRef<Path>>(file_name: P) -> GenericResult<String> {
    let path = resource_path(file_name)?;
    std::fs::read_to_string(path).map_err(|e| e.into())
}

pub fn load_binary<P: AsRef<Path>>(file_name: P) -> GenericResult<Vec<u8>> {
    let path = resource_path(file_name)?;
    std::fs::read(path).map_err(|e| e.into())
}

pub fn load_texture(
    file_name: Option<&str>,
    is_normal_map: bool,
    device: &wgpu::Device,
    queue: &wgpu::Queue,
) -> GenericResult<Texture> {
    let Some(file_name) = file_name else {
        return Err("can not load texture".into());
    };
    let file_path = std::path::Path::new("img").join(file_name);
    let data = load_binary(file_path)?;
    Texture::from_image(device, queue, &data, file_name, is_normal_map)
}

pub fn load_model(
    file_name: &str,
    device: &wgpu::Device,
    queue: &wgpu::Queue,
) -> GenericResult<Model> {
    let img_path = std::path::Path::new("img");
    let obj_text = load_string(img_path.join(file_name))?;
    let obj_cursor = Cursor::new(obj_text);
    let mut obj_reader = BufReader::new(obj_cursor);

    let (models, obj_materials) = tobj::load_obj_buf(
        &mut obj_reader,
        &tobj::LoadOptions {
            triangulate: true,
            single_index: true,
            ..Default::default()
        },
        move |path| {
            let mat_text = load_string(img_path.join(path)).unwrap();
            tobj::load_mtl_buf(&mut BufReader::new(Cursor::new(mat_text)))
        },
    )?;

    let mut materials = Vec::new();
    for m in obj_materials? {
        let diffuse_texture = load_texture(m.diffuse_texture.as_deref(), false, device, queue)?;
        let normal_texture = load_texture(m.normal_texture.as_deref(), false, device, queue).ok();

        materials.push(Material::new(
            device,
            &m.name,
            diffuse_texture,
            normal_texture,
        ));
    }

    let meshes = models
        .into_iter()
        .map(|m| {
            let vertices = (0..m.mesh.positions.len() / 3)
                .map(|i| {
                    // let color = if m.mesh.vertex_color.is_empty() {
                    //     [0.0; 4]
                    // } else {
                    //     [
                    //         m.mesh.vertex_color[i * 3],
                    //         m.mesh.vertex_color[i * 3 + 1],
                    //         m.mesh.vertex_color[i * 3 + 2],
                    //         m.mesh.vertex_color[i * 3 + 3],
                    //     ]
                    // };
                    let tex_coords = if m.mesh.texcoords.is_empty() {
                        [0.0; 2]
                    } else {
                        [m.mesh.texcoords[i * 2], m.mesh.texcoords[i * 2 + 1]]
                    };
                    let normals = if m.mesh.normals.is_empty() {
                        [0.0; 3]
                    } else {
                        [
                            m.mesh.normals[i * 3],
                            m.mesh.normals[i * 3 + 1],
                            m.mesh.normals[i * 3 + 2],
                        ]
                    };
                    VertexTex {
                        positions: [
                            m.mesh.positions[i * 3],
                            m.mesh.positions[i * 3 + 1],
                            m.mesh.positions[i * 3 + 2],
                        ],
                        tex_coords,
                        normals,
                    }
                })
                .collect::<Vec<_>>();

            let vertex_buffer = Some(device.create_buffer_init(
                &wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("{:?} Vertex Buffer", file_name)),
                    contents: bytemuck::cast_slice(&vertices),
                    usage: wgpu::BufferUsages::VERTEX,
                },
            ));
            let index_buffer = Some(
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("{:?} Index Buffer", file_name)),
                    contents: bytemuck::cast_slice(&m.mesh.indices),
                    usage: wgpu::BufferUsages::INDEX,
                }),
            );

            Mesh {
                vertex_buffer,
                index_buffer,
                num_indices: m.mesh.indices.len() as u32,
                num_vertices: vertices.len() as u32,
                material: m.mesh.material_id,
            }
        })
        .collect::<Vec<_>>();

    Ok(Model {
        meshes,
        materials,
        instance_buffer: None,
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        vertex_layout_kind: VertexLayoutKind::VertexTexture,
    })
}

pub fn shape_to_model<V: Vertex>(
    shape: shape::Shape<V>,
    device: &wgpu::Device,
    queue: &wgpu::Queue,
) -> Model {
    shapes_to_model([shape].iter(), device, queue)
}

pub fn shape_ref_to_model<V: Vertex>(
    shape: &shape::Shape<V>,
    device: &wgpu::Device,
    queue: &wgpu::Queue,
) -> Model {
    shapes_to_model([shape].into_iter(), device, queue)
}

pub fn shapes_to_model<'a, V: Vertex>(
    shapes: impl Iterator<Item = &'a shape::Shape<V>>,
    device: &wgpu::Device,
    queue: &wgpu::Queue,
) -> Model {
    let mut iter = shapes.peekable();
    let first = iter.peek().unwrap();
    let primitive_topology = first.primitive_topology;

    let (materials, material_idx) = match first.texture_kind.as_ref() {
        Some(&TextureKind::Video {
            width: w,
            height: h,
        }) => {
            let texture = Texture::create_video_texture(device, w, h).unwrap();
            let material = Material::new(device, "video_texture", texture, None);
            (vec![material], Some(0))
        }
        Some(TextureKind::Image(name)) => {
            let texture = load_texture(Some(name), false, device, queue).unwrap();
            let material = Material::new(device, "alt_material", texture, None);
            (vec![material], Some(0))
        }
        None => (vec![], None),
    };

    let vertex_layout_kind = first.vertex_layout_kind();

    let mut meshes = Vec::new();
    for shape in iter {
        meshes.push(shape_to_mesh(shape, device, material_idx));
    }

    Model {
        meshes,
        materials,
        instance_buffer: None,
        primitive_topology,
        vertex_layout_kind,
    }
}

pub fn shape_to_mesh<V: Vertex>(
    shape: &shape::Shape<V>,
    device: &wgpu::Device,
    material: Option<usize>,
) -> Mesh {
    let index_buffer = if shape.indices.is_empty() {
        None
    } else {
        Some(
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Index Buffer"),
                contents: bytemuck::cast_slice(&shape.indices),
                usage: wgpu::BufferUsages::INDEX,
            }),
        )
    };
    let vertex_buffer = if shape.vertices.is_empty() {
        None
    } else {
        Some(
            device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(&shape.vertices),
                usage: wgpu::BufferUsages::VERTEX,
            }),
        )
    };

    Mesh {
        vertex_buffer,
        index_buffer,
        num_indices: shape.indices.len() as u32,
        num_vertices: shape.vertices.len() as u32,
        material,
    }
}
