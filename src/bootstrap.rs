use super::{gpu::Gpu, window::Window, EguiContext, Gui, Renderer, RendererDefault, Texture};
use std::time::Instant;
use winit::{
    event::*,
    event_loop::{ControlFlow, EventLoop},
};

pub struct Bootstrap {
    gpu: Gpu,
    window: Window,
    event_loop: EventLoop<()>,
    renderer: Box<dyn Renderer>,
    // TODO
    depth_texture: Option<Texture>,
    egui: Option<EguiContext>,
}

impl Bootstrap {
    pub async fn new(width: u32, height: u32, demo: &mut dyn Gui) -> Self {
        let event_loop = EventLoop::new();
        let window = Window::new(&event_loop, width, height, demo.win_resizable());
        let gpu = Gpu::new(&window).await;
        let mut init = demo.init(&gpu, &window);
        let egui = init
            .scenes
            .iter_mut()
            .find(|scene| scene.egui_ctx.is_some())
            .and_then(|scene| scene.egui_ctx.take());
        let mut depth_texture = None;

        let renderer = if let Some(custom_renderer) = init.custom_renderer {
            custom_renderer
        } else {
            depth_texture = Some(Texture::create_depth_texture(
                gpu.device(),
                gpu.config(),
                "depth_texture",
            ));

            Box::new(RendererDefault::new(gpu.device(), gpu.config(), init))
        };

        Self {
            gpu,
            window,
            event_loop,
            renderer,
            depth_texture,
            egui,
        }
    }

    fn resize(
        new_size: winit::dpi::PhysicalSize<u32>,
        window: &mut Window,
        gpu: &mut Gpu,
        renderer: &mut dyn Renderer,
        depth_texture: Option<&mut Texture>,
    ) {
        dbg!(new_size);
        gpu.resize(new_size);
        renderer.resize(new_size.width, new_size.height);
        window.resize(new_size);

        if let Some(depth_texture) = depth_texture {
            *depth_texture =
                Texture::create_depth_texture(gpu.device(), gpu.config(), "depth_texture");
        }
    }

    pub fn show(mut self, mut demo: Box<dyn Gui>) {
        let mut last_render_time = Instant::now();

        self.event_loop.run(move |event, _, control_flow| {
            if let Event::WindowEvent { ref event, .. } = event {
                if let Some(egui) = self.egui.as_mut() {
                    if egui.handle_event(self.window.win(), event) {
                        return;
                    }
                }
            }

            match event {
                Event::WindowEvent { ref event, .. } => match event {
                    WindowEvent::CloseRequested => {
                        *control_flow = ControlFlow::Exit;
                    }
                    WindowEvent::Resized(physical_size) => {
                        Self::resize(
                            *physical_size,
                            &mut self.window,
                            &mut self.gpu,
                            self.renderer.as_mut(),
                            self.depth_texture.as_mut(),
                        );
                    }
                    WindowEvent::ScaleFactorChanged { new_inner_size, .. } => {
                        Self::resize(
                            **new_inner_size,
                            &mut self.window,
                            &mut self.gpu,
                            self.renderer.as_mut(),
                            self.depth_texture.as_mut(),
                        );
                    }
                    WindowEvent::MouseInput { state, button, .. } => {
                        let button_index = match button {
                            MouseButton::Left => 0,
                            MouseButton::Middle => 1,
                            MouseButton::Right => 2,
                            _ => 0,
                        };
                        match state {
                            ElementState::Pressed => {
                                self.window.wuc.mouse_buttons[button_index] = true;
                            }
                            ElementState::Released => {
                                self.window.wuc.mouse_buttons[button_index] = false;
                            }
                        }
                    }
                    WindowEvent::CursorMoved { position, .. } => {
                        self.window
                            .wuc
                            .set_mouse_screen(position.x as f32, position.y as f32);
                    }
                    WindowEvent::MouseWheel { delta, .. } => match delta {
                        MouseScrollDelta::LineDelta(x, y) => {
                            self.window.wuc.set_mouse_scroll(*x, *y);
                        }
                        MouseScrollDelta::PixelDelta(pos) => {
                            self.window.wuc.set_mouse_scroll(pos.x as f32, pos.y as f32);
                        }
                    },
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                virtual_keycode: Some(virtual_code),
                                state,
                                ..
                            },
                        ..
                    } => match (virtual_code, state) {
                        (VirtualKeyCode::Escape, state) => {
                            self.window.wuc.is_escape_pressed = *state == ElementState::Pressed;
                        }
                        (vkc, state) => {
                            self.window
                                .wuc
                                .keys_pressed
                                .insert(*vkc, *state == ElementState::Pressed);
                        }
                    },
                    _ => {}
                },
                Event::RedrawRequested(_window_id) => {
                    let now = Instant::now();
                    let dt = now.duration_since(last_render_time).as_secs_f32();
                    last_render_time = now;

                    demo.each_frame(&self.gpu, self.renderer.as_mut(), &mut self.window.wuc);

                    self.renderer.show(
                        &self.gpu,
                        &self.window,
                        self.depth_texture.as_ref(),
                        self.egui.as_mut(),
                    );

                    self.window.wuc.update(dt);
                    if self.window.wuc.is_escape_pressed {
                        println!("EXIT");
                        *control_flow = ControlFlow::Exit;
                    }
                    if self.window.wuc.frame_count % 120 == 0 {
                        println!(
                            "fps: {}",
                            self.window.wuc.frame_count as f32 / self.window.wuc.total_time
                        );
                    }
                }
                Event::MainEventsCleared => {
                    self.window.win().request_redraw();
                }
                _ => {}
            }
        });
    }
}
