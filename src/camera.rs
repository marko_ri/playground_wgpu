use super::{factory, WindowUpdateContext};
use cgmath::*;
use std::f32::consts::FRAC_PI_2;
use wgpu::util::DeviceExt;

#[rustfmt::skip]
pub const OPENGL_TO_WGPU_MATRIX: Matrix4<f32> = Matrix4::new(
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 0.5, 0.5,
    0.0, 0.0, 0.0, 1.0,
);

const SAFE_FRAC_PI_2: f32 = FRAC_PI_2 - 0.0001;

pub struct Camera {
    position: Vector3<f32>,
    projection: Projection,
    yaw: Rad<f32>,
    pitch: Rad<f32>,
    // TODO:
    view_matrix: Matrix4<f32>,
}

impl Camera {
    pub fn new_ortho(position: Vector3<f32>, width: f32, height: f32) -> Self {
        Self {
            position,
            projection: Projection::ortho(width, height, 1.0),
            yaw: Rad(0.0),
            pitch: Rad(0.0),
            view_matrix: Matrix4::identity(),
        }
    }

    pub fn new_ortho_size(position: Vector3<f32>, width: f32, height: f32, size: f32) -> Self {
        Self {
            position,
            projection: Projection::ortho(width, height, size),
            yaw: Rad(0.0),
            pitch: Rad(0.0),
            view_matrix: Matrix4::identity(),
        }
    }

    pub fn new_perspective(position: Vector3<f32>, width: f32, height: f32) -> Self {
        Self {
            position,
            projection: Projection::perspective(width, height),
            yaw: Deg(-90.0).into(),
            pitch: Deg(-20.0).into(),
            view_matrix: Matrix4::identity(),
        }
    }

    pub fn calc_view_matrix(&self) -> Matrix4<f32> {
        self.projection
            .calc_view_matrix(self.position, self.pitch, self.yaw)
    }

    pub fn set_view_matrix(&mut self, view_matrix: Matrix4<f32>) {
        self.view_matrix = view_matrix;
    }

    pub fn view_projection_matrix(&self) -> Matrix4<f32> {
        self.projection.projection_matrix() * self.view_matrix
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.projection.resize(width, height);
    }

    pub fn view_matrix(&self) -> &Matrix4<f32> {
        &self.view_matrix
    }

    pub fn projection_matrix(&self) -> Matrix4<f32> {
        self.projection.projection_matrix()
    }

    pub fn set_position(&mut self, position: Vector3<f32>) {
        self.position = position;
    }
}

pub enum Projection {
    Perspective {
        aspect: f32,
        fovy: Rad<f32>,
        znear: f32,
        zfar: f32,
    },
    Ortho {
        left: f32,
        right: f32,
        bottom: f32,
        top: f32,
        near: f32,
        far: f32,
    },
}

impl Projection {
    // bottom_left = (0, 0), top_right = (width, height)
    fn ortho(width: f32, height: f32, size: f32) -> Self {
        let aspect = width / height;
        Projection::Ortho {
            left: 0.0,
            right: size * aspect,
            bottom: 0.0,
            top: size,
            near: -1.0,
            far: 1.0,
        }
    }

    fn perspective(width: f32, height: f32) -> Self {
        Projection::Perspective {
            aspect: width / height,
            fovy: Deg(45.0).into(),
            znear: 0.1,
            zfar: 100.0,
        }
    }

    fn calc_view_matrix(
        &self,
        position: Vector3<f32>,
        pitch: Rad<f32>,
        yaw: Rad<f32>,
    ) -> Matrix4<f32> {
        match self {
            Projection::Perspective { .. } => {
                let (sin_pitch, cos_pitch) = pitch.0.sin_cos();
                let (sin_yaw, cos_yaw) = yaw.sin_cos();

                Matrix4::look_to_rh(
                    point3(position.x, position.y, position.z),
                    Vector3::new(cos_pitch * cos_yaw, sin_pitch, cos_pitch * sin_yaw).normalize(),
                    Vector3::unit_y(),
                )
            }
            Projection::Ortho { .. } => {
                use std::ops::Neg;
                Matrix4::look_to_rh(
                    point3(position.x, position.y, position.z),
                    Vector3::unit_z().neg(),
                    Vector3::unit_y(),
                )
            }
        }
    }

    fn projection_matrix(&self) -> Matrix4<f32> {
        match self {
            Projection::Perspective {
                fovy,
                aspect,
                znear,
                zfar,
            } => OPENGL_TO_WGPU_MATRIX * perspective(*fovy, *aspect, *znear, *zfar),
            Projection::Ortho {
                left,
                right,
                bottom,
                top,
                near,
                far,
            } => OPENGL_TO_WGPU_MATRIX * ortho(*left, *right, *bottom, *top, *near, *far),
        }
    }

    pub fn resize(&mut self, _width: u32, _height: u32) {
        // let new_aspect = width as f32 / height as f32;
        // match self {
        //     Projection::Perspective { aspect, .. } => {
        //         *aspect = new_aspect;
        //     }
        //     Projection::Ortho { right, top, .. } => {
        //         *right = *top * new_aspect;
        //     }
        // }
    }
}

pub struct CameraController {
    amount_left: f32,
    amount_right: f32,
    amount_forward: f32,
    amount_backward: f32,
    amount_up: f32,
    amount_down: f32,
    rotate_horizontal: f32,
    rotate_vertical: f32,
    scroll: f32,
    speed: f32,
    sensitivity: f32,
}

impl CameraController {
    pub fn new(speed: f32, sensitivity: f32) -> Self {
        Self {
            amount_left: 0.0,
            amount_right: 0.0,
            amount_forward: 0.0,
            amount_backward: 0.0,
            amount_up: 0.0,
            amount_down: 0.0,
            rotate_horizontal: 0.0,
            rotate_vertical: 0.0,
            scroll: 0.0,
            speed,
            sensitivity,
        }
    }

    pub fn move_forward(&mut self, amount: f32) {
        self.amount_forward = amount;
    }

    pub fn move_backward(&mut self, amount: f32) {
        self.amount_backward = amount;
    }

    pub fn move_left(&mut self, amount: f32) {
        self.amount_left = amount;
    }

    pub fn move_right(&mut self, amount: f32) {
        self.amount_right = amount;
    }

    pub fn move_up(&mut self, amount: f32) {
        self.amount_up = amount;
    }

    pub fn move_down(&mut self, amount: f32) {
        self.amount_down = amount;
    }

    // pub fn process_mouse(&mut self, wuc: &WindowUpdateContext) {
    //     let mouse_screen = wuc.mouse_screen();
    //     self.rotate_horizontal = mouse_screen.x * 0.001;
    //     self.rotate_vertical = mouse_screen.y * 0.001;
    // }

    pub fn process_scroll(&mut self, wuc: &mut WindowUpdateContext) {
        self.scroll = -wuc.mouse_scroll().y * 0.5;
        wuc.set_mouse_scroll(0.0, 0.0);
    }

    fn update(&mut self, dt: f32, camera: &mut Camera) {
        // move forward/backward and left/right
        let (yaw_sin, yaw_cos) = camera.yaw.0.sin_cos();
        let forward = Vector3::new(yaw_cos, 0.0, yaw_sin).normalize();
        let right = Vector3::new(-yaw_sin, 0.0, yaw_cos).normalize();
        camera.position += forward * (self.amount_forward - self.amount_backward) * self.speed * dt;
        camera.position += right * (self.amount_right - self.amount_left) * self.speed * dt;

        // move in/out (aka. "zoom")
        let (pitch_sin, pitch_cos) = camera.pitch.0.sin_cos();
        let scrollward =
            Vector3::new(pitch_cos * yaw_cos, pitch_sin, pitch_cos * yaw_sin).normalize();
        camera.position += scrollward * self.scroll * self.speed * self.sensitivity * dt;
        self.scroll = 0.0;

        // move up/down. Since we do not use roll, we can just modify
        // the y coordinate directly
        camera.position.y += (self.amount_up - self.amount_down) * self.speed * dt;

        // rotate
        camera.yaw += Rad(self.rotate_horizontal) * self.sensitivity * dt;
        camera.pitch += Rad(-self.rotate_vertical) * self.sensitivity * dt;

        // If process_mouse isn't called every frame, these values
        // will not get set to zero, and the camera will rotate
        // when moving in a non cardinal direction.
        self.rotate_horizontal = 0.0;
        self.rotate_vertical = 0.0;

        // Keep the camera's angle from going too high/low.
        if camera.pitch < -Rad(SAFE_FRAC_PI_2) {
            camera.pitch = -Rad(SAFE_FRAC_PI_2);
        } else if camera.pitch > Rad(SAFE_FRAC_PI_2) {
            camera.pitch = Rad(SAFE_FRAC_PI_2);
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
pub struct CameraUniform {
    view_proj: [[f32; 4]; 4],
    view_position: [f32; 4],
}

impl CameraUniform {
    fn new(camera: &Camera) -> Self {
        Self {
            view_proj: camera.view_projection_matrix().into(),
            view_position: vec4(camera.position.x, camera.position.y, camera.position.y, 1.0)
                .into(),
        }
    }
}

pub struct CameraResource {
    buffer: wgpu::Buffer,
    bind_group: wgpu::BindGroup,
    bind_group_layout: wgpu::BindGroupLayout,
}

impl CameraResource {
    pub fn new(device: &wgpu::Device, uniform: CameraUniform) -> Self {
        let bind_group_layout = factory::create_camera_bg_layout(device);

        let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Camera Buffer"),
            contents: bytemuck::cast_slice(&[uniform]),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: buffer.as_entire_binding(),
            }],
            label: Some("camera_bind_group"),
        });

        Self {
            buffer,
            bind_group,
            bind_group_layout,
        }
    }

    fn update(&self, queue: &wgpu::Queue, uniform: CameraUniform) {
        queue.write_buffer(&self.buffer, 0, bytemuck::cast_slice(&[uniform]));
    }
}

pub struct CameraContainer {
    camera: Camera,
    controller: CameraController,
    resource: CameraResource,
}

impl CameraContainer {
    pub fn new(device: &wgpu::Device, camera: Camera) -> Self {
        let speed = 4.0;
        let sensitivity = 0.4;
        let controller = CameraController::new(speed, sensitivity);
        let uniform = CameraUniform::new(&camera);
        let resource = CameraResource::new(device, uniform);

        Self {
            camera,
            controller,
            resource,
        }
    }

    pub fn update(&mut self, queue: &wgpu::Queue, wuc: &WindowUpdateContext) {
        let camera = &mut self.camera;
        self.controller.update(wuc.delta_time, camera);
        let uniform = CameraUniform::new(camera);
        self.resource.update(queue, uniform);
    }

    pub fn bind_group_layout(&self) -> &wgpu::BindGroupLayout {
        &self.resource.bind_group_layout
    }

    pub fn bind_group(&self) -> &wgpu::BindGroup {
        &self.resource.bind_group
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        self.camera.resize(width, height);
    }

    pub fn camera(&self) -> &Camera {
        &self.camera
    }

    pub fn controller(&mut self) -> &mut CameraController {
        &mut self.controller
    }

    pub fn set_position(&mut self, position: Vector3<f32>) {
        self.camera.set_position(position);
    }

    pub fn set_view_matrix(&mut self, view_matrix: Matrix4<f32>) {
        self.camera.set_view_matrix(view_matrix);
    }
}
