use super::{
    model::{Index, Instance, Model, Vertex, VertexCol, VertexLayoutKind, VertexTex},
    resource, Gpu,
};
use cgmath::*;
use std::ops::Add;
use std::str::FromStr;

const MIN_LINE_WIDTH: f32 = 0.05;

#[derive(Default)]
pub struct Shape<V: Vertex> {
    pub vertices: Vec<V>,
    pub indices: Vec<Index>,
    pub texture_kind: Option<TextureKind>,
    pub primitive_topology: wgpu::PrimitiveTopology,
}

pub enum TextureKind {
    Image(String),
    Video { width: u32, height: u32 },
}

impl FromStr for TextureKind {
    type Err = Box<dyn std::error::Error + Send + Sync>;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(TextureKind::Image(s.to_string()))
    }
}

impl<V: Vertex> Shape<V> {
    pub fn transform(&mut self, matrix: cgmath::Matrix4<f32>) {
        for vertex in self.vertices.iter_mut() {
            vertex.transform(matrix);
        }
    }

    pub fn vertex_layout_kind(&self) -> VertexLayoutKind {
        self.vertices[0].vertex_layout_kind()
    }
}

pub fn pentagon_col() -> Shape<VertexCol> {
    let vertices = vec![
        VertexCol {
            positions: [-0.0868241, 0.49240386, 0.0],
            color: [1.0, 0.0, 0.0, 0.0],
        },
        VertexCol {
            positions: [-0.49513406, 0.06958647, 0.0],
            color: [0.0, 1.0, 0.0, 0.0],
        },
        VertexCol {
            positions: [-0.21918549, -0.44939706, 0.0],
            color: [0.0, 0.0, 1.0, 0.0],
        },
        VertexCol {
            positions: [0.35966998, -0.3473291, 0.0],
            color: [0.0; 4],
        },
        VertexCol {
            positions: [0.44147372, 0.2347359, 0.0],
            color: [0.0; 4],
        },
    ];

    Shape {
        vertices,
        indices: vec![0, 1, 4, 1, 2, 4, 2, 3, 4],
        ..Default::default()
    }
}

pub fn pentagon_tex(texture_kind: Option<TextureKind>) -> Shape<VertexTex> {
    let vertices = vec![
        VertexTex {
            positions: [-0.0868241, 0.49240386, 0.0],
            tex_coords: [0.4131759, 0.00759614],
            ..Default::default()
        },
        VertexTex {
            positions: [-0.49513406, 0.06958647, 0.0],
            tex_coords: [0.0048659444, 0.43041354],
            ..Default::default()
        },
        VertexTex {
            positions: [-0.21918549, -0.44939706, 0.0],
            tex_coords: [0.28081453, 0.949397],
            ..Default::default()
        },
        VertexTex {
            positions: [0.35966998, -0.3473291, 0.0],
            tex_coords: [0.85967, 0.84732914],
            ..Default::default()
        },
        VertexTex {
            positions: [0.44147372, 0.2347359, 0.0],
            tex_coords: [0.9414737, 0.2652641],
            ..Default::default()
        },
    ];

    Shape {
        vertices,
        indices: vec![0, 1, 4, 1, 2, 4, 2, 3, 4],
        texture_kind,
        ..Default::default()
    }
}

pub fn triangle() -> Shape<VertexCol> {
    let vertices = vec![
        VertexCol {
            positions: [0.0, 0.5, 0.0],
            ..Default::default()
        },
        VertexCol {
            positions: [-0.5, -0.5, 0.0],
            ..Default::default()
        },
        VertexCol {
            positions: [0.5, -0.5, 0.0],
            ..Default::default()
        },
    ];

    Shape {
        vertices,
        ..Default::default()
    }
}

pub fn rectangle(
    center: Vector2<f32>,
    width: f32,
    height: f32,
    color: [f32; 4],
) -> Shape<VertexCol> {
    let bl = vec2(center.x - width / 2.0, center.y - height / 2.0);
    let tr = vec2(center.x + width / 2.0, center.y + height / 2.0);

    let vertices = vec![
        VertexCol {
            positions: [bl.x, tr.y, 0.0],
            color,
        },
        VertexCol {
            positions: [bl.x, bl.y, 0.0],
            color,
        },
        VertexCol {
            positions: [tr.x, bl.y, 0.0],
            color,
        },
        VertexCol {
            positions: [tr.x, tr.y, 0.0],
            color,
        },
    ];

    Shape {
        vertices,
        indices: vec![0, 1, 3, 3, 1, 2],
        ..Default::default()
    }
}

pub fn rectangle_fan(
    center: Vector2<f32>,
    width: f32,
    height: f32,
    color: [f32; 4],
) -> Shape<VertexCol> {
    let bl = vec2(center.x - width / 2.0, center.y - height / 2.0);
    let tr = vec2(center.x + width / 2.0, center.y + height / 2.0);

    let vertices = vec![
        VertexCol {
            positions: [bl.x, tr.y, 0.0],
            color,
        },
        VertexCol {
            positions: [bl.x, bl.y, 0.0],
            color,
        },
        VertexCol {
            positions: [tr.x, tr.y, 0.0],
            color,
        },
        VertexCol {
            positions: [tr.x, bl.y, 0.0],
            color,
        },
    ];

    Shape {
        vertices,
        primitive_topology: wgpu::PrimitiveTopology::TriangleStrip,
        ..Default::default()
    }
}

pub fn rectangle_frame(
    r_center: Vector2<f32>,
    r_width: f32,
    r_height: f32,
    color: &[f32; 4],
    l_width: Option<f32>,
) -> Shape<VertexCol> {
    let bl = vec2(r_center.x - r_width / 2.0, r_center.y - r_height / 2.0);
    let tr = vec2(r_center.x + r_width / 2.0, r_center.y + r_height / 2.0);
    let mut result = Shape::default();
    let num_vert = 4;

    let mut bottom = line_rect(&bl, &vec2(tr.x, bl.y), color, l_width);
    result.vertices.append(&mut bottom.vertices);
    result.indices.append(&mut bottom.indices);

    let mut right = line_rect(&vec2(tr.x, bl.y), &tr, color, l_width);
    result.vertices.append(&mut right.vertices);
    for idx in right.indices.into_iter() {
        result.indices.push(idx + num_vert);
    }

    let mut up = line_rect(&tr, &vec2(bl.x, tr.y), color, l_width);
    result.vertices.append(&mut up.vertices);
    for idx in up.indices.into_iter() {
        result.indices.push(idx + 2 * num_vert);
    }

    let mut left = line_rect(&vec2(bl.x, tr.y), &bl, color, l_width);
    result.vertices.append(&mut left.vertices);
    for idx in left.indices.into_iter() {
        result.indices.push(idx + 3 * num_vert);
    }

    result
}

pub fn rectangle_transparent() -> Shape<VertexCol> {
    let vertices = vec![
        VertexCol {
            positions: [-0.5, 0.5, 0.0],
            color: [0.0; 4],
        },
        VertexCol {
            positions: [-0.5, -0.5, 0.0],
            color: [0.0; 4],
        },
        VertexCol {
            positions: [0.5, -0.5, 0.0],
            color: [0.0; 4],
        },
        VertexCol {
            positions: [0.5, 0.5, 0.0],
            color: [0.0; 4],
        },
    ];

    Shape {
        vertices,
        indices: vec![0, 1, 3, 3, 1, 2],
        ..Default::default()
    }
}

pub fn rectangle_tex(
    center: Vector2<f32>,
    width: f32,
    height: f32,
    texture_kind: Option<TextureKind>,
) -> Shape<VertexTex> {
    let bl = vec2(center.x - width / 2.0, center.y - height / 2.0);
    let tr = vec2(center.x + width / 2.0, center.y + height / 2.0);

    let vertices = vec![
        VertexTex {
            positions: [bl.x, tr.y, 0.0],
            tex_coords: [0.0, 0.0],
            normals: [0.0; 3],
        },
        VertexTex {
            positions: [bl.x, bl.y, 0.0],
            tex_coords: [0.0, 1.0],
            normals: [0.0; 3],
        },
        VertexTex {
            positions: [tr.x, bl.y, 0.0],
            tex_coords: [1.0, 1.0],
            normals: [0.0; 3],
        },
        VertexTex {
            positions: [tr.x, tr.y, 0.0],
            tex_coords: [1.0, 0.0],
            normals: [0.0; 3],
        },
    ];

    Shape {
        vertices,
        indices: vec![0, 1, 3, 3, 1, 2],
        texture_kind,
        ..Default::default()
    }
}

pub fn circle(center: Vector2<f32>, radius: f32, sides: u8, color: [f32; 4]) -> Shape<VertexCol> {
    let mut ort = vec2(radius, 0.0);
    let deg = Deg(360.0 / sides as f32);
    let rotate: Basis2<f32> = Rotation2::from_angle(Rad::from(deg));
    let mut vertices = Vec::new();

    for _ in 0..sides {
        let p = center.add(&ort);
        vertices.push(p);
        ort = rotate.rotate_vector(ort);
    }
    // close the circle
    vertices.push(vertices[0]);

    lines_strip(vertices, color)
}

pub fn circle_fan(
    center: Vector2<f32>,
    radius: f32,
    sides: u8,
    color: [f32; 4],
    width: f32,
) -> Shape<VertexCol> {
    let inner_scale = 1.0 - width;
    let mut vertices = Vec::new();
    let two_pi = 2.0 * std::f32::consts::PI;

    for i in 0..sides {
        let x = i as f32 * two_pi / sides as f32;
        let cos = x.cos();
        let sin = x.sin();
        let p_outer = vec2(radius * cos, radius * sin);
        let p_inner = vec2(radius * inner_scale * cos, radius * inner_scale * sin);

        let one = VertexCol {
            positions: [center.x + p_inner.x, center.y + p_inner.y, 0.0],
            color,
        };
        vertices.push(one);
        let two = VertexCol {
            positions: [center.x + p_outer.x, center.y + p_outer.y, 0.0],
            color,
        };
        vertices.push(two);
    }
    vertices.push(vertices[0]);
    vertices.push(vertices[1]);

    Shape {
        vertices,
        primitive_topology: wgpu::PrimitiveTopology::TriangleStrip,
        ..Default::default()
    }
}

fn lines_strip(pos: Vec<Vector2<f32>>, color: [f32; 4]) -> Shape<VertexCol> {
    let mut vertices = Vec::new();
    for i in 0..pos.len() - 1 {
        let start = VertexCol {
            positions: [pos[i].x, pos[i].y, 0.0],
            color,
        };
        vertices.push(start);
        let end = VertexCol {
            positions: [pos[i + 1].x, pos[i + 1].y, 0.0],
            color,
        };
        vertices.push(end);
    }

    Shape {
        vertices,
        primitive_topology: wgpu::PrimitiveTopology::LineStrip,
        ..Default::default()
    }
}

pub fn line_rect(
    start: &Vector2<f32>,
    end: &Vector2<f32>,
    color: &[f32; 4],
    width: Option<f32>,
) -> Shape<VertexCol> {
    let half_width = if let Some(w) = width {
        f32::max(w * 0.5, MIN_LINE_WIDTH)
    } else {
        MIN_LINE_WIDTH
    };
    let mut vertices = Vec::new();

    let dx = end.x - start.x;
    let dy = end.y - start.y;
    // (x.powi(2) + y.powi(2)).sqrt()
    let l = dx.hypot(dy);
    let u = dx * half_width / l;
    let v = dy * half_width / l;

    vertices.push(VertexCol {
        positions: [start.x + v, start.y - u, 0.0],
        color: *color,
    });
    vertices.push(VertexCol {
        positions: [start.x - v, start.y + u, 0.0],
        color: *color,
    });
    vertices.push(VertexCol {
        positions: [end.x - v, end.y + u, 0.0],
        color: *color,
    });
    vertices.push(VertexCol {
        positions: [end.x + v, end.y - u, 0.0],
        color: *color,
    });

    Shape {
        vertices,
        indices: vec![2, 1, 0, 2, 0, 3],
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        ..Default::default()
    }
}

pub fn line_fan(
    start: &Vector2<f32>,
    end: &Vector2<f32>,
    width: f32,
    color: &[f32; 4],
) -> Shape<VertexCol> {
    let half_width = width * 0.5;

    let dx = end.x - start.x;
    let dy = end.y - start.y;
    // (x.powi(2) + y.powi(2)).sqrt()
    let l = dx.hypot(dy);
    let u = dx * half_width / l;
    let v = dy * half_width / l;

    let vertices = vec![
        VertexCol {
            positions: [start.x - v, start.y + u, 0.0],
            color: *color,
        },
        VertexCol {
            positions: [end.x - v, end.y + u, 0.0],
            color: *color,
        },
        VertexCol {
            positions: [start.x + v, start.y - u, 0.0],
            color: *color,
        },
        VertexCol {
            positions: [end.x + v, end.y - u, 0.0],
            color: *color,
        },
    ];

    Shape {
        vertices,
        primitive_topology: wgpu::PrimitiveTopology::TriangleStrip,
        ..Default::default()
    }
}

// Grid Model made by instancing rectangle_frame
pub fn grid(gpu: &Gpu, center: Vector2<f32>, num_cells: u16, cell_size: f32) -> Model {
    // cell at the bottom left of the grid
    let cell_center = center
        - vec2(
            0.5 * num_cells as f32 * cell_size,
            0.5 * num_cells as f32 * cell_size,
        )
        + vec2(0.5 * cell_size, 0.5 * cell_size);
    let cell = rectangle_frame(cell_center, cell_size, cell_size, &[1.0; 4], None);

    let mut instances = Vec::new();
    for y in 0..num_cells {
        for x in 0..num_cells {
            let translate = vec3(x as f32 * cell_size, y as f32 * cell_size, 0.0);
            instances.push(Instance { translate });
        }
    }

    let mut result = resource::shape_to_model(cell, gpu.device(), gpu.queue());
    result.add_instances(gpu.device(), instances);
    result
}

pub fn plane(
    width: f32,
    height: f32,
    rows: u32,
    columns: u32,
    texture_name: &str,
) -> Shape<VertexTex> {
    let vertices = plane_vertices(width, height, rows, columns);
    let indices = plane_indices(rows, columns);

    Shape {
        vertices,
        indices,
        primitive_topology: wgpu::PrimitiveTopology::TriangleList,
        texture_kind: Some(TextureKind::Image(texture_name.to_string())),
    }
}

fn plane_vertices(width: f32, height: f32, rows: u32, columns: u32) -> Vec<VertexTex> {
    let row_height = height / rows as f32;
    let col_width = width / columns as f32;

    let mut result = Vec::new();
    for row in 0..=rows {
        let y = row as f32 * row_height;
        for col in 0..=columns {
            let x = col as f32 * col_width;

            let mut vertex = VertexTex::default();
            vertex.positions[0] = x;
            vertex.positions[1] = y;
            vertex.positions[2] = 0.0;

            vertex.normals[0] = 0.0;
            vertex.normals[1] = 0.0;
            vertex.normals[2] = 1.0;

            vertex.tex_coords[0] = col as f32 / columns as f32;
            vertex.tex_coords[1] = 1.0 - row as f32 / rows as f32;

            result.push(vertex);
        }
    }

    result
}

fn plane_indices(rows: u32, columns: u32) -> Vec<Index> {
    let columns_offset = columns + 1;

    let mut result = Vec::new();
    for row in 0..rows {
        for col in 0..columns {
            let left_bottom = columns_offset * row + col;
            let right_bottom = columns_offset * row + (col + 1);
            let left_up = columns_offset * (row + 1) + col;
            let right_up = columns_offset * (row + 1) + (col + 1);

            result.push(left_up);
            result.push(left_bottom);
            result.push(right_bottom);

            result.push(right_up);
            result.push(left_up);
            result.push(right_bottom);
        }
    }

    result
}
