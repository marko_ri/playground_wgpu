use super::{gpu::Gpu, scene::Scene, DEPTH_FORMAT};
use egui_wgpu::renderer::ScreenDescriptor;
use winit::{event::WindowEvent, window::Window};

pub struct EguiContext {
    pub context: egui::Context,
    state: egui_winit::State,
    renderer: egui_wgpu::Renderer,
    pub content: fn(&egui::Context, &mut Scene),
    screen_descriptor: ScreenDescriptor,
    clipped_primitive: Vec<egui::ClippedPrimitive>,
}

impl EguiContext {
    pub fn new(gpu: &Gpu, window: &Window, content: fn(&egui::Context, &mut Scene)) -> Self {
        let mut state = egui_winit::State::new(&window);
        state.set_pixels_per_point(window.scale_factor() as f32);

        let renderer =
            egui_wgpu::Renderer::new(gpu.device(), gpu.config().format, Some(DEPTH_FORMAT), 1);

        let context = egui::Context::default();

        let style = egui::Style {
            text_styles: [
                (
                    egui::TextStyle::Heading,
                    egui::FontId::new(20.0, egui::FontFamily::Proportional),
                ),
                (
                    egui::TextStyle::Body,
                    egui::FontId::new(15.0, egui::FontFamily::Proportional),
                ),
                (
                    egui::TextStyle::Monospace,
                    egui::FontId::new(15.0, egui::FontFamily::Proportional),
                ),
                (
                    egui::TextStyle::Button,
                    egui::FontId::new(15.0, egui::FontFamily::Proportional),
                ),
                (
                    egui::TextStyle::Small,
                    egui::FontId::new(15.0, egui::FontFamily::Proportional),
                ),
            ]
            .into(),
            ..Default::default()
        };
        context.set_style(style);

        let screen_descriptor = ScreenDescriptor {
            size_in_pixels: [gpu.config().width, gpu.config().height],
            pixels_per_point: window.scale_factor() as f32,
        };

        Self {
            context,
            state,
            renderer,
            content,
            screen_descriptor,
            clipped_primitive: Vec::new(),
        }
    }

    pub fn handle_event(&mut self, window: &Window, event: &WindowEvent) -> bool {
        let response = self.state.on_event(&self.context, event);
        if response.repaint {
            window.request_redraw();
        }
        response.consumed
    }

    pub fn update(
        &mut self,
        gpu: &Gpu,
        encoder: &mut wgpu::CommandEncoder,
        window: &Window,
        scene: &mut Scene,
    ) -> Vec<egui::TextureId> {
        let input = self.state.take_egui_input(window);
        self.context.begin_frame(input);

        (self.content)(&self.context, scene);

        let output = self.context.end_frame();
        self.clipped_primitive = self.context.tessellate(output.shapes);

        self.state
            .handle_platform_output(window, &self.context, output.platform_output);

        self.screen_descriptor = ScreenDescriptor {
            size_in_pixels: [gpu.config().width, gpu.config().height],
            pixels_per_point: window.scale_factor() as f32,
        };

        for (id, delta) in &output.textures_delta.set {
            self.renderer
                .update_texture(gpu.device(), gpu.queue(), *id, delta);
        }

        self.renderer.update_buffers(
            gpu.device(),
            gpu.queue(),
            encoder,
            &self.clipped_primitive,
            &self.screen_descriptor,
        );

        output.textures_delta.free
    }

    pub fn draw<'a>(&'a mut self, render_pass: &mut wgpu::RenderPass<'a>) {
        self.renderer.render(
            render_pass,
            &self.clipped_primitive,
            &self.screen_descriptor,
        );
    }

    pub fn free(&mut self, egui_textures: Option<Vec<egui::TextureId>>) {
        if let Some(textures) = egui_textures {
            for id in &textures {
                self.renderer.free_texture(id);
            }
        }
    }
}
