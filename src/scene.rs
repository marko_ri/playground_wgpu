use super::{
    camera::CameraContainer,
    factory,
    light::LightContainer,
    model::{Material, Mesh, Model},
    window::Window,
    BindGroupKind, InitBuffer,
};
use std::collections::HashMap;
use wgpu::util::DeviceExt;

#[derive(Default)]
pub struct Scene {
    pub name: String,
    pub pipelines: Vec<Pipeline>,
    pub light_container: Option<LightContainer>,
    pub background_color: wgpu::Color,
    pub camera_container: Option<CameraContainer>,
    pub egui_state: Option<EguiState>,
}

impl Scene {
    pub fn update(&mut self, queue: &wgpu::Queue, window: &Window) {
        if let Some(camera_container) = self.camera_container.as_mut() {
            camera_container.update(queue, &window.wuc);
        }

        if let Some(egui) = self.egui_state.as_ref() {
            let data = egui.as_slice().to_vec();
            let pipeline_name = egui.pipeline_name.clone();
            self.set_uniform_data(&pipeline_name, queue, &data);
        }

        if let Some(light_container) = &mut self.light_container {
            light_container.update(queue);
        }
    }

    pub fn set_uniform_data(&mut self, pipeline_name: &str, queue: &wgpu::Queue, data: &[f32]) {
        let uniform_resource = self
            .pipelines
            .iter_mut()
            .find(|p| p.name == pipeline_name)
            .and_then(|p| p.uniform_resource.as_mut())
            .unwrap();
        uniform_resource.update(queue, data);
    }

    pub fn set_uniform_data_idx(
        &mut self,
        pipeline_name: &str,
        queue: &wgpu::Queue,
        data: &[f32],
        idx: usize,
    ) {
        let uniform_resource = self
            .pipelines
            .iter_mut()
            .find(|p| p.name == pipeline_name)
            .and_then(|p| p.uniform_resource.as_mut())
            .unwrap();
        uniform_resource.update_idx(queue, data, idx);
    }

    pub fn set_storage_data(&mut self, pipeline_name: &str, queue: &wgpu::Queue, data: &[f32]) {
        let storage_resource = self
            .pipelines
            .iter_mut()
            .find(|p| p.name == pipeline_name)
            .and_then(|p| p.storage_resource.as_mut())
            .unwrap();
        storage_resource.update(queue, data);
    }

    pub fn egui_state(&mut self, pipeline_name: &str) -> &mut [f32] {
        if let Some(egui_state) = self
            .egui_state
            .as_mut()
            .filter(|e| e.pipeline_name == pipeline_name)
        {
            egui_state.as_slice_mut()
        } else {
            panic!("No Egui For Pipeline: {}", pipeline_name)
        }
    }

    pub fn resize(&mut self, width: u32, height: u32) {
        if let Some(camera_container) = self.camera_container.as_mut() {
            camera_container.resize(width, height);
        }
    }

    pub fn draw<'a>(&'a mut self, render_pass: &mut wgpu::RenderPass<'a>) {
        for pipeline in self.pipelines.iter() {
            if !pipeline.is_visible {
                continue;
            }

            render_pass.set_pipeline(&pipeline.render_pipeline);

            if let Some(uniforms_bg_idx) = pipeline.bind_group_indices.get(&BindGroupKind::Uniform)
            {
                let bind_group = &pipeline.uniform_resource.as_ref().unwrap().bind_group;
                render_pass.set_bind_group(*uniforms_bg_idx as u32, bind_group, &[]);
            }

            if let Some(storage_bg_idx) = pipeline.bind_group_indices.get(&BindGroupKind::Storage) {
                render_pass.set_bind_group(
                    *storage_bg_idx as u32,
                    &pipeline.storage_resource.as_ref().unwrap().bind_group,
                    &[],
                );
            }

            if let Some(model) = pipeline.model.as_ref() {
                let (num_instances, instance_buffer) = if model.instance_buffer.is_none() {
                    (1, None)
                } else {
                    let ib = model.instance_buffer.as_ref().unwrap();
                    (ib.0, Some(&ib.1))
                };
                for mesh in model.meshes.iter() {
                    let material = if let Some(idx) = mesh.material {
                        model.materials.get(idx)
                    } else {
                        None
                    };
                    self.draw_mesh(
                        render_pass,
                        mesh,
                        material,
                        0..num_instances,
                        instance_buffer,
                        &pipeline.bind_group_indices,
                    );
                }
            } else {
                if let Some(cam) = self.camera_container.as_ref() {
                    render_pass.set_bind_group(0, cam.bind_group(), &[]);
                }
                render_pass.draw(0..3, 0..1);
            }
        }

        if let Some(light_container) = self.light_container.as_ref() {
            render_pass.set_pipeline(light_container.render_pipeline());
            let model = light_container.model();
            for mesh in model.meshes.iter() {
                let material = if let Some(idx) = mesh.material {
                    model.materials.get(idx)
                } else {
                    None
                };
                self.draw_mesh(
                    render_pass,
                    mesh,
                    material,
                    0..1,
                    None,
                    light_container.bind_group_indices(),
                );
            }
        }
    }

    fn draw_mesh<'a>(
        &'a self,
        render_pass: &mut wgpu::RenderPass<'a>,
        mesh: &'a Mesh,
        material: Option<&'a Material>,
        instance_range: std::ops::Range<u32>,
        instance_buffer: Option<&'a wgpu::Buffer>,
        bind_group_indices: &HashMap<BindGroupKind, u8>,
    ) {
        if let Some(vertex_buffer) = &mesh.vertex_buffer {
            render_pass.set_vertex_buffer(0, vertex_buffer.slice(..));
        }

        if let Some(instance_buffer) = instance_buffer {
            render_pass.set_vertex_buffer(1, instance_buffer.slice(..));
        }

        if let Some(camera_bg_idx) = bind_group_indices.get(&BindGroupKind::Camera) {
            render_pass.set_bind_group(
                *camera_bg_idx as u32,
                self.camera_container.as_ref().unwrap().bind_group(),
                &[],
            );
        }

        if let Some(texture_bg_idx) = bind_group_indices.get(&BindGroupKind::Texture) {
            render_pass.set_bind_group(*texture_bg_idx as u32, &material.unwrap().bind_group, &[]);
        }

        if let Some(light_bg_idx) = bind_group_indices.get(&BindGroupKind::Light) {
            render_pass.set_bind_group(
                *light_bg_idx as u32,
                self.light_container.as_ref().unwrap().bind_group(),
                &[],
            );
        }

        if let Some(index_buffer) = &mesh.index_buffer {
            render_pass.set_index_buffer(index_buffer.slice(..), wgpu::IndexFormat::Uint32);
            render_pass.draw_indexed(0..mesh.num_indices, 0, instance_range);
        } else {
            render_pass.draw(0..mesh.num_vertices, instance_range);
        }
    }

    pub fn set_model(&mut self, pipeline_name: &str, model: Model) {
        let pipeline = self
            .pipelines
            .iter_mut()
            .find(|p| p.name == pipeline_name)
            .unwrap();
        pipeline.model = Some(model);
    }

    pub fn add_mesh_to_model(&mut self, pipeline_name: &str, mesh: Mesh) {
        let model = self
            .pipelines
            .iter_mut()
            .find(|p| p.name == pipeline_name)
            .and_then(|p| p.model.as_mut())
            .unwrap();
        model.meshes.push(mesh);
    }

    pub fn camera_container(&mut self) -> Option<&mut CameraContainer> {
        self.camera_container.as_mut()
    }

    pub fn light_container(&mut self) -> Option<&mut LightContainer> {
        self.light_container.as_mut()
    }
}

#[derive(Debug)]
pub struct UniformResource {
    buffers: Vec<wgpu::Buffer>,
    bind_group: wgpu::BindGroup,
    pub bind_group_layout: wgpu::BindGroupLayout,
}

impl UniformResource {
    pub fn new(device: &wgpu::Device, uniforms: &[InitBuffer]) -> Self {
        let buf_size: Vec<usize> = uniforms.iter().map(|u| u.len()).collect();
        let bind_group_layout = factory::create_uniform_bg_layout(device, &buf_size);

        let buffers: Vec<wgpu::Buffer> = uniforms
            .iter()
            .enumerate()
            .map(|(i, uniform)| {
                device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("uniform_buffer: {}", i)),
                    contents: bytemuck::cast_slice(uniform.as_slice()),
                    usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
                })
            })
            .collect();

        let entries: Vec<wgpu::BindGroupEntry> = uniforms
            .iter()
            .enumerate()
            .map(|(i, _uniform)| wgpu::BindGroupEntry {
                binding: i as u32,
                resource: buffers[i].as_entire_binding(),
            })
            .collect();

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &entries,
            label: Some("uniform_bind_group"),
        });

        Self {
            buffers,
            bind_group,
            bind_group_layout,
        }
    }

    fn update(&mut self, queue: &wgpu::Queue, data: &[f32]) {
        queue.write_buffer(self.buffers.first().unwrap(), 0, bytemuck::cast_slice(data));
    }

    fn update_idx(&mut self, queue: &wgpu::Queue, data: &[f32], idx: usize) {
        queue.write_buffer(&self.buffers[idx], 0, bytemuck::cast_slice(data));
    }
}

pub struct StorageResource {
    buffer: wgpu::Buffer,
    bind_group: wgpu::BindGroup,
    pub bind_group_layout: wgpu::BindGroupLayout,
}

impl StorageResource {
    pub fn new(device: &wgpu::Device, data: &InitBuffer) -> Self {
        let bind_group_layout = factory::create_storage_bg_layout(device, data.len());
        let buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Storage buffer"),
            contents: bytemuck::cast_slice(data.as_slice()),
            usage: wgpu::BufferUsages::VERTEX
                | wgpu::BufferUsages::STORAGE
                | wgpu::BufferUsages::COPY_DST,
        });
        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: buffer.as_entire_binding(),
            }],
            label: Some("storage_bind_group"),
        });

        Self {
            buffer,
            bind_group,
            bind_group_layout,
        }
    }

    fn update(&mut self, queue: &wgpu::Queue, data: &[f32]) {
        queue.write_buffer(&self.buffer, 0, bytemuck::cast_slice(data));
    }
}

pub struct Pipeline {
    pub name: String,
    pub shader_name: String,
    pub render_pipeline: wgpu::RenderPipeline,
    pub model: Option<Model>,
    pub uniform_resource: Option<UniformResource>,
    pub bind_group_indices: HashMap<BindGroupKind, u8>,
    pub is_visible: bool,
    pub storage_resource: Option<StorageResource>,
}

pub struct EguiState {
    uniform: InitBuffer,
    pub pipeline_name: String,
    pub scene_name: String,
}

impl EguiState {
    pub fn new(scene_name: &str, pipeline_name: &str, init_data: Vec<f32>) -> Self {
        Self {
            uniform: InitBuffer::new(init_data),
            pipeline_name: pipeline_name.to_string(),
            scene_name: scene_name.to_string(),
        }
    }

    fn as_slice(&self) -> &[f32] {
        self.uniform.as_slice()
    }

    fn as_slice_mut(&mut self) -> &mut [f32] {
        self.uniform.as_slice_mut()
    }
}
