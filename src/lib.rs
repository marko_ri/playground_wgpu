#![allow(dead_code)]

mod gpu;
pub use gpu::Gpu;
mod window;
pub use window::{Window, WindowUpdateContext};
pub mod factory;
mod model;
pub use model::{Instance, InstanceRaw, Model, Vertex, VertexCol, VertexLayoutKind, VertexTex};
mod renderer_default;
pub use renderer_default::RendererDefault;
mod light;
pub use light::Light;
mod scene;
pub use scene::{EguiState, Scene};
mod bootstrap;
pub use bootstrap::Bootstrap;
mod camera;
pub mod shape;
pub use camera::{Camera, CameraController};
pub mod collision;
pub mod resource;
mod texture;
pub use texture::Texture;
mod egui_context;
pub use egui_context::EguiContext;
pub mod net;
pub mod tile;
pub mod video_reader;

pub type GenericResult<T> = Result<T, Box<dyn std::error::Error + Send + Sync>>;
pub const DEPTH_FORMAT: wgpu::TextureFormat = wgpu::TextureFormat::Depth32Float;

pub trait Renderer {
    fn show(
        &mut self,
        gpu: &Gpu,
        window: &Window,
        depth_texture: Option<&Texture>,
        egui: Option<&mut EguiContext>,
    );
    fn resize(&mut self, width: u32, height: u32);
    fn active_scene(&self) -> &Scene;
    fn active_scene_mut(&mut self) -> &mut Scene;
    fn set_active_scene(&mut self, idx: usize);
}

#[derive(Default)]
pub struct Init {
    pub scenes: Vec<InitScene>,
    pub custom_renderer: Option<Box<dyn Renderer>>,
}

#[derive(Default)]
pub struct InitScene {
    pub name: String,
    pub camera: Option<Camera>,
    pub light: Option<Light>,
    pub render_pipelines: Vec<InitPipeline>,
    pub egui_ctx: Option<EguiContext>,
    pub egui_state: Option<EguiState>,
}

#[derive(Default)]
pub struct InitPipeline {
    pub pipeline_name: String,
    pub shader_name: String,
    pub model: Option<model::Model>,
    pub uniforms: Vec<InitBuffer>,
    pub storage: Option<InitBuffer>,
    pub hide: bool,
}

#[derive(Clone)]
pub struct InitBuffer {
    buffer: Vec<f32>,
}

impl InitBuffer {
    pub fn new(mut data: Vec<f32>) -> Self {
        let real_size = data.len();
        // TODO: set the size to the next power of 2
        let aligned_size = 2_usize.pow((real_size as f32).log2().ceil() as u32);

        let buffer = if real_size == aligned_size {
            data
        } else {
            data.resize(aligned_size, 0.0);
            data
        };

        Self { buffer }
    }

    pub fn as_slice(&self) -> &[f32] {
        &self.buffer[..]
    }

    pub fn as_slice_mut(&mut self) -> &mut [f32] {
        &mut self.buffer[..]
    }

    pub fn set_val(&mut self, idx: usize, val: f32) {
        self.buffer[idx] = val;
    }

    pub fn val(&self, idx: usize) -> f32 {
        self.buffer[idx]
    }

    pub fn len(&self) -> usize {
        self.buffer.len()
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub enum BindGroupKind {
    Camera,
    Texture,
    Light,
    Uniform,
    Storage,
}

pub trait Gui: Send {
    fn win_resizable(&self) -> bool {
        true
    }
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init;
    fn each_frame(&mut self, gpu: &Gpu, renderer: &mut dyn Renderer, wuc: &mut WindowUpdateContext);
}
