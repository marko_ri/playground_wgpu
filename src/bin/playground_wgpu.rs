mod demo;

use playground_wgpu::{Bootstrap, Gui};
use tokio::runtime::Builder;

fn main() {
    std::env::set_var("RUST_LOG", "INFO");
    env_logger::init();

    let win_width = 768;
    let win_height = 480;
    let mut demo = create_demo(win_width, win_height);

    let rt = Builder::new_multi_thread().enable_all().build().unwrap();
    let bootstrap = rt.block_on(Bootstrap::new(win_width, win_height, demo.as_mut()));
    bootstrap.show(demo);
}

fn create_demo(win_width: u32, win_height: u32) -> Box<dyn Gui> {
    let args: Vec<String> = std::env::args().collect();
    let name = args.get(1).map_or("learn_wgpu", String::as_str);

    match name {
        "learn_wgpu" => Box::<demo::learn_wgpu::Demo>::default(),
        "sobel" => Box::<demo::sobel::Demo>::default(),
        "circle" => Box::<demo::circle::Demo>::default(),
        "grid" => Box::<demo::grid::Demo>::default(),
        "font" => Box::<demo::font::Demo>::default(),
        "boid" => Box::<demo::boid::Demo>::default(),
        "bouncy" => Box::<demo::bouncy::Demo>::default(),
        "bezier" => Box::<demo::bezier::Demo>::default(),
        "verlet" => Box::<demo::verlet::Demo>::default(),
        "video" => Box::<demo::video::Demo>::default(),
        "dda" => Box::new(demo::dda::Demo::new(win_width, win_height)),
        "gerstner" => Box::<demo::gerstner::Demo>::default(),
        "net_client" => {
            let port = args.get(2).map_or("8080", String::as_str);
            Box::new(demo::net_client::Demo::new(port))
        }
        _ => panic!("Unknown Argument"),
    }
}
