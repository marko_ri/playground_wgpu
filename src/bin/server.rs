use futures::{SinkExt, StreamExt};
use playground_wgpu::net::{Player, Request, Response};
use std::{
    collections::{HashMap, HashSet},
    net::IpAddr,
    sync::{Arc, RwLock},
};
use tokio::{net::TcpListener, sync::broadcast};
use tokio_util::codec::{Framed, LengthDelimitedCodec};

#[tokio::main]
async fn main() {
    std::env::set_var("RUST_LOG", "DEBUG");
    env_logger::init();

    let port = std::env::args().nth(1).unwrap_or("8080".to_string());
    let listener = TcpListener::bind(&format!("127.0.0.1:{}", port))
        .await
        .unwrap();
    log::info!("Server started on port: {port}");
    let (tx, _rx) = broadcast::channel(10);
    // TODO:
    let good_clients: Arc<RwLock<HashMap<String, Player>>> = Arc::new(RwLock::new(HashMap::new()));
    let bad_clients: Arc<RwLock<HashSet<String>>> = Arc::new(RwLock::new(HashSet::new()));

    loop {
        let (socket, addr) = listener.accept().await.unwrap();
        let client_ip = socket.peer_addr().unwrap().ip();
        let client_port = socket.peer_addr().unwrap().port();
        let client_addr = format!("{client_ip}:{client_port}");
        let tx_broadcast = tx.clone();
        let mut rx_broadcast = tx_broadcast.subscribe();
        let good_clients = Arc::clone(&good_clients);
        let bad_clients = Arc::clone(&bad_clients);

        tokio::spawn(async move {
            // websockets ?, udp ?, tls ...
            let framed = Framed::new(socket, LengthDelimitedCodec::new());
            let (mut writer, mut reader) = framed.split();

            loop {
                tokio::select! {
                    result = reader.next() => match result {
                        Some(Ok(msg)) => {
                            let req: Request = bincode::deserialize(&msg).unwrap();
                            match req {
                                Request::Register => {
                                    if is_valid_client(&client_ip) {
                                        // 1. create default player state
                                        let player = Player::default();
                                        // 2. send default state to client who wants to join,
                                        //    together with already existing clients
                                        let existing_clients = good_clients.read().unwrap().values()
                                            .map(|existing| (existing.id, existing.clone()))
                                            .collect();
                                        let resp = Response::Allowed((player.clone(), existing_clients));
                                        let encoded_msg = bincode::serialize(&resp).unwrap();
                                        if let Err(e) = writer.send(encoded_msg.into()).await {
                                            eprintln!("GoodGonner {client_addr}: {e:?}");
                                            break;
                                        }
                                        // 3. broadcast to everyone that new player joined
                                        let resp = Response::Joined(player.clone());
                                        let _ = tx_broadcast.send((resp, addr)).unwrap();
                                        // 4. save player's state
                                        let num_clients = {
                                            let mut goodies = good_clients.write().unwrap();
                                            goodies.insert(client_addr.clone(), player);
                                            goodies.len()
                                        };
                                        log::info!("Client joined: {client_addr}. Total count: {num_clients}");
                                    } else {
                                        let resp = Response::Denied("because reasons...".to_string());
                                        let encoded_msg = bincode::serialize(&resp).unwrap();
                                        if let Err(e) = writer.send(encoded_msg.into()).await {
                                            eprintln!("BadGonner {client_addr}: {e:?}");
                                        }
                                        bad_clients.write().unwrap().insert(client_addr);
                                        break;
                                    }
                                },
                                Request::Update(player) => {
                                    let is_goodie = good_clients.read().unwrap().contains_key(&client_addr);
                                    if is_goodie {
                                        if is_valid_update(&player) {
                                            // update this player's state (for now, reinsert)
                                            good_clients.write().unwrap().insert(client_addr.clone(), player.clone());
                                            // broadcast to everyone new player's state
                                            let _ = tx_broadcast.send((Response::Updated(player), addr)).unwrap();
                                        } else {
                                            // penalize this player ?
                                        }
                                    } else {
                                        bad_clients.write().unwrap().insert(client_addr);
                                        break;
                                    }
                                }
                                Request::Remove => {
                                    if let Some(player) = good_clients.read().unwrap().get(&client_addr) {
                                        // broadcast to everyone player was removed
                                        let _ = tx_broadcast.send((Response::Removed(player.id), addr)).unwrap();
                                        good_clients.write().unwrap().remove(&client_addr);
                                    } else {
                                        bad_clients.write().unwrap().insert(client_addr);
                                        break;
                                    }
                                },
                            }
                        },
                        _ => {
                            // client disconnected
                            let gone_id = good_clients.read().unwrap().get(&client_addr).unwrap().id;
                            let num_clients = {
                                let mut goodies = good_clients.write().unwrap();
                                goodies.remove(&client_addr);
                                goodies.len()
                            };
                            // broadcast to everyone player was removed
                            let _ = tx_broadcast.send((Response::Removed(gone_id), addr)).unwrap();
                            log::info!("Client disconnected: {client_addr}. Total count: {num_clients}");
                            break;
                        }
                    },
                    result = rx_broadcast.recv() => match result {
                        Ok((response, other_addr)) => {
                            if addr != other_addr {
                                let encoded_response = bincode::serialize(&response).unwrap();
                                if let Err(e) = writer.send(encoded_response.into()).await {
                                    eprintln!("Gonner {client_addr}: {e:?}");
                                    good_clients.write().unwrap().remove(&client_addr);
                                    break;
                                }
                            }
                        },
                        Err(_) => {
                            panic!("Broadcast Channel Closed...somehow");
                        }
                    }
                }
            }
        });
    }
}

fn is_valid_client(client_ip: &IpAddr) -> bool {
    client_ip.is_loopback()
}

fn is_valid_update(_player: &Player) -> bool {
    true
}
