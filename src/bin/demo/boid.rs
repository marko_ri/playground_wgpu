mod renderer_custom;

use playground_wgpu::*;
use renderer_custom::RendererCustom;

pub struct Demo {
    num_particles: u32,
    particles_per_group: u32,
    sim_params: Vec<f32>,
    vertices: Vec<f32>,
}

impl Default for Demo {
    fn default() -> Self {
        let sim_params = vec![
            0.04,  // deltaT
            0.1,   // rule1Distance
            0.025, // rule2Distance
            0.025, // rule3Distance
            0.02,  // rule1Scale
            0.05,  // rule2Scale
            0.005, // rule3Scale
        ];
        let vertices = vec![-0.01f32, -0.02, 0.01, -0.02, 0.00, 0.02];

        Self {
            num_particles: 1000,
            particles_per_group: 64,
            sim_params,
            vertices,
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, _window: &Window) -> Init {
        let renderer = RendererCustom::new(
            gpu,
            self.num_particles,
            self.particles_per_group,
            &self.sim_params,
            &self.vertices,
        );
        Init {
            custom_renderer: Some(Box::new(renderer)),
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        _gpu: &Gpu,
        _renderer: &mut dyn Renderer,
        _wuc: &mut WindowUpdateContext,
    ) {
    }
}
