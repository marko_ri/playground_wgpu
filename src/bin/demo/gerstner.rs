// https://github.com/samdauwe/webgpu-native-examples/blob/master/src/examples/gerstner_waves.c
// Unreal Engine 4 Gerstner Waves: https://www.youtube.com/watch?v=_y7Z0MbGOMw

use cgmath::*;
use playground_wgpu::*;

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct Wave {
    wave_length: f32,
    amplitude: f32,
    steepness: f32,
    _padding1: f32,
    direction: [f32; 2],
    _padding2: [f32; 2],
}

#[repr(C)]
#[derive(Copy, Clone, bytemuck::Pod, bytemuck::Zeroable)]
struct WaveParam {
    waves: [Wave; 5],
    amplitude_sum: f32,
}

impl Default for WaveParam {
    fn default() -> Self {
        let waves = [
            Wave {
                wave_length: 8.0,
                amplitude: 0.1,
                steepness: 1.0,
                _padding1: 0.0,
                direction: [1.0, 1.3],
                _padding2: [0.0; 2],
            },
            Wave {
                wave_length: 4.0,
                amplitude: 0.1,
                steepness: 0.8,
                _padding1: 0.0,
                direction: [-0.7, 0.0],
                _padding2: [0.0; 2],
            },
            Wave {
                wave_length: 5.0,
                amplitude: 0.2,
                steepness: 1.0,
                _padding1: 0.0,
                direction: [0.3, 0.2],
                _padding2: [0.0; 2],
            },
            Wave {
                wave_length: 10.0,
                amplitude: 0.5,
                steepness: 1.0,
                _padding1: 0.0,
                direction: [4.3, 1.2],
                _padding2: [0.0; 2],
            },
            Wave {
                wave_length: 3.0,
                amplitude: 0.1,
                steepness: 1.0,
                _padding1: 0.0,
                direction: [0.5, 0.5],
                _padding2: [0.0; 2],
            },
        ];

        Self {
            waves,
            amplitude_sum: 0.0,
        }
    }
}

#[derive(Clone, Copy)]
enum UniformBindingIdx {
    Empty,
    Scene(usize),
    Wave(usize),
}

pub struct Demo {
    plane_width: f32,
    plane_height: f32,
    plane_rows: u32,
    plane_columns: u32,
    wave_param: WaveParam,
    model_matrix: Matrix4<f32>,
    waves_normalized: bool,
    prev_mouse: Vector2<f32>,
    current_mouse: Vector2<f32>,
    uniform_binding_idx: [UniformBindingIdx; 2],
}

impl Default for Demo {
    fn default() -> Self {
        let plane_width = 12.0;
        let plane_height = 12.0;
        let model_matrix = init_model_matrix(plane_width, plane_height);
        Self {
            plane_width,
            plane_height,
            plane_rows: 100,
            plane_columns: 100,
            wave_param: WaveParam::default(),
            model_matrix,
            waves_normalized: false,
            prev_mouse: vec2(-20.0, 0.0),
            current_mouse: vec2(-20.0, 0.0),
            uniform_binding_idx: [UniformBindingIdx::Empty; 2],
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let plane = shape::plane(
            self.plane_width,
            self.plane_height,
            self.plane_rows,
            self.plane_columns,
            "sea-color.jpg",
        );
        let model = resource::shape_to_model(plane, gpu.device(), gpu.queue());
        let uniforms = vec![
            self.create_scene_uniform(&window.wuc),
            self.create_wave_uniform(),
        ];
        self.uniform_binding_idx = [UniformBindingIdx::Scene(0), UniformBindingIdx::Wave(1)];

        let pipeline = InitPipeline {
            pipeline_name: "gerstner.wgsl".to_string(),
            shader_name: "gerstner.wgsl".to_string(),
            model: Some(model),
            uniforms,
            ..Default::default()
        };

        let scene = InitScene {
            name: "scene".to_string(),
            camera: Some(Camera::new_perspective(
                vec3(0.0, 5.0, 50.0),
                window.wuc.size.width as f32,
                window.wuc.size.height as f32,
            )),
            render_pipelines: vec![pipeline],
            ..Default::default()
        };

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        self.update_controls(wuc);

        if let Some(cam) = renderer.active_scene_mut().camera_container() {
            let (cam_pos, view_matrix) = self.calc_camera(wuc);
            cam.set_position(cam_pos);
            cam.set_view_matrix(view_matrix);
        }

        // update uniforms
        let uniform = self.create_scene_uniform(wuc);
        let mut idx = None;
        for binding_idx in self.uniform_binding_idx.into_iter() {
            if let UniformBindingIdx::Scene(val) = binding_idx {
                idx = Some(val);
                break;
            }
        }
        let idx = idx.unwrap();
        renderer.active_scene_mut().set_uniform_data_idx(
            "gerstner.wgsl",
            gpu.queue(),
            uniform.as_slice(),
            idx,
        );
    }
}

impl Demo {
    fn create_scene_uniform(&self, wuc: &WindowUpdateContext) -> InitBuffer {
        let mut data = Vec::new();
        let mat: [[f32; 4]; 4] = self.model_matrix.into();
        for num in mat.into_iter() {
            data.extend(num);
        }
        data.push(wuc.total_time);

        InitBuffer::new(data)
    }

    fn calc_camera(&self, _wuc: &WindowUpdateContext) -> (Vector3<f32>, Matrix4<f32>) {
        let quat = Quaternion::from(Euler {
            x: Deg(self.current_mouse.x),
            y: Deg(self.current_mouse.y),
            z: Deg(0.0),
        });
        let radius = 15.0;
        let view_matrix = create_orbit_view_matrix(radius, quat);
        let view_position = position_from_view_matrix(view_matrix);

        (view_position, view_matrix)
    }

    fn create_wave_uniform(&mut self) -> InitBuffer {
        if !self.waves_normalized {
            for wave in self.wave_param.waves.iter_mut() {
                // normalize
                let x = wave.direction[0];
                let y = wave.direction[1];
                let amplitude = (x * x + y * y).sqrt();
                wave.direction = [x / amplitude, y / amplitude];
            }
            self.waves_normalized = true;
        }

        self.wave_param.amplitude_sum = self.wave_param.waves.iter().map(|w| w.amplitude).sum();

        let data = bytemuck::cast_slice(&[self.wave_param]).to_vec();
        InitBuffer::new(data)
    }

    fn update_controls(&mut self, wuc: &WindowUpdateContext) {
        let mouse_pos = wuc.mouse_screen();
        if wuc.is_mouse_pressed(0) {
            self.prev_mouse = *mouse_pos;
        }
        if wuc.mouse_buttons[0] {
            let drag_distance = mouse_pos - self.prev_mouse;
            self.current_mouse -= drag_distance;
            self.prev_mouse = *mouse_pos;
        }

        // limit x and y
        self.current_mouse.x %= 360.0;
        self.current_mouse.y = f32::max(-90.0, f32::min(-10.0, self.current_mouse.y));
    }
}

fn init_model_matrix(plane_width: f32, plane_height: f32) -> Matrix4<f32> {
    let plane_center = vec3(-plane_width / 2.0, -plane_height / 2.0, 0.0);
    let translate = Matrix4::from_translation(plane_center);
    let rotate = Matrix4::from_angle_x(Deg(-90.0));

    rotate * translate
}

fn create_orbit_view_matrix(radius: f32, quat: Quaternion<f32>) -> Matrix4<f32> {
    // inv(R*T)
    let translate = Matrix4::from_translation(vec3(0.0, 0.0, radius));
    let rotate = Matrix4::from(quat);
    (rotate * translate).invert().unwrap()
}

fn position_from_view_matrix(view_matrix: Matrix4<f32>) -> Vector3<f32> {
    let inv = view_matrix.invert().unwrap();
    vec3(inv[3][0], inv[3][1], inv[3][2])
}
