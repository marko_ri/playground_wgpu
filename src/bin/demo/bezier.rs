use cgmath::*;
use playground_wgpu::*;

const NUM_SEGMENTS: u8 = 25;

#[derive(PartialEq)]
enum MouseHover {
    Gpu,
    Cpu,
}

struct CircleMouseCpu {
    center: Vector2<f32>,
    start: Vector2<f32>,
    end: Vector2<f32>,
}

pub struct Demo {
    follow_cursor: bool,
    mouse_hover: MouseHover,
    boundary_gpu: collision::Aabb,
    boundary_cpu: collision::Aabb,
    circle_mouse_cpu: CircleMouseCpu,
    circle_center_gpu: Vector2<f32>,
    total_shapes_len: usize,
}

impl Default for Demo {
    fn default() -> Self {
        Self {
            follow_cursor: true,
            mouse_hover: MouseHover::Gpu,
            boundary_gpu: collision::Aabb {
                bottom_left: vec2(0.55, 0.1),
                top_right: vec2(0.95, 0.9),
            },
            boundary_cpu: collision::Aabb {
                bottom_left: vec2(0.05, 0.1),
                top_right: vec2(0.45, 0.9),
            },
            circle_mouse_cpu: CircleMouseCpu {
                center: vec2(0.1, 0.5),
                start: vec2(0.1, 0.3),
                end: vec2(0.4, 0.7),
            },
            circle_center_gpu: vec2(0.7, 0.7),
            total_shapes_len: 0,
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let aspect = window.wuc.size.width as f32 / window.wuc.size.height as f32;
        let shapes = self.shapes(aspect);
        let model = resource::shapes_to_model(shapes.iter(), gpu.device(), gpu.queue());

        let pipeline = InitPipeline {
            pipeline_name: "bezier.wgsl".to_string(),
            shader_name: "bezier.wgsl".to_string(),
            model: Some(model),
            uniforms: vec![self.create_uniform(&window.wuc)],
            ..Default::default()
        };
        let scene = InitScene {
            name: "scene".to_string(),
            render_pipelines: vec![pipeline],
            camera: Some(Camera::new_ortho(
                vec3(0.0, 0.0, 0.0),
                window.wuc.size.width as f32,
                window.wuc.size.height as f32,
            )),
            ..Default::default()
        };

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        let mut mouse_pos = vec2(
            wuc.mouse_screen().x / wuc.size.width as f32,
            1.0 - wuc.mouse_screen().y / wuc.size.height as f32,
        );

        if wuc.is_mouse_pressed(0) {
            self.mouse_hover = self.mouse_hover(&mouse_pos);
            self.follow_cursor = !self.follow_cursor;
        }

        self.mouse_clamp(&mut mouse_pos);

        if self.follow_cursor
            && self.mouse_hover == MouseHover::Cpu
            && !is_equal(&mouse_pos, &self.circle_mouse_cpu.center)
        {
            // TODO: we need to update only the line and mouce_circle
            self.circle_mouse_cpu.center = mouse_pos;
            let aspect = wuc.size.width as f32 / wuc.size.height as f32;
            let shapes = self.shapes(aspect);
            let model = resource::shapes_to_model(shapes.iter(), gpu.device(), gpu.queue());
            renderer.active_scene_mut().set_model("bezier.wgsl", model);
        }

        if self.follow_cursor && self.mouse_hover == MouseHover::Gpu {
            self.circle_center_gpu = mouse_pos;
        }

        let uniform = self.create_uniform(wuc);
        renderer.active_scene_mut().set_uniform_data(
            "bezier.wgsl",
            gpu.queue(),
            uniform.as_slice(),
        );
    }
}

impl Demo {
    fn shapes(&mut self, aspect: f32) -> Vec<shape::Shape<VertexCol>> {
        // gpu boundary recrangle
        let mut result = Vec::new();
        let shape_gpu = shape::rectangle_fan(
            vec2(
                self.boundary_gpu.center().x * aspect,
                self.boundary_gpu.center().y,
            ),
            self.boundary_gpu.width() * aspect,
            self.boundary_gpu.height(),
            [0.0, 0.1, 0.0, 0.1],
        );
        result.push(shape_gpu);

        // cpu - start circle
        let start_center = vec2(
            self.circle_mouse_cpu.start.x * aspect,
            self.circle_mouse_cpu.start.y,
        );
        let shape_start = shape::circle_fan(start_center, 0.03, 36, [1.0, 0.0, 0.0, 1.0], 0.3);
        result.push(shape_start);

        // cpu - end circle
        let end_center = vec2(
            self.circle_mouse_cpu.end.x * aspect,
            self.circle_mouse_cpu.end.y,
        );
        let shape_end = shape::circle_fan(end_center, 0.03, 36, [0.11, 0.27, 0.91, 1.0], 0.3);
        result.push(shape_end);

        // cpu - mouse circle
        let mouse_circle = vec2(
            self.circle_mouse_cpu.center.x * aspect,
            self.circle_mouse_cpu.center.y,
        );
        let shape_mouse = shape::circle_fan(mouse_circle, 0.03, 36, [0.0, 1.0, 0.0, 1.0], 0.3);
        result.push(shape_mouse);

        // cpu - line segments
        let mut line_segments = draw_bezier(start_center, end_center, mouse_circle, NUM_SEGMENTS);
        result.append(&mut line_segments);

        // cpu boundary rectangle
        let shape_cpu = shape::rectangle_fan(
            vec2(
                self.boundary_cpu.center().x * aspect,
                self.boundary_cpu.center().y,
            ),
            self.boundary_cpu.width() * aspect,
            self.boundary_cpu.height(),
            [0.1, 0.0, 0.0, 0.1],
        );
        result.push(shape_cpu);

        self.total_shapes_len = result.len();
        result
    }

    fn mouse_hover(&self, mouse_pos: &Vector2<f32>) -> MouseHover {
        let aabb_cpu = collision::Aabb {
            bottom_left: vec2(
                self.boundary_cpu.bottom_left.x,
                self.boundary_cpu.bottom_left.y,
            ),
            top_right: vec2(self.boundary_cpu.top_right.x, self.boundary_cpu.top_right.y),
        };
        if collision::point_in_aabb(mouse_pos, &aabb_cpu) {
            MouseHover::Cpu
        } else {
            MouseHover::Gpu
        }
    }

    fn mouse_clamp(&mut self, mouse_pos: &mut Vector2<f32>) {
        match self.mouse_hover {
            MouseHover::Cpu => {
                if mouse_pos.x < self.boundary_cpu.bottom_left.x {
                    mouse_pos.x = self.boundary_cpu.bottom_left.x;
                }
                if mouse_pos.y < self.boundary_cpu.bottom_left.y {
                    mouse_pos.y = self.boundary_cpu.bottom_left.y;
                }
                if mouse_pos.x > self.boundary_cpu.top_right.x {
                    mouse_pos.x = self.boundary_cpu.top_right.x;
                }
                if mouse_pos.y > self.boundary_cpu.top_right.y {
                    mouse_pos.y = self.boundary_cpu.top_right.y;
                }
            }
            MouseHover::Gpu => {
                if mouse_pos.x < self.boundary_gpu.bottom_left.x {
                    mouse_pos.x = self.boundary_gpu.bottom_left.x;
                }
                if mouse_pos.y < self.boundary_gpu.bottom_left.y {
                    mouse_pos.y = self.boundary_gpu.bottom_left.y;
                }
                if mouse_pos.x > self.boundary_gpu.top_right.x {
                    mouse_pos.x = self.boundary_gpu.top_right.x;
                }
                if mouse_pos.y > self.boundary_gpu.top_right.y {
                    mouse_pos.y = self.boundary_gpu.top_right.y;
                }
            }
        }
    }

    fn create_uniform(&self, wuc: &WindowUpdateContext) -> InitBuffer {
        let data = vec![
            wuc.size.width as f32,
            wuc.size.height as f32,
            wuc.total_time,
            self.circle_center_gpu.x,
            self.circle_center_gpu.y,
        ];

        InitBuffer::new(data)
    }
}

fn is_equal(vector: &Vector2<f32>, point: &Vector2<f32>) -> bool {
    vector.x == point.x && vector.y == point.y
}

fn bezier(a: Vector2<f32>, b: Vector2<f32>, c: Vector2<f32>, t: f32) -> Vector2<f32> {
    let first = a.lerp(c, t);
    let second = c.lerp(b, t);
    first.lerp(second, t)
}

fn draw_bezier(
    a: Vector2<f32>,
    b: Vector2<f32>,
    c: Vector2<f32>,
    num_segs: u8,
) -> Vec<shape::Shape<VertexCol>> {
    let mut result = Vec::new();
    let mut pp = a;
    for i in 1..=num_segs {
        let t = i as f32 / num_segs as f32;
        let p = bezier(a, b, c, t);
        result.push(shape::line_fan(&p, &pp, 0.005, &[1.0; 4]));
        pp = p;
    }
    result
}
