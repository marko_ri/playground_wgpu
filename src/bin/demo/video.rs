// https://github.com/leandromoreira/ffmpeg-libav-tutorial
// https://github.com/zmwangx/rust-ffmpeg/blob/master/examples/dump-frames.rs
// https://github.com/samdauwe/webgpu-native-examples/blob/master/src/examples/video_uploading.c
// TODO: sound decoding

use cgmath::*;
use playground_wgpu::*;
use std::sync::mpsc;

pub struct Demo {
    bytes_per_pixel: u32,
    drawn_region_size: wgpu::Extent3d,
    video_texture: Option<wgpu::Texture>,
    video_width: u32,
    video_height: u32,
    rx: mpsc::Receiver<Vec<u8>>,
}

impl Default for Demo {
    fn default() -> Self {
        let path = "resources/img/Big_Buck_Bunny.mp4";
        let video = video_reader::VideoDecodeState::open_file(path);
        let video_width = video.width();
        let video_height = video.height();
        let drawn_region_size = wgpu::Extent3d {
            width: video_width,
            height: video_height,
            depth_or_array_layers: 1,
        };
        let bytes_per_pixel = 4;

        let (tx, rx) = mpsc::channel();
        let _handle = std::thread::spawn(move || {
            video.start_decode(tx);
        });

        Self {
            bytes_per_pixel,
            drawn_region_size,
            video_texture: None,
            video_width,
            video_height,
            rx,
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let background = {
            let shape = shape::rectangle_tex(
                vec2(
                    0.5 * window.wuc.size.width as f32,
                    0.5 * window.wuc.size.height as f32,
                ),
                0.5 * self.video_width as f32,
                0.5 * self.video_height as f32,
                Some(shape::TextureKind::Video {
                    width: self.video_width,
                    height: self.video_height,
                }),
            );
            resource::shape_to_model(shape, gpu.device(), gpu.queue())
        };

        let pipeline = InitPipeline {
            pipeline_name: "pipeline".to_string(),
            shader_name: "default_texture.wgsl".to_string(),
            model: Some(background),
            ..Default::default()
        };

        let mut scene = InitScene {
            name: "scene".to_string(),
            render_pipelines: vec![pipeline],
            camera: Some(Camera::new_ortho_size(
                vec3(0.0, 0.0, 0.0),
                window.wuc.size.width as f32,
                window.wuc.size.height as f32,
                window.wuc.size.height as f32,
            )),
            ..Default::default()
        };

        // take texture from material
        self.video_texture = scene.render_pipelines[0].model.as_mut().unwrap().materials[0]
            .diffuse_texture
            .take()
            .map(|t| t.texture);

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        _renderer: &mut dyn Renderer,
        _wuc: &mut WindowUpdateContext,
    ) {
        self.update_capture_texture(gpu.queue());
    }
}

impl Demo {
    fn update_capture_texture(&self, queue: &wgpu::Queue) {
        if let Ok(buffer) = self.rx.try_recv() {
            queue.write_texture(
                wgpu::ImageCopyTexture {
                    aspect: wgpu::TextureAspect::All,
                    texture: self.video_texture.as_ref().unwrap(),
                    mip_level: 0,
                    origin: wgpu::Origin3d::ZERO,
                },
                &buffer,
                wgpu::ImageDataLayout {
                    offset: 0,
                    bytes_per_row: Some(self.drawn_region_size.width * self.bytes_per_pixel),
                    rows_per_image: Some(self.drawn_region_size.height),
                },
                self.drawn_region_size,
            );
        }
    }
}
