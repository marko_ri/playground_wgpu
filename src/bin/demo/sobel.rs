use cgmath::*;
use playground_wgpu::*;
use std::str::FromStr;

const SCENE01: &str = "scene01";
const SCENE02: &str = "scene02";
const U_SOB_SIZE: usize = 2;
const U_SOB_X: usize = 0;
const U_SOB_FILTER: usize = 1;

#[derive(Default)]
pub struct Demo {}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let scene01 = {
            let mut shape_rectangle = shape::rectangle_transparent();
            shape_rectangle.transform(cgmath::Matrix4::from_scale(2.0));
            let rectangle = resource::shape_to_model(shape_rectangle, gpu.device(), gpu.queue());
            let uniform = create_uniform(
                window.wuc.size.width,
                window.wuc.size.height,
                window.wuc.total_time,
            );
            let pipeline_01 = InitPipeline {
                pipeline_name: "intro_shader_art.wgsl".to_string(),
                shader_name: "intro_shader_art.wgsl".to_string(),
                model: Some(rectangle),
                uniforms: vec![uniform],
                ..Default::default()
            };

            InitScene {
                name: SCENE01.to_string(),
                render_pipelines: vec![pipeline_01],
                ..Default::default()
            }
        };

        let scene02 = {
            let shape_rectangle = shape::rectangle_tex(
                vec2(0.0, 0.0),
                1.0,
                1.0,
                shape::TextureKind::from_str("butterfly.jpg").ok(),
            );
            let model_rectangle =
                resource::shape_to_model(shape_rectangle, gpu.device(), gpu.queue());

            let pipeline = InitPipeline {
                pipeline_name: "sobel.wgsl".to_string(),
                shader_name: "sobel.wgsl".to_string(),
                model: Some(model_rectangle),
                uniforms: vec![InitBuffer::new(vec![0.0; U_SOB_SIZE])],
                ..Default::default()
            };

            let egui_content = |ctx: &egui::Context, scene: &mut Scene| {
                egui::Window::new(&scene.name).show(ctx, |ui| {
                    let uniform = scene.egui_state("sobel.wgsl");
                    ui.label(format!("Mouse: {:?}", ctx.pointer_latest_pos()));
                    ui.separator();
                    ui.add(egui::Slider::new(&mut uniform[U_SOB_X], -1.0..=1.0).text("x"));
                    ui.label(format!("Slider val: {:.1}", uniform[U_SOB_X]));
                    ui.separator();
                    let mut checkbox_val = uniform[U_SOB_FILTER] == 1.0;
                    ui.checkbox(&mut checkbox_val, "Sobel On/Off");
                    uniform[U_SOB_FILTER] = if checkbox_val { 1.0 } else { 0.0 };
                });
            };

            let egui = EguiContext::new(gpu, window.win(), egui_content);
            let egui_state =
                EguiState::new(SCENE02, &pipeline.pipeline_name, vec![0.0; U_SOB_SIZE]);
            InitScene {
                name: SCENE02.to_string(),
                render_pipelines: vec![pipeline],
                egui_ctx: Some(egui),
                egui_state: Some(egui_state),
                ..Default::default()
            }
        };

        Init {
            scenes: vec![scene01, scene02],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        if wuc.is_key_pressed(&winit::event::VirtualKeyCode::Key1) {
            renderer.set_active_scene(0);
        }
        if wuc.is_key_pressed(&winit::event::VirtualKeyCode::Key2) {
            renderer.set_active_scene(1);
        }

        if renderer.active_scene().name == SCENE01 {
            renderer.active_scene_mut().set_uniform_data(
                "intro_shader_art.wgsl",
                gpu.queue(),
                create_uniform(wuc.size.width, wuc.size.height, wuc.total_time).as_slice(),
            );
        }
    }
}

fn create_uniform(win_width: u32, win_height: u32, total_time: f32) -> InitBuffer {
    let data = vec![win_width as f32, win_height as f32, total_time];
    InitBuffer::new(data)
}
