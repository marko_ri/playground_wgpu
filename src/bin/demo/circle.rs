use cgmath::*;
use playground_wgpu::*;

pub struct Demo {
    circle_line: Vector2<f32>,
    circle_fan: Vector2<f32>,
    circle_shader: Vector2<f32>,
}

impl Default for Demo {
    fn default() -> Self {
        Self {
            circle_line: vec2(0.2, 0.8),
            circle_fan: vec2(0.5, 0.8),
            circle_shader: vec2(0.5, 0.5),
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let circle_line = resource::shape_to_model(
            shape::circle(self.circle_line, 0.1, 36, [1.0, 1.0, 0.0, 0.0]),
            gpu.device(),
            gpu.queue(),
        );
        let pipeline_line = InitPipeline {
            pipeline_name: "pipeline_line".to_string(),
            shader_name: "default.wgsl".to_string(),
            model: Some(circle_line),
            ..Default::default()
        };

        // different wgpu::PrimitiveTopology
        let circle_fan = resource::shape_to_model(
            shape::circle_fan(self.circle_fan, 0.1, 36, [1.0, 0.0, 1.0, 0.0], 0.5),
            gpu.device(),
            gpu.queue(),
        );
        let pipeline_fan = InitPipeline {
            pipeline_name: "pipeline_fan".to_string(),
            shader_name: "default.wgsl".to_string(),
            model: Some(circle_fan),
            ..Default::default()
        };

        let mut shape_rectangle = shape::rectangle_transparent();
        shape_rectangle.transform(cgmath::Matrix4::from_scale(2.0));
        let model_rectangle = resource::shape_to_model(shape_rectangle, gpu.device(), gpu.queue());

        let pipeline_shader = InitPipeline {
            pipeline_name: "pipeline_shader".to_string(),
            shader_name: "circle.wgsl".to_string(),
            model: Some(model_rectangle),
            uniforms: vec![self.create_uniform(&window.wuc)],
            ..Default::default()
        };

        let scene = InitScene {
            name: "scene".to_string(),
            render_pipelines: vec![pipeline_line, pipeline_fan, pipeline_shader],
            camera: Some(Camera::new_ortho(
                vec3(0.0, 0.0, 0.0),
                window.wuc.size.width as f32,
                window.wuc.size.height as f32,
            )),
            ..Default::default()
        };

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        //TODO: only on window resize
        renderer.active_scene_mut().set_uniform_data(
            "pipeline_shader",
            gpu.queue(),
            self.create_uniform(wuc).as_slice(),
        );
    }
}

impl Demo {
    fn create_uniform(&self, wuc: &WindowUpdateContext) -> InitBuffer {
        let data = vec![
            wuc.size.width as f32,
            wuc.size.height as f32,
            self.circle_shader.x,
            self.circle_shader.y,
        ];

        InitBuffer::new(data)
    }
}
