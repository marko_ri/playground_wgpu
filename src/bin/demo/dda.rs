// Super Fast Ray Casting in Tiled Worlds using DDA: https://www.youtube.com/watch?v=NbSee-XM7WA

use cgmath::*;
use playground_wgpu::*;
use std::collections::HashSet;
use winit::event::VirtualKeyCode;

const WORLD_SIZE: f32 = 20.0;
const CELL_SIZE: f32 = 1.0;
const NUM_CELLS: u16 = 20;
const GREEN: [f32; 4] = [0.0, 1.0, 0.0, 1.0];
const RADIUS: f32 = 0.02 * WORLD_SIZE;
const KEY_SPEED: f32 = 0.005;

// uniform values are in normalized space
// TODO: there is probably a way that does not require switching
// between world space and normalized space (when not using vertex buffer) ?
pub struct Demo {
    wall: HashSet<Vector2<i16>>,
    // ray_start/ray_end/intersection are normalized coordinates
    uniform: InitBuffer,
}

impl Demo {
    pub fn new(win_width: u32, win_height: u32) -> Self {
        let aspect = win_width as f32 / win_height as f32;
        let ray_start = vec2(0.5 / aspect, 0.5);
        let uniform = create_uniform(win_width, win_height, &ray_start);
        Self {
            wall: HashSet::new(),
            uniform,
        }
    }

    fn set_win_width(&mut self, val: f32) {
        self.uniform.set_val(0, val);
    }

    fn set_win_height(&mut self, val: f32) {
        self.uniform.set_val(1, val);
    }

    fn ray_start_x(&mut self, aspect: f32) -> f32 {
        self.uniform.val(2) * aspect * WORLD_SIZE
    }

    fn add_to_ray_start_x(&mut self, val: f32) {
        let old_val = self.uniform.val(2);
        self.uniform.set_val(2, old_val + val);
    }

    fn ray_start_y(&mut self) -> f32 {
        self.uniform.val(3) * WORLD_SIZE
    }

    fn add_to_ray_start_y(&mut self, val: f32) {
        let old_val = self.uniform.val(3);
        self.uniform.set_val(3, old_val + val);
    }

    fn set_ray_end_x(&mut self, val: f32, aspect: f32) {
        self.uniform.set_val(4, val / (aspect * WORLD_SIZE));
    }

    fn set_ray_end_y(&mut self, val: f32) {
        self.uniform.set_val(5, val / WORLD_SIZE);
    }

    fn set_intersection_x(&mut self, val: f32, aspect: f32) {
        self.uniform.set_val(6, val / (aspect * WORLD_SIZE));
    }

    fn set_intersection_y(&mut self, val: f32) {
        self.uniform.set_val(7, val / WORLD_SIZE);
    }
}

impl Gui for Demo {
    fn win_resizable(&self) -> bool {
        false
    }

    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let grid_center = vec2(0.5, 0.5) * WORLD_SIZE;
        let grid_model = shape::grid(gpu, grid_center, NUM_CELLS, CELL_SIZE);

        let pipe_grid = InitPipeline {
            pipeline_name: "grid".to_string(),
            shader_name: "default_instances.wgsl".to_string(),
            model: Some(grid_model),
            ..Default::default()
        };

        let mut circles_shape = shape::rectangle_transparent();
        circles_shape.transform(cgmath::Matrix4::from_scale(2.0 * WORLD_SIZE));
        let circles_model = resource::shape_to_model(circles_shape, gpu.device(), gpu.queue());

        let pipe_circles = InitPipeline {
            pipeline_name: "circles".to_string(),
            shader_name: "dda.wgsl".to_string(),
            model: Some(circles_model),
            uniforms: vec![self.uniform.clone()],
            ..Default::default()
        };

        // model can not be empty, so put one wall in bottom left
        let wall_shape = shape::rectangle_fan(
            vec2(0.5 * CELL_SIZE, 0.5 * CELL_SIZE),
            CELL_SIZE,
            CELL_SIZE,
            GREEN,
        );
        let wall_model = resource::shape_to_model(wall_shape, gpu.device(), gpu.queue());
        let pipe_wall = InitPipeline {
            pipeline_name: "wall".to_string(),
            shader_name: "default.wgsl".to_string(),
            model: Some(wall_model),
            ..Default::default()
        };

        let scene = InitScene {
            name: "scene".to_string(),
            render_pipelines: vec![pipe_grid, pipe_wall, pipe_circles],
            camera: Some(Camera::new_ortho_size(
                vec3(0.0, 0.0, 0.0),
                window.wuc.size.width as f32,
                window.wuc.size.height as f32,
                WORLD_SIZE,
            )),
            ..Default::default()
        };

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        let aspect = wuc.size.width as f32 / wuc.size.height as f32;
        self.set_win_width(wuc.size.width as f32);
        self.set_win_height(wuc.size.height as f32);

        self.move_ray_start(wuc, aspect);

        let mouse_grid = wuc.mouse_world(WORLD_SIZE, aspect);
        if self.mouse_in_grid(&mouse_grid) {
            if wuc.is_mouse_pressed(2) {
                self.draw_wall(gpu.device(), renderer, &mouse_grid);
            }

            if wuc.mouse_buttons[0] {
                self.set_ray_end_x(mouse_grid.x, aspect);
                self.set_ray_end_y(mouse_grid.y);

                let ray_start = vec2(self.ray_start_x(aspect), self.ray_start_y());
                let ray_dir = (mouse_grid - ray_start).normalize();
                let distance = collision::dda(
                    &ray_start,
                    &ray_dir,
                    CELL_SIZE,
                    WORLD_SIZE,
                    |tl_curr_tile| self.wall_hit(tl_curr_tile),
                );
                if distance > 0.0 {
                    let intersection = ray_start + ray_dir * distance;
                    self.set_intersection_x(intersection.x, aspect);
                    self.set_intersection_y(intersection.y);
                }
            }
        }

        if wuc.is_mouse_released(0) {
            self.set_intersection_x(-1.0, aspect);
            self.set_intersection_y(-1.0);
        }

        renderer.active_scene_mut().set_uniform_data(
            "circles",
            gpu.queue(),
            self.uniform.as_slice(),
        );
    }
}

impl Demo {
    fn draw_wall(
        &mut self,
        device: &wgpu::Device,
        renderer: &mut dyn Renderer,
        mouse: &Vector2<f32>,
    ) {
        let top_left = vec2(
            (mouse.x / CELL_SIZE).trunc() as i16,
            ((mouse.y + 1.0) / CELL_SIZE).trunc() as i16,
        );
        if self.wall.contains(&top_left) {
            return;
        }
        let center = vec2(
            top_left.x as f32 + 0.5 * CELL_SIZE,
            top_left.y as f32 - 0.5 * CELL_SIZE,
        );
        let shape = shape::rectangle_fan(center, CELL_SIZE, CELL_SIZE, GREEN);
        let mesh = resource::shape_to_mesh(&shape, device, None);

        renderer.active_scene_mut().add_mesh_to_model("wall", mesh);

        self.wall.insert(top_left);
    }

    fn wall_hit(&self, tl: &Vector2<i16>) -> bool {
        self.wall.contains(tl)
    }

    fn mouse_in_grid(&self, mouse: &Vector2<f32>) -> bool {
        0.0 <= mouse.x && mouse.x <= WORLD_SIZE && 0.0 <= mouse.y && mouse.y <= WORLD_SIZE
    }

    fn move_ray_start(&mut self, wuc: &WindowUpdateContext, aspect: f32) {
        if *wuc.keys_pressed.get(&VirtualKeyCode::A).unwrap_or(&false)
            && self.ray_start_x(aspect) - RADIUS * aspect > 0.0 + KEY_SPEED
        {
            self.add_to_ray_start_x(-KEY_SPEED);
        }
        if *wuc.keys_pressed.get(&VirtualKeyCode::D).unwrap_or(&false)
            && self.ray_start_x(aspect) + RADIUS * aspect < WORLD_SIZE - KEY_SPEED
        {
            self.add_to_ray_start_x(KEY_SPEED);
        }
        if *wuc.keys_pressed.get(&VirtualKeyCode::W).unwrap_or(&false)
            && self.ray_start_y() + RADIUS < WORLD_SIZE - KEY_SPEED
        {
            self.add_to_ray_start_y(KEY_SPEED);
        }
        if *wuc.keys_pressed.get(&VirtualKeyCode::S).unwrap_or(&false)
            && self.ray_start_y() - RADIUS > 0.0 + KEY_SPEED
        {
            self.add_to_ray_start_y(-KEY_SPEED);
        }
    }
}

fn create_uniform(win_width: u32, win_height: u32, ray_start: &Vector2<f32>) -> InitBuffer {
    let data = vec![
        win_width as f32,
        win_height as f32,
        ray_start.x,
        ray_start.y,
        -1.0,
        -1.0,
        -1.0,
        -1.0,
    ];

    InitBuffer::new(data)
}
