mod renderer_custom;

use cgmath::*;
use playground_wgpu::*;
use renderer_custom::RendererCustom;

pub struct Demo {
    num_circles: u32,
    circles_per_group: u32,
}

impl Default for Demo {
    fn default() -> Self {
        Self {
            num_circles: 100,
            circles_per_group: 64,
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, _window: &Window) -> Init {
        let radius = 0.03;
        let circle = circle(vec2(0.0, 0.0), radius, 36);
        let renderer = RendererCustom::new(
            gpu,
            self.num_circles,
            self.circles_per_group,
            &[radius],
            &circle,
            radius,
        );
        Init {
            custom_renderer: Some(Box::new(renderer)),
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        _gpu: &Gpu,
        _renderer: &mut dyn Renderer,
        _wuc: &mut WindowUpdateContext,
    ) {
        // compute shader is handling collisions
    }
}

pub fn circle(center: Vector2<f32>, radius: f32, sides: u8) -> Vec<[f32; 2]> {
    use std::ops::Add;

    let mut ort = vec2(radius, 0.0);
    let deg = Deg(360.0 / sides as f32);
    let rotate: Basis2<f32> = Rotation2::from_angle(Rad::from(deg));
    let mut result = Vec::new();

    for _ in 0..sides {
        let p = center.add(&ort);
        result.push([p.x, p.y]);
        ort = rotate.rotate_vector(ort);
    }
    // close the circle
    let first = center.add(vec2(radius, 0.0));
    result.push([first.x, first.y]);

    result
}
