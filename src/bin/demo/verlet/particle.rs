// A single point whose motion is simulated through space.
// Each particle is simulated using a Verlet integration method.

use cgmath::*;

pub struct Particle {
    pub pos: Vector2<f32>,
    old_pos: Vector2<f32>,
    acceleration: Vector2<f32>,
    friction: f32,
    bounce: f32,
    // if mass == 0.0, particle can not move
    pub mass: f32,
    pub radius: f32,
}

impl Particle {
    pub fn new(position: Vector2<f32>) -> Self {
        Self {
            pos: position,
            old_pos: position,
            acceleration: Vector2::zero(),
            friction: 0.99,
            bounce: 0.8,
            mass: 1.0,
            radius: 0.01,
        }
    }

    pub fn simulate(&mut self, dt: f32) {
        // A mass of zero denotes that particle cannot move
        if self.mass == 0.0 {
            return;
        }

        let velocity = self.pos - self.old_pos;
        self.old_pos = self.pos;
        // x[n + 1] = x[n] + (v[n] + a * dt) * dt;
        // x[n + 1] = x[n] + (x[n] - x[n - 1]) + a * dt * dt;
        self.pos += velocity + self.acceleration * dt * dt;
    }

    pub fn apply_force(&mut self, force: Vector2<f32>) {
        if self.mass > 0.0 {
            self.acceleration += force / self.mass;
        }
    }

    // Since the particle is simulated via Verlet integration, a change in the
    // particle's position results in an immediate change in velocity
    pub fn apply_impulse(&mut self, impulse: Vector2<f32>) {
        if self.mass > 0.0 {
            self.pos += impulse / self.mass;
        }
    }

    // By applying forces and resetting them after,  we ensure that the forces
    // must be applied every time step of the simulation in order to be
    // regarded as a continuous force
    pub fn reset_forces(&mut self) {
        self.acceleration = Vector2::zero();
    }

    // TODO: it is hard to do self collision in Verlet integration (?)
    // Restrain this particle to the simulation world boundaries
    pub fn restrain(&mut self, world_size: &Vector2<f32>) {
        if self.mass == 0.0 {
            return;
        }

        let vel = (self.pos - self.old_pos) * self.friction;

        if self.pos.x < 0.0 {
            self.pos.x = 0.0;
            self.old_pos.x = self.pos.x + vel.x * self.bounce;
        } else if self.pos.x > world_size.x {
            self.pos.x = world_size.x;
            self.old_pos.x = self.pos.x + vel.x * self.bounce;
        }

        if self.pos.y < 0.0 {
            self.pos.y = 0.0;
            self.old_pos.y = self.pos.y + vel.y * self.bounce;
        } else if self.pos.y > world_size.y {
            self.pos.y = world_size.y;
            self.old_pos.y = self.pos.y + vel.y * self.bounce;
        }
    }
}
