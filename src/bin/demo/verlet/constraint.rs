// The constraint attempts to maintain that initial distance between the
// two particles. The constraint is modelled as a spring, and as such,
// is mathematically defined using Hooke's law

use super::particle::Particle;
use cgmath::*;

pub struct Constraint {
    // target distance the particles try to maintain from one another
    target: f32,
    // Hooke's law spring constant [0.0, 1.0] (0 = no spring, 1 = rigid bar)
    stiff: f32,
    // Hooke's law dampening constant
    //damp: f32,
    pub particle1: usize,
    pub particle2: usize,
}

impl Constraint {
    pub fn new(
        particle1: usize,
        particle2: usize,
        target: Option<f32>,
        particles: &mut [Particle],
    ) -> Self {
        let target = if let Some(t) = target {
            t
        } else {
            let p1 = &particles[particle1];
            let p2 = &particles[particle2];
            let sq_dist = (p2.pos.x - p1.pos.x).powi(2) + (p2.pos.y - p1.pos.y).powi(2);
            sq_dist.sqrt()
        };

        Self {
            target,
            stiff: 1.0,
            particle1,
            particle2,
        }
    }

    pub fn relax_stick(&mut self, particles: &mut [Particle]) {
        let p1 = &particles[self.particle1];
        let p2 = &particles[self.particle2];
        let dx = p2.pos.x - p1.pos.x;
        let dy = p2.pos.y - p1.pos.y;
        let distance = (dx * dx + dy * dy).sqrt();
        let difference = self.target - distance;
        let percent = difference / distance / 2.0;
        let offset_x = dx * percent;
        let offset_y = dy * percent;

        let p1 = &mut particles[self.particle1];
        if p1.mass > 0.0 {
            p1.pos.x -= offset_x;
            p1.pos.y -= offset_y;
        }

        let p2 = &mut particles[self.particle2];
        if p2.mass > 0.0 {
            p2.pos.x += offset_x;
            p2.pos.y += offset_y;
        }
    }

    // TODO: does not work in this form
    #[allow(unused)]
    pub fn relax_spring(&mut self, particles: &mut [Particle]) {
        let p1 = &particles[self.particle1];
        let p2 = &particles[self.particle2];

        let force = 0.5
            * self.stiff
            * (p2.pos.distance(p1.pos) - self.target)
            * (p2.pos - p1.pos).normalize();

        if p1.mass > 0.0 && p2.mass == 0.0 {
            particles[self.particle1].apply_impulse(2.0 * force);
        } else if p1.mass == 0.0 && p2.mass > 0.0 {
            particles[self.particle2].apply_impulse(-2.0 * force);
        } else {
            particles[self.particle1].apply_impulse(force);
            particles[self.particle2].apply_impulse(-force);
        }
    }
}
