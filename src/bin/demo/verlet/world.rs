use super::{constraint::Constraint, particle::Particle};
use cgmath::*;

pub struct World {
    gravity: Vector2<f32>,
    pub particles: Vec<Particle>,
    pub constraints: Vec<Constraint>,
}

impl World {
    pub fn new(gravity: Vector2<f32>) -> Self {
        Self {
            gravity,
            particles: Vec::new(),
            constraints: Vec::new(),
        }
    }

    pub fn simulate(&mut self, world_size: &Vector2<f32>, dt: f32) {
        for particle in self.particles.iter_mut() {
            particle.apply_force(self.gravity);
            particle.simulate(dt);
            particle.restrain(world_size);
            particle.reset_forces();
        }

        for constraint in self.constraints.iter_mut() {
            constraint.relax_stick(&mut self.particles);
        }
    }

    pub fn add_particle(&mut self, pos: Vector2<f32>) -> usize {
        self.particles.push(Particle::new(pos));
        self.particles.len() - 1
    }

    pub fn add_constraint(&mut self, particle1: usize, particle2: usize, distance: Option<f32>) {
        self.constraints.push(Constraint::new(
            particle1,
            particle2,
            distance,
            &mut self.particles,
        ));
    }
}
