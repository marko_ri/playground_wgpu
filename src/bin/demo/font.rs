use cgmath::*;
use playground_wgpu::*;
use std::str::FromStr;

const NUM_VERT: usize = 4;

struct Mario {
    tileset: tile::TileSet,
    used_tiles: Vec<u32>,
    current_tile: u32,
}

struct Text {
    shape: shape::Shape<VertexTex>,
}

impl Text {
    fn new(glyphs: &[&tile::Tile], top_left: &Vector2<f32>) -> Self {
        let w = 0.05;
        let h = 0.05;

        let mut final_shape = shape::Shape {
            texture_kind: shape::TextureKind::from_str("charmap_oldschool_white.png").ok(),
            ..Default::default()
        };
        let first_center = vec2(top_left.x + w / 2.0, top_left.y - h / 2.0);

        for (i, glyph) in glyphs.iter().enumerate() {
            // TODO: all in one line (no wrapping)
            let center = vec2(first_center.x + i as f32 * w, first_center.y);
            let mut shape = shape::rectangle_tex(center, w, h, None);
            set_sprite_tex_coord(&mut shape.vertices, glyph);

            final_shape.vertices.append(&mut shape.vertices);
            for idx in shape.indices.into_iter() {
                final_shape.indices.push(idx + i as u32 * NUM_VERT as u32);
            }
        }

        Self { shape: final_shape }
    }

    fn change_fps(&mut self, fps: u8, glyphs: &tile::TileSet) {
        let text = format!("fps {:03}", fps);
        for (i, c) in text.bytes().enumerate() {
            let idx = i * NUM_VERT;
            set_sprite_tex_coord(
                &mut self.shape.vertices[idx..idx + NUM_VERT],
                glyphs.glyph(c),
            )
        }
    }
}

pub struct Demo {
    mario: Mario,
    fonts: tile::TileSet,
    fps_text: Text,
    move_text: Text,
    selected_sprite: Option<usize>,
}

impl Default for Demo {
    fn default() -> Self {
        let mut fonts = tile::TileSet::new(128.0, 64.0, 95, 7.0, 9.0, 0.0);
        fonts.default_font();

        let fps_glyphs: Vec<&tile::Tile> = "fps 000".bytes().map(|b| fonts.glyph(b)).collect();
        let fps_text = Text::new(&fps_glyphs, &vec2(0.0, 1.0));

        let move_glyphs: Vec<&tile::Tile> = "move letters with mouse"
            .bytes()
            .map(|b| fonts.glyph(b))
            .collect();
        let move_text = Text::new(&move_glyphs, &vec2(0.1, 0.5));

        Self {
            mario: Mario {
                tileset: tile::TileSet::new(224.0, 32.0, 26, 16.0, 16.0, 0.0),
                used_tiles: vec![0, 1, 2],
                current_tile: 0,
            },
            fonts,
            fps_text,
            move_text,
            selected_sprite: None,
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        // mario
        let mut shape_mario = shape::rectangle_tex(
            vec2(0.2, 0.2),
            0.2,
            0.2,
            shape::TextureKind::from_str("mario.png").ok(),
        );
        // because we are using the same shader for all three pipelines
        for vertex in shape_mario.vertices.iter_mut() {
            vertex.tex_coords = [0.0, 0.0];
        }
        let model_mario = resource::shape_to_model(shape_mario, gpu.device(), gpu.queue());

        let first_tile = self.mario.tileset.tiles.get(&0).unwrap();
        let pipeline_mario = InitPipeline {
            pipeline_name: "pipeline_mario".to_string(),
            shader_name: "default_sprite.wgsl".to_string(),
            model: Some(model_mario),
            uniforms: vec![self.create_uniform(first_tile)],
            ..Default::default()
        };

        // fps
        let fps = f32::round(window.wuc.frame_count as f32 / window.wuc.total_time) as u8;
        self.fps_text.change_fps(fps, &self.fonts);
        let model_fps =
            resource::shape_ref_to_model(&self.fps_text.shape, gpu.device(), gpu.queue());

        let pipeline_fps = InitPipeline {
            pipeline_name: "pipeline_fps".to_string(),
            shader_name: "default_sprite.wgsl".to_string(),
            model: Some(model_fps),
            uniforms: vec![self.create_uniform(&tile::Tile::default())],
            ..Default::default()
        };

        // text
        let model_text =
            resource::shape_ref_to_model(&self.move_text.shape, gpu.device(), gpu.queue());

        let pipeline_text = InitPipeline {
            pipeline_name: "pipeline_text".to_string(),
            shader_name: "default_sprite.wgsl".to_string(),
            model: Some(model_text),
            uniforms: vec![self.create_uniform(&tile::Tile::default())],
            ..Default::default()
        };

        let scene01 = InitScene {
            name: "scene".to_string(),
            camera: Some(Camera::new_ortho(
                vec3(0.0, 0.0, 0.0),
                window.wuc.size.width as f32,
                window.wuc.size.height as f32,
            )),
            render_pipelines: vec![pipeline_mario, pipeline_fps, pipeline_text],
            ..Default::default()
        };

        Init {
            scenes: vec![scene01],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        if wuc.frame_count % 10 == 0 {
            let tile_idx = self.mario.used_tiles[self.mario.current_tile as usize];
            let tile = self.mario.tileset.tiles.get(&tile_idx).unwrap();
            let uniform = self.create_uniform(tile);
            renderer.active_scene_mut().set_uniform_data(
                "pipeline_mario",
                gpu.queue(),
                uniform.as_slice(),
            );

            self.mario.current_tile =
                (self.mario.current_tile + 1) % self.mario.used_tiles.len() as u32;
        }

        if wuc.frame_count % 120 == 0 {
            let fps = f32::floor(wuc.frame_count as f32 / wuc.total_time) as u8;
            self.fps_text.change_fps(fps, &self.fonts);
            self.shape_to_gpu(
                gpu.device(),
                gpu.queue(),
                renderer,
                &self.fps_text.shape,
                "pipeline_fps",
            );
        }

        if wuc.is_mouse_pressed(0) {
            let cam = renderer.active_scene_mut().camera_container().unwrap();
            for (i, vert) in self.move_text.shape.vertices.chunks(NUM_VERT).enumerate() {
                let aabb = to_aabb(vert);
                if collision::clicked_on_aabb(&aabb, wuc, cam.camera()) {
                    self.selected_sprite = Some(i);
                    break;
                }
            }
        }

        if wuc.is_mouse_released(0) {
            self.selected_sprite = None;
        }

        if let Some(sprite_idx) = self.selected_sprite {
            let cam = renderer.active_scene_mut().camera_container().unwrap();
            let idx = sprite_idx * NUM_VERT;
            change_position(
                &mut self.move_text.shape.vertices[idx..idx + NUM_VERT],
                collision::mouse_screen_to_world(wuc, cam.camera()),
            );

            self.shape_to_gpu(
                gpu.device(),
                gpu.queue(),
                renderer,
                &self.move_text.shape,
                "pipeline_text",
            );
        }
    }
}

impl Demo {
    fn shape_to_gpu(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        renderer: &mut dyn Renderer,
        shape: &shape::Shape<VertexTex>,
        pipeline: &str,
    ) {
        let model = resource::shape_ref_to_model(shape, device, queue);
        renderer.active_scene_mut().set_model(pipeline, model);
    }

    fn create_uniform(&self, tile: &tile::Tile) -> InitBuffer {
        let data = vec![
            tile.left_top.x,
            tile.left_top.x,
            tile.right_bottom.x,
            tile.right_bottom.x,
            tile.left_top.y,
            tile.right_bottom.y,
            tile.right_bottom.y,
            tile.left_top.y,
        ];

        InitBuffer::new(data)
    }
}

pub fn set_sprite_tex_coord(vertices: &mut [VertexTex], glyph: &tile::Tile) {
    let tex_coords = [
        [glyph.left_top.x, glyph.left_top.y],
        [glyph.left_top.x, glyph.right_bottom.y],
        [glyph.right_bottom.x, glyph.right_bottom.y],
        [glyph.right_bottom.x, glyph.left_top.y],
    ];

    for (i, vertex) in vertices.iter_mut().enumerate() {
        vertex.tex_coords = tex_coords[i];
    }
}

pub fn to_aabb(vertices: &[VertexTex]) -> collision::Aabb {
    collision::Aabb {
        bottom_left: vec2(vertices[1].positions[0], vertices[1].positions[1]),
        top_right: vec2(vertices[3].positions[0], vertices[3].positions[1]),
    }
}

pub fn change_position(vertices: &mut [VertexTex], center: Vector2<f32>) {
    let width = vertices[3].positions[0] - vertices[1].positions[0];
    let height = vertices[3].positions[1] - vertices[1].positions[1];
    vertices[0].positions = [center.x - width / 2.0, center.y + height / 2.0, 0.0];
    vertices[1].positions = [center.x - width / 2.0, center.y - height / 2.0, 0.0];
    vertices[2].positions = [center.x + width / 2.0, center.y - height / 2.0, 0.0];
    vertices[3].positions = [center.x + width / 2.0, center.y + height / 2.0, 0.0];
}
