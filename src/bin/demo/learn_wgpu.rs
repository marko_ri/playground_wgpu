use cgmath::*;
use playground_wgpu::*;
use std::str::FromStr;
use winit::event::VirtualKeyCode;

const SCENE01: &str = "scene01";
const SCENE02: &str = "scene02";
const SCENE03: &str = "scene03";

#[derive(Default)]
pub struct Demo {}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        const NUM_INSTANCES_PER_ROW: u32 = 10;
        const SPACE_BETWEEN: f32 = 3.0;
        let scale = Matrix4::from_scale(0.25);
        let translate_left = Matrix4::from_translation(vec3(0.4, 0.5, 0.0));
        let translate_right = Matrix4::from_translation(vec3(1.2, 0.5, 0.0));
        let translate_up = Matrix4::from_translation(vec3(0.8, 0.8, 0.0));
        let instances = (0..NUM_INSTANCES_PER_ROW)
            .flat_map(|z| {
                (0..NUM_INSTANCES_PER_ROW).map(move |x| {
                    let x = SPACE_BETWEEN * (x as f32 - NUM_INSTANCES_PER_ROW as f32 / 2.0);
                    let z = SPACE_BETWEEN * (z as f32 - NUM_INSTANCES_PER_ROW as f32 / 2.0);

                    let translate = Vector3 { x, y: 0.0, z };

                    Instance { translate }
                })
            })
            .collect::<Vec<_>>();

        let scene01 = {
            let mut shape_texture =
                shape::pentagon_tex(shape::TextureKind::from_str("cube_diffuse.jpg").ok());
            shape_texture.transform(translate_left * scale);
            let model_texture = resource::shape_to_model(shape_texture, gpu.device(), gpu.queue());

            let mut triangle01 = shape::triangle();
            triangle01.transform(translate_right * scale);
            let mut triangle02 = shape::triangle();
            triangle02.transform(translate_up * scale);
            let model_triangle = resource::shapes_to_model(
                [triangle01, triangle02].iter(),
                gpu.device(),
                gpu.queue(),
            );

            let pipeline_01 = InitPipeline {
                pipeline_name: "default_nobuf.wgsl".to_string(),
                shader_name: "default_nobuf.wgsl".to_string(),
                ..Default::default()
            };
            let pipeline_02 = InitPipeline {
                pipeline_name: "default.wgsl".to_string(),
                shader_name: "default.wgsl".to_string(),
                model: Some(model_triangle),
                ..Default::default()
            };
            let pipeline_03 = InitPipeline {
                pipeline_name: "default_texture.wgsl".to_string(),
                shader_name: "default_texture.wgsl".to_string(),
                model: Some(model_texture),
                ..Default::default()
            };

            InitScene {
                name: SCENE01.to_string(),
                camera: Some(Camera::new_ortho(
                    vec3(0.0, 0.0, 0.0),
                    window.wuc.size.width as f32,
                    window.wuc.size.height as f32,
                )),
                render_pipelines: vec![pipeline_01, pipeline_02, pipeline_03],
                ..Default::default()
            }
        };

        let scene02 = {
            let shape_instances = shape::pentagon_col();
            let mut model_instances =
                resource::shape_to_model(shape_instances, gpu.device(), gpu.queue());
            model_instances.add_instances(gpu.device(), instances.clone());

            let pipeline_01 = InitPipeline {
                pipeline_name: "default_instances.wgsl".to_string(),
                shader_name: "default_instances.wgsl".to_string(),
                model: Some(model_instances),
                ..Default::default()
            };

            InitScene {
                name: SCENE02.to_string(),
                camera: Some(Camera::new_perspective(
                    vec3(0.0, 5.0, 10.0),
                    window.wuc.size.width as f32,
                    window.wuc.size.height as f32,
                )),
                render_pipelines: vec![pipeline_01],
                ..Default::default()
            }
        };

        let scene03 = {
            let mut model_obj =
                resource::load_model("cube.obj", gpu.device(), gpu.queue()).unwrap();
            model_obj.add_instances(gpu.device(), instances);

            let pipeline_01 = InitPipeline {
                pipeline_name: "texture_instances_light.wgsl".to_string(),
                shader_name: "texture_instances_light.wgsl".to_string(),
                model: Some(model_obj),
                ..Default::default()
            };

            let model_light = resource::load_model("cube.obj", gpu.device(), gpu.queue()).unwrap();
            let light = Light::new(model_light, [2.0, 2.0, 2.0], [1.0, 1.0, 1.0]);

            InitScene {
                name: SCENE03.to_string(),
                camera: Some(Camera::new_perspective(
                    vec3(0.0, 5.0, 10.0),
                    window.wuc.size.width as f32,
                    window.wuc.size.height as f32,
                )),
                light: Some(light),
                render_pipelines: vec![pipeline_01],
                ..Default::default()
            }
        };

        Init {
            scenes: vec![scene01, scene02, scene03],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        _gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        if let Some(cam) = renderer.active_scene_mut().camera_container() {
            move_camera(wuc, cam.controller());
            cam.controller().process_scroll(wuc);

            let view_matrix = cam.camera().calc_view_matrix();
            cam.set_view_matrix(view_matrix);
        }

        let mouse_pos = wuc.mouse_screen();
        renderer.active_scene_mut().background_color = wgpu::Color {
            r: mouse_pos.x as f64 / wuc.size.width as f64,
            g: mouse_pos.y as f64 / wuc.size.height as f64,
            b: 1.0,
            a: 1.0,
        };

        if wuc.is_key_pressed(&VirtualKeyCode::Key1) {
            renderer.set_active_scene(0);
        }
        if wuc.is_key_pressed(&VirtualKeyCode::Key2) {
            renderer.set_active_scene(1);
        }
        if wuc.is_key_pressed(&VirtualKeyCode::Key3) {
            renderer.set_active_scene(2);
        }

        if let Some(light) = renderer.active_scene_mut().light_container() {
            let old_position = vec3(
                light.old_position()[0],
                light.old_position()[1],
                light.old_position()[2],
            );
            let quaternion = Quaternion::from_angle_y(Deg(1.0));
            light.set_position((quaternion * old_position).into());
        }
    }
}

fn move_camera(wuc: &WindowUpdateContext, cam: &mut CameraController) {
    if wuc.is_key_pressed(&VirtualKeyCode::W) {
        cam.move_forward(1.0);
    }
    if wuc.is_key_released(&VirtualKeyCode::W) {
        cam.move_forward(0.0);
    }

    if wuc.is_key_pressed(&VirtualKeyCode::S) {
        cam.move_backward(1.0);
    }
    if wuc.is_key_released(&VirtualKeyCode::S) {
        cam.move_backward(0.0);
    }

    if wuc.is_key_pressed(&VirtualKeyCode::A) {
        cam.move_left(1.0);
    }
    if wuc.is_key_released(&VirtualKeyCode::A) {
        cam.move_left(0.0);
    }

    if wuc.is_key_pressed(&VirtualKeyCode::D) {
        cam.move_right(1.0);
    }
    if wuc.is_key_released(&VirtualKeyCode::D) {
        cam.move_right(0.0);
    }

    if wuc.is_key_pressed(&VirtualKeyCode::Space) {
        cam.move_up(1.0);
    }
    if wuc.is_key_released(&VirtualKeyCode::Space) {
        cam.move_up(0.0);
    }

    if wuc.is_key_pressed(&VirtualKeyCode::LShift) {
        cam.move_down(1.0);
    }
    if wuc.is_key_released(&VirtualKeyCode::LShift) {
        cam.move_down(0.0);
    }
}
