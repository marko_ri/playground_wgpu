// https://github.com/mikemarek/soft-body-physics
// Coding Math：Verlet Integration: https://www.youtube.com/watch?v=3HjO_RGIjCU
// TODO: Euler Integration

mod constraint;
mod particle;
mod world;

use cgmath::*;
use playground_wgpu::*;
use winit::event::VirtualKeyCode;
use world::World;

struct DemoScene {
    world: World,
    pinned_particles: Vec<usize>,
}

impl DemoScene {
    fn coords(&self) -> Vec<f32> {
        let mut result = Vec::new();

        // first two floats are:
        // number of particles, number of constraints
        result.push(self.world.particles.len() as f32);
        result.push(self.world.constraints.len() as f32);

        for p in self.world.particles.iter() {
            result.push(p.pos.x);
            result.push(p.pos.y);
            result.push(p.radius);
        }

        for c in self.world.constraints.iter() {
            result.push(self.world.particles[c.particle1].pos.x);
            result.push(self.world.particles[c.particle1].pos.y);

            result.push(self.world.particles[c.particle2].pos.x);
            result.push(self.world.particles[c.particle2].pos.y);
        }

        // size does not have to be power of two
        result
    }
}

// the best way would be to use compute shader for calulating constraints,
// but let's calculate constraints on cpu and copy coordinates of each particle to gpu on each frame
// (we do not have large amount of particles here, and we are learning...)
pub struct Demo {
    world_size: Vector2<f32>,
    scene0: DemoScene,
    scene1: DemoScene,
    old_mouse_pos: Vector2<f32>,
    mouse_strength: f32,
}

impl Default for Demo {
    fn default() -> Self {
        let scene0 = {
            let gravity = vec2(0.0, -0.5);
            let mut world = World::new(gravity);

            // scene0 square
            let tl = vec2(0.5, 0.5);
            let dist = 0.1;
            let p0 = world.add_particle(tl);
            let p1 = world.add_particle(vec2(tl.x, tl.y - dist));
            let p2 = world.add_particle(vec2(tl.x + dist, tl.y - dist));
            let p3 = world.add_particle(vec2(tl.x + dist, tl.y));

            world.add_constraint(p0, p1, None);
            world.add_constraint(p1, p2, None);
            world.add_constraint(p2, p3, None);
            world.add_constraint(p3, p0, None);
            world.add_constraint(p0, p2, None);
            world.add_constraint(p1, p3, None);

            world.particles[p0].apply_impulse(vec2(0.05, 0.05));

            // scene0 rope
            let p4 = world.add_particle(vec2(tl.x, tl.y + dist));
            let p5 = world.add_particle(vec2(tl.x, tl.y + 2.0 * dist));
            let p6 = world.add_particle(vec2(tl.x, tl.y + 3.0 * dist));

            world.add_constraint(p0, p4, None);
            world.add_constraint(p4, p5, None);
            world.add_constraint(p5, p6, None);

            world.particles[p6].mass = 0.0;

            DemoScene {
                world,
                pinned_particles: vec![p6],
            }
        };

        let scene1 = {
            let gravity = vec2(0.0, -0.5);
            let mut world = World::new(gravity);

            // scene1 cloth (2d grid of particles)
            let mut particles: Vec<Vec<usize>> = Vec::new();
            let step = 10;
            let (min_x, max_x) = (0.2, 0.8);
            let (min_y, max_y) = (0.4, 0.9);
            let size = vec2((max_x - min_x) / step as f32, (max_y - min_y) / step as f32);

            for y in 0..step {
                let mut row = Vec::new();
                for x in 0..step {
                    let y = y as f32;
                    let x = x as f32;
                    row.push(world.add_particle(vec2(min_x + x * size.x, min_y + y * size.y)));
                }
                particles.push(row);
            }

            let pinned1 = particles[step - 1][0];
            let pinned2 = particles[step - 1][step - 1];
            world.particles[pinned1].mass = 0.0;
            world.particles[pinned2].mass = 0.0;

            // scene1 - add horizontal contraints
            for y in 0..step {
                for x in 1..step {
                    world.add_constraint(particles[y][x - 1], particles[y][x], None);
                }
            }

            // scene1 - add vertical contraints
            for y in 1..step {
                for x in 0..step {
                    world.add_constraint(particles[y - 1][x], particles[y][x], None);
                }
            }

            DemoScene {
                world,
                pinned_particles: vec![pinned1, pinned2],
            }
        };

        Self {
            world_size: vec2(1.0, 1.0),
            scene0,
            scene1,
            old_mouse_pos: Vector2::zero(),
            mouse_strength: 1000.0,
        }
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let scene0 = {
            let mut background_shape = shape::rectangle_transparent();
            background_shape.transform(cgmath::Matrix4::from_scale(2.0));
            let background_model =
                resource::shape_to_model(background_shape, gpu.device(), gpu.queue());

            let uniform = create_uniform(window.wuc.size.width, window.wuc.size.height);

            let pipeline = InitPipeline {
                pipeline_name: "square".to_string(),
                shader_name: "verlet.wgsl".to_string(),
                model: Some(background_model),
                uniforms: vec![uniform],
                storage: Some(InitBuffer::new(self.scene0.coords())),
                ..Default::default()
            };

            InitScene {
                name: "square".to_string(),
                render_pipelines: vec![pipeline],
                ..Default::default()
            }
        };

        let scene1 = {
            let mut background_shape = shape::rectangle_transparent();
            background_shape.transform(cgmath::Matrix4::from_scale(2.0));
            let background_model =
                resource::shape_to_model(background_shape, gpu.device(), gpu.queue());

            let uniform = create_uniform(window.wuc.size.width, window.wuc.size.height);
            let pipeline = InitPipeline {
                pipeline_name: "cloth".to_string(),
                shader_name: "verlet.wgsl".to_string(),
                model: Some(background_model),
                uniforms: vec![uniform],
                storage: Some(InitBuffer::new(self.scene1.coords())),
                ..Default::default()
            };

            InitScene {
                name: "cloth".to_string(),
                render_pipelines: vec![pipeline],
                ..Default::default()
            }
        };

        Init {
            scenes: vec![scene0, scene1],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        if wuc.is_key_pressed(&VirtualKeyCode::Key1) {
            renderer.set_active_scene(0);
        }
        if wuc.is_key_pressed(&VirtualKeyCode::Key2) {
            renderer.set_active_scene(1);
        }

        if renderer.active_scene().name == "square" {
            let world = &mut self.scene0.world;
            let pinned_particle = self.scene0.pinned_particles[0];
            world.particles[pinned_particle].pos.x = 0.5 + Rad(wuc.total_time).cos() * 0.2;
            world.simulate(&self.world_size, wuc.delta_time);

            renderer.active_scene_mut().set_storage_data(
                "square",
                gpu.queue(),
                &self.scene0.coords(),
            );
        }

        if renderer.active_scene().name == "cloth" {
            let world = &mut self.scene1.world;

            if wuc.mouse_buttons[0] {
                let mouse_pos = wuc.mouse_normalized();
                let force = (mouse_pos - self.old_mouse_pos) * self.mouse_strength;
                for particle in world.particles.iter_mut() {
                    if mouse_pos.distance2(particle.pos) < particle.radius.powi(2) {
                        particle.apply_force(force);
                    }
                }
                self.old_mouse_pos = mouse_pos;
            }

            world.simulate(&self.world_size, wuc.delta_time);
            renderer.active_scene_mut().set_storage_data(
                "cloth",
                gpu.queue(),
                &self.scene1.coords(),
            );
        }
    }
}

fn create_uniform(win_width: u32, win_height: u32) -> InitBuffer {
    let data = vec![win_width as f32, win_height as f32];
    InitBuffer::new(data)
}
