use cgmath::*;
use playground_wgpu::{
    net::{client, Player, Request, Response},
    *,
};
use std::{collections::HashMap, sync::mpsc::TryRecvError};
use tokio::runtime::Builder;
use winit::event::VirtualKeyCode;

// TODO: fix too much cloning (put serialized bytes on channels directly?)
// (lots of buffering going on here)
pub struct Demo {
    rx_in: std::sync::mpsc::Receiver<Response>,
    tx_out: tokio::sync::mpsc::Sender<Request>,
    player: Player,
    other_players: HashMap<u32, Player>,
}

impl Demo {
    pub fn new(port: &str) -> Self {
        let (tx_in, rx_in) = std::sync::mpsc::channel();
        let (tx_out, rx_out) = tokio::sync::mpsc::channel(10);
        let port_own = port.to_string();

        let rt = Builder::new_current_thread().enable_all().build().unwrap();

        // TODO: is this the correct way?
        let _handle = std::thread::spawn(move || {
            rt.block_on(client::run(port_own, tx_in, rx_out));
        });

        // server is setting our start state, so wait for it...
        log::info!("Connecting to server on port {port}");
        tx_out.blocking_send(Request::Register).expect("No Server");
        let (player, other_players) = match rx_in.recv().expect("No Server") {
            Response::Allowed((player, other_players)) => (player, other_players),
            Response::Denied(reason) => panic!("Server refused connection: {reason}"),
            _ => unreachable!(),
        };

        Self {
            rx_in,
            tx_out,
            player,
            other_players,
        }
    }

    fn create_model(&self, gpu: &Gpu) -> Model {
        let mut shapes = Vec::new();
        shapes.push(shape::circle_fan(
            self.player.position,
            self.player.radius,
            10,
            [1.0; 4],
            0.2,
        ));
        for other in self.other_players.values() {
            shapes.push(shape::circle_fan(
                other.position,
                other.radius,
                10,
                [1.0; 4],
                0.05,
            ));
        }

        resource::shapes_to_model(shapes.iter(), gpu.device(), gpu.queue())
    }

    fn to_gpu(&self, gpu: &Gpu, renderer: &mut dyn Renderer) {
        // TODO: update only players that changed
        let model = self.create_model(gpu);
        renderer.active_scene_mut().set_model("net", model);
    }

    fn to_net(&self) {
        // only updates for now...
        // TODO: fix all this cloning (use bincode serialization...)
        let player = self.player.clone();
        self.tx_out
            .blocking_send(Request::Update(player))
            .expect("No Server");
    }

    fn from_net(&mut self) -> bool {
        match self.rx_in.try_recv() {
            Ok(Response::Joined(player)) => {
                self.other_players.insert(player.id, player);
                true
            }
            Ok(Response::Updated(player)) => {
                self.other_players.insert(player.id, player);
                true
            }
            Ok(Response::Removed(player_id)) => {
                self.other_players.remove(&player_id);
                true
            }
            Err(TryRecvError::Disconnected) => {
                panic!("Server is disconnected, no reason to exist anymore");
            }
            _ => {
                // do nothing on every other message, or no message
                false
            }
        }
    }

    fn move_player(&mut self, wuc: &WindowUpdateContext) -> bool {
        self.player.velocity = vec2(0.0, 0.0);
        if *wuc
            .keys_pressed
            .get(&VirtualKeyCode::Left)
            .unwrap_or(&false)
        {
            self.player.velocity.x -= 1.0;
        }
        if *wuc
            .keys_pressed
            .get(&VirtualKeyCode::Right)
            .unwrap_or(&false)
        {
            self.player.velocity.x += 1.0;
        }
        if *wuc.keys_pressed.get(&VirtualKeyCode::Up).unwrap_or(&false) {
            self.player.velocity.y += 1.0;
        }
        if *wuc
            .keys_pressed
            .get(&VirtualKeyCode::Down)
            .unwrap_or(&false)
        {
            self.player.velocity.y -= 1.0;
        }

        let mut moved = false;
        if self.player.velocity.magnitude2() > 0.0 {
            moved = true;
            self.player.velocity = self.player.velocity.normalize() * 2.0;
            self.player.position += self.player.velocity * wuc.delta_time;
        }

        moved
    }
}

impl Gui for Demo {
    fn init(&mut self, gpu: &Gpu, _window: &Window) -> Init {
        let pipeline = InitPipeline {
            pipeline_name: "net".to_string(),
            shader_name: "net.wgsl".to_string(),
            model: Some(self.create_model(gpu)),
            ..Default::default()
        };

        let scene = InitScene {
            name: "net".to_string(),
            render_pipelines: vec![pipeline],
            ..Default::default()
        };

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        let other_moved = self.from_net();

        let me_moved = self.move_player(wuc);

        if me_moved || other_moved {
            // send only players that moved to gpu
            self.to_gpu(gpu, renderer);
        }

        if me_moved {
            self.to_net();
        }
    }
}
