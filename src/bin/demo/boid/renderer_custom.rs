// https://github.com/gfx-rs/wgpu/tree/trunk/examples/boids

use nanorand::{Rng, WyRand};
use playground_wgpu::*;
use wgpu::util::DeviceExt;

pub struct RendererCustom {
    particle_bind_groups: Vec<wgpu::BindGroup>,
    particle_buffers: Vec<wgpu::Buffer>,
    vertices_buffer: wgpu::Buffer,
    compute_pipeline: wgpu::ComputePipeline,
    render_pipeline: wgpu::RenderPipeline,
    work_group_count: u32,
    frame_num: usize,
    num_particles: u32,
    // TODO:
    scene: Scene,
}

impl RendererCustom {
    pub fn new(
        gpu: &Gpu,
        num_particles: u32,
        particles_per_group: u32,
        sim_data: &[f32],
        vertex_data: &[f32],
    ) -> Self {
        let compute_bg_layout =
            create_compute_bg_layout(gpu.device(), sim_data.len(), num_particles);

        let compute_pipeline_layout =
            factory::create_pipeline_layout("compute", gpu.device(), &[&compute_bg_layout]);

        let compute_pipeline =
            factory::create_compute_pipeline(gpu.device(), &compute_pipeline_layout, "boid.wgsl");

        let render_pipeline_layout = factory::create_pipeline_layout("render", gpu.device(), &[]);

        let render_pipeline = create_render_pipeline(
            gpu.device(),
            &render_pipeline_layout,
            gpu.config().format,
            "boid.wgsl",
        );

        let sim_buffer = gpu
            .device()
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Simulation Parameter Buffer"),
                contents: bytemuck::cast_slice(sim_data),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

        let vertices_buffer = gpu
            .device()
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(vertex_data),
                usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
            });

        let particle_data = create_particle_data(num_particles);
        // creates two buffers of particle data each of size NUM_PARTICLES
        // the two buffers alternate as dst and src for each frame
        let mut particle_buffers = Vec::<wgpu::Buffer>::new();
        for i in 0..2 {
            particle_buffers.push(gpu.device().create_buffer_init(
                &wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("Particle Buffer {i}")),
                    contents: bytemuck::cast_slice(&particle_data),
                    usage: wgpu::BufferUsages::VERTEX
                        | wgpu::BufferUsages::STORAGE
                        | wgpu::BufferUsages::COPY_DST,
                },
            ));
        }

        // create two bind groups, one for each buffer as the src
        // where the alternate buffer is used as the dst
        let mut particle_bind_groups = Vec::<wgpu::BindGroup>::new();
        for i in 0..2 {
            particle_bind_groups.push(gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &compute_bg_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: sim_buffer.as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: particle_buffers[i].as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: particle_buffers[(i + 1) % 2].as_entire_binding(), // bind to opposite buffer
                    },
                ],
                label: None,
            }));
        }

        // calculates number of work groups from PARTICLES_PER_GROUP constant
        let work_group_count =
            ((num_particles as f32) / (particles_per_group as f32)).ceil() as u32;

        Self {
            particle_bind_groups,
            particle_buffers,
            vertices_buffer,
            compute_pipeline,
            render_pipeline,
            work_group_count,
            frame_num: 0,
            num_particles,
            scene: Scene::default(),
        }
    }
}

impl Renderer for RendererCustom {
    fn show(
        &mut self,
        gpu: &Gpu,
        _window: &Window,
        _depth_texture: Option<&Texture>,
        _egui: Option<&mut EguiContext>,
    ) {
        let output = gpu.surface().get_current_texture().unwrap();
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let color_attachments = [Some(wgpu::RenderPassColorAttachment {
            view: &view,
            resolve_target: None,
            ops: wgpu::Operations {
                // Not clearing here in order to test wgpu's zero texture
                // initialization on a surface texture.
                load: wgpu::LoadOp::Load,
                store: true,
            },
        })];

        let render_pass_descriptor = wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &color_attachments,
            depth_stencil_attachment: None,
        };

        let mut command_encoder = gpu
            .device()
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        command_encoder.push_debug_group("compute boid movement");
        {
            let mut compute_pass =
                command_encoder.begin_compute_pass(&wgpu::ComputePassDescriptor { label: None });
            compute_pass.set_pipeline(&self.compute_pipeline);
            compute_pass.set_bind_group(0, &self.particle_bind_groups[self.frame_num % 2], &[]);
            compute_pass.dispatch_workgroups(self.work_group_count, 1, 1);
        }
        command_encoder.pop_debug_group();

        command_encoder.push_debug_group("render boids");
        {
            let mut render_pass = command_encoder.begin_render_pass(&render_pass_descriptor);
            render_pass.set_pipeline(&self.render_pipeline);
            render_pass
                .set_vertex_buffer(0, self.particle_buffers[(self.frame_num + 1) % 2].slice(..));
            render_pass.set_vertex_buffer(1, self.vertices_buffer.slice(..));
            render_pass.draw(0..3, 0..self.num_particles);
        }
        command_encoder.pop_debug_group();

        self.frame_num += 1;

        gpu.queue()
            .submit(std::iter::once(command_encoder.finish()));
        output.present();
    }

    fn resize(&mut self, _width: u32, _height: u32) {}

    fn active_scene(&self) -> &Scene {
        &self.scene
    }

    fn active_scene_mut(&mut self) -> &mut Scene {
        &mut self.scene
    }

    fn set_active_scene(&mut self, _idx: usize) {}
}

fn create_compute_bg_layout(
    device: &wgpu::Device,
    sim_len: usize,
    num_particles: u32,
) -> wgpu::BindGroupLayout {
    device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        entries: &[
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: wgpu::BufferSize::new(
                        (sim_len * std::mem::size_of::<f32>()) as _,
                    ),
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Storage { read_only: true },
                    has_dynamic_offset: false,
                    min_binding_size: wgpu::BufferSize::new((num_particles * 16) as _),
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 2,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Storage { read_only: false },
                    has_dynamic_offset: false,
                    min_binding_size: wgpu::BufferSize::new((num_particles * 16) as _),
                },
                count: None,
            },
        ],
        label: None,
    })
}

fn create_render_pipeline(
    device: &wgpu::Device,
    layout: &wgpu::PipelineLayout,
    color_format: wgpu::TextureFormat,
    shader_name: &str,
) -> wgpu::RenderPipeline {
    let shader = factory::create_shader(device, shader_name);

    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: None,
        layout: Some(layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[
                wgpu::VertexBufferLayout {
                    array_stride: 4 * 4,
                    step_mode: wgpu::VertexStepMode::Instance,
                    attributes: &wgpu::vertex_attr_array![0 => Float32x2, 1 => Float32x2],
                },
                wgpu::VertexBufferLayout {
                    array_stride: 2 * 4,
                    step_mode: wgpu::VertexStepMode::Vertex,
                    attributes: &wgpu::vertex_attr_array![2 => Float32x2],
                },
            ],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(wgpu::ColorTargetState {
                format: color_format,
                blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    })
}

fn create_particle_data(num_particles: u32) -> Vec<f32> {
    let mut result = vec![0.0; (4 * num_particles) as usize];
    // Generate a num (-1, 1)
    let mut rng = WyRand::new_seed(42);
    let mut unif = || rng.generate::<f32>() * 2f32 - 1f32;

    for particle_instance_chunk in result.chunks_mut(4) {
        particle_instance_chunk[0] = unif(); // posx
        particle_instance_chunk[1] = unif(); // posy
        particle_instance_chunk[2] = unif() * 0.1; // velx
        particle_instance_chunk[3] = unif() * 0.1; // vely
    }

    result
}
