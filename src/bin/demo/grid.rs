// Circle Vs Rectangle Collisions: https://www.youtube.com/watch?v=D2a5fHX-Qrs

use cgmath::*;
use playground_wgpu::*;
use winit::event::VirtualKeyCode;

const WORLD_SIZE: f32 = 40.0;
const TILE_SIZE: f32 = 1.0;
const NUM_CELLS: u16 = 10;
const BRICKS: &str = "\
@@@@@@@@@@
@        @
@        @
@ @  @   @
@  @  @  @
@   @    @
@     @  @
@    @   @
@        @
@@@@@@@@@@
";

struct Circle {
    center: Vector2<f32>,
    radius: f32,
    vel: Vector2<f32>,
    from_grid_center: Vector2<f32>,
}

impl Circle {
    fn new(grid_center: Vector2<f32>) -> Self {
        let center = vec2(
            grid_center.x - (NUM_CELLS as f32 - 3.0) * TILE_SIZE * 0.5,
            grid_center.y + (NUM_CELLS as f32 - 3.0) * TILE_SIZE * 0.5,
        );
        let from_grid_center = vec2(center.x, center.y) - grid_center;
        Self {
            center,
            radius: 0.5 * TILE_SIZE,
            vel: vec2(0.0, 0.0),
            from_grid_center,
        }
    }

    fn update(&mut self, grid_center: Vector2<f32>, scale: f32) {
        self.center = vec2(
            grid_center.x + self.from_grid_center.x * scale,
            grid_center.y + self.from_grid_center.y * scale,
        );
        self.radius = scale * 0.5 * TILE_SIZE;
    }

    fn potential_position(
        &mut self,
        grid_center: Vector2<f32>,
        scale: f32,
        elapsed_time: f32,
        bricks: &[Brick],
    ) {
        let mut potential_pos = vec2(
            self.center.x + self.vel.x * elapsed_time,
            self.center.y + self.vel.y * elapsed_time,
        );

        // region that could collide this frame with the circle
        let area_min = vec2(
            f32::min(self.center.x, potential_pos.x),
            f32::min(self.center.y, potential_pos.y),
        ) - vec2(self.radius, self.radius);

        let area_max = vec2(
            f32::max(self.center.x, potential_pos.x),
            f32::max(self.center.y, potential_pos.y),
        ) + vec2(self.radius, self.radius);

        let area_width = area_max.x - area_min.x;
        let area_height = area_max.y - area_min.y;
        let area = collision::Aabb {
            bottom_left: vec2(
                self.center.x - area_width / 2.0,
                self.center.y - area_height / 2.0,
            ),
            top_right: vec2(
                self.center.x + area_width / 2.0,
                self.center.y + area_height / 2.0,
            ),
        };

        for brick in bricks.iter() {
            if collision::aabb_in_aabb(&brick.aabb(), &area) {
                collision::circle_aabb_resolution(&mut potential_pos, self.radius, &brick.aabb());
            }
        }

        self.center = potential_pos;
        self.from_grid_center = (self.center - grid_center) / scale;
    }
}

struct Grid {
    center: Vector2<f32>,
    cell_size: f32,
}

impl Grid {
    fn update(&mut self, grid_translate: Vector2<f32>, scale: f32) {
        self.center += grid_translate;
        self.cell_size = TILE_SIZE * scale;
    }

    fn model(&mut self, gpu: &Gpu) -> Model {
        shape::grid(gpu, self.center, NUM_CELLS, self.cell_size)
    }
}

struct Brick {
    center: Vector2<f32>,
    size: f32,
    from_grid_center: Vector2<f32>,
}

impl Brick {
    fn aabb(&self) -> collision::Aabb {
        collision::Aabb {
            bottom_left: vec2(
                self.center.x - self.size / 2.0,
                self.center.y - self.size / 2.0,
            ),
            top_right: vec2(
                self.center.x + self.size / 2.0,
                self.center.y + self.size / 2.0,
            ),
        }
    }

    fn update(&mut self, grid_center: Vector2<f32>, scale: f32) {
        self.center = vec2(
            grid_center.x + self.from_grid_center.x * scale,
            grid_center.y + self.from_grid_center.y * scale,
        );
        self.size = TILE_SIZE * scale;
    }
}

pub struct Demo {
    scale: f32,
    start_pan: Vector2<f32>,
    grid: Grid,
    circle: Circle,
    bricks: Vec<Brick>,
}

impl Default for Demo {
    fn default() -> Self {
        let grid_center = vec2(0.5, 0.5) * WORLD_SIZE;
        Self {
            scale: 1.0,
            start_pan: vec2(0.0, 0.0),
            grid: Grid {
                center: grid_center,
                cell_size: TILE_SIZE,
            },
            circle: Circle::new(grid_center),
            bricks: init_bricks(grid_center),
        }
    }
}

impl Gui for Demo {
    fn win_resizable(&self) -> bool {
        false
    }

    fn init(&mut self, gpu: &Gpu, window: &Window) -> Init {
        let grid_model = shape::grid(gpu, self.grid.center, NUM_CELLS, self.grid.cell_size);
        let pipeline_grid = InitPipeline {
            pipeline_name: "grid".to_string(),
            shader_name: "default_instances.wgsl".to_string(),
            model: Some(grid_model),
            ..Default::default()
        };

        let bricks_model = self.bricks_model(gpu);
        let pipeline_bricks = InitPipeline {
            pipeline_name: "bricks".to_string(),
            shader_name: "default_instances.wgsl".to_string(),
            model: Some(bricks_model),
            ..Default::default()
        };

        let camera = Camera::new_ortho_size(
            vec3(0.0, 0.0, 0.0),
            window.wuc.size.width as f32,
            window.wuc.size.height as f32,
            WORLD_SIZE,
        );

        let mut background = shape::rectangle_transparent();
        background.transform(cgmath::Matrix4::from_scale(2.0 * WORLD_SIZE));
        let background_model = resource::shape_to_model(background, gpu.device(), gpu.queue());

        let pipeline_circle = InitPipeline {
            pipeline_name: "circle".to_string(),
            shader_name: "grid.wgsl".to_string(),
            model: Some(background_model),
            uniforms: vec![self.create_uniform(&window.wuc)],
            ..Default::default()
        };

        let scene = InitScene {
            name: "scene".to_string(),
            render_pipelines: vec![pipeline_grid, pipeline_bricks, pipeline_circle],
            camera: Some(camera),
            ..Default::default()
        };

        Init {
            scenes: vec![scene],
            ..Default::default()
        }
    }

    fn each_frame(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        wuc: &mut WindowUpdateContext,
    ) {
        let aspect = wuc.size.width as f32 / wuc.size.height as f32;

        // mouse center is top_left, so move it to bottom_left
        let mouse_pos = wuc.mouse_world(WORLD_SIZE, aspect);

        if wuc.is_mouse_pressed(0) {
            self.start_pan = mouse_pos;
        }
        if wuc.mouse_buttons[0] {
            let grid_translate = mouse_pos - self.start_pan;
            self.zoom(gpu, renderer, grid_translate, wuc);
            self.start_pan = mouse_pos;
        }

        if *wuc.keys_pressed.get(&VirtualKeyCode::Q).unwrap_or(&false) {
            self.scale = 5.0_f32.min(self.scale + 0.1);
            let grid_translate = mouse_pos - self.grid.center;
            self.zoom(gpu, renderer, grid_translate, wuc);
        }
        if *wuc.keys_pressed.get(&VirtualKeyCode::A).unwrap_or(&false) {
            self.scale = 0.5_f32.max(self.scale - 0.1);
            let grid_translate = mouse_pos - self.grid.center;
            self.zoom(gpu, renderer, grid_translate, wuc);
        }

        self.circle.vel = vec2(0.0, 0.0);
        if *wuc
            .keys_pressed
            .get(&VirtualKeyCode::Left)
            .unwrap_or(&false)
        {
            self.circle.vel.x -= 1.0;
        }
        if *wuc
            .keys_pressed
            .get(&VirtualKeyCode::Right)
            .unwrap_or(&false)
        {
            self.circle.vel.x += 1.0;
        }
        if *wuc.keys_pressed.get(&VirtualKeyCode::Up).unwrap_or(&false) {
            self.circle.vel.y += 1.0;
        }
        if *wuc
            .keys_pressed
            .get(&VirtualKeyCode::Down)
            .unwrap_or(&false)
        {
            self.circle.vel.y -= 1.0;
        }
        if self.circle.vel.magnitude2() > 0.0 {
            self.circle.vel = self.circle.vel.normalize() * self.scale * 10.0;
            self.circle.potential_position(
                self.grid.center,
                self.scale,
                wuc.delta_time,
                &self.bricks,
            );
        }
        self.circle_to_gpu(gpu, renderer, wuc);
    }
}

impl Demo {
    fn circle_to_gpu(&self, gpu: &Gpu, renderer: &mut dyn Renderer, wuc: &WindowUpdateContext) {
        let uniform = self.create_uniform(wuc);
        renderer
            .active_scene_mut()
            .set_uniform_data("circle", gpu.queue(), uniform.as_slice());
    }

    fn zoom(
        &mut self,
        gpu: &Gpu,
        renderer: &mut dyn Renderer,
        grid_translate: Vector2<f32>,
        wuc: &WindowUpdateContext,
    ) {
        self.grid.update(grid_translate, self.scale);
        let grid_model = self.grid.model(gpu);
        renderer.active_scene_mut().set_model("grid", grid_model);

        for brick in self.bricks.iter_mut() {
            brick.update(self.grid.center, self.scale);
        }
        let bricks_model = self.bricks_model(gpu);
        renderer
            .active_scene_mut()
            .set_model("bricks", bricks_model);

        self.circle.update(self.grid.center, self.scale);
        self.circle_to_gpu(gpu, renderer, wuc);
    }

    fn bricks_model(&self, gpu: &Gpu) -> Model {
        let mut instances = Vec::new();
        for brick in self.bricks.iter() {
            let fgc = brick.from_grid_center * self.scale;
            let translate = vec3(fgc.x, fgc.y, 0.0);
            instances.push(Instance { translate });
        }

        let first = self.bricks.first().unwrap();
        let brick = shape::rectangle(
            self.grid.center,
            first.size,
            first.size,
            [0.8, 0.3, 0.2, 1.0],
        );
        let mut result = resource::shape_to_model(brick, gpu.device(), gpu.queue());
        result.add_instances(gpu.device(), instances);
        result
    }

    fn create_uniform(&self, wuc: &WindowUpdateContext) -> InitBuffer {
        // not sure this is the correct way: (we are ignoring the camera)
        // keep it normalized so we can draw it in shader in normalized space
        let aspect = wuc.size.width as f32 / wuc.size.height as f32;
        let data = vec![
            self.circle.center.x / (aspect * WORLD_SIZE),
            self.circle.center.y / WORLD_SIZE,
            self.circle.radius / (aspect * WORLD_SIZE),
            wuc.size.width as f32,
            wuc.size.height as f32,
        ];

        InitBuffer::new(data)
    }
}

fn init_bricks(grid_center: Vector2<f32>) -> Vec<Brick> {
    let mut result = Vec::new();

    let tl = vec2(
        grid_center.x - NUM_CELLS as f32 * 0.5 * TILE_SIZE,
        grid_center.y + NUM_CELLS as f32 * 0.5 * TILE_SIZE,
    );

    for (line_idx, line) in BRICKS.lines().enumerate() {
        for (char_idx, char) in line.bytes().enumerate() {
            if char == b'@' {
                let x = tl.x + char_idx as f32 * TILE_SIZE + 0.5 * TILE_SIZE;
                let y = tl.y - line_idx as f32 * TILE_SIZE - 0.5 * TILE_SIZE;
                let brick_center = vec2(x, y);
                let from_grid_center = brick_center - grid_center;
                result.push(Brick {
                    center: brick_center,
                    size: TILE_SIZE,
                    from_grid_center,
                });
            }
        }
    }

    result
}
