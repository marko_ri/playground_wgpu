use nanorand::{Rng, WyRand};
use playground_wgpu::*;
use wgpu::util::DeviceExt;

pub struct RendererCustom {
    circle_bg: Vec<wgpu::BindGroup>,
    circle_buffers: Vec<wgpu::Buffer>,
    vertex_buffer: wgpu::Buffer,
    compute_pipeline: wgpu::ComputePipeline,
    render_pipeline: wgpu::RenderPipeline,
    work_group_count: u32,
    frame_num: usize,
    num_circles: u32,
    num_vertices: u32,
    // TODO:
    scene: Scene,
}

impl RendererCustom {
    pub fn new(
        gpu: &Gpu,
        num_circles: u32,
        circles_per_group: u32,
        params: &[f32],
        circle: &[[f32; 2]],
        radius: f32,
    ) -> Self {
        let compute_bg_layout = create_compute_bg_layout(gpu.device(), params.len(), num_circles);

        let compute_pipeline_layout =
            factory::create_pipeline_layout("compute", gpu.device(), &[&compute_bg_layout]);

        let compute_pipeline =
            factory::create_compute_pipeline(gpu.device(), &compute_pipeline_layout, "bouncy.wgsl");

        let render_pipeline_layout = factory::create_pipeline_layout("render", gpu.device(), &[]);

        let render_pipeline = create_render_pipeline(
            gpu.device(),
            &render_pipeline_layout,
            gpu.config().format,
            "bouncy.wgsl",
        );

        let params_buffer = gpu
            .device()
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Params Buffer"),
                contents: bytemuck::cast_slice(params),
                usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            });

        let vertex_buffer = gpu
            .device()
            .create_buffer_init(&wgpu::util::BufferInitDescriptor {
                label: Some("Vertex Buffer"),
                contents: bytemuck::cast_slice(circle),
                usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
            });

        let circle_data = create_circle_data(num_circles, radius);
        let mut circle_buffers = Vec::<wgpu::Buffer>::new();
        for i in 0..2 {
            circle_buffers.push(gpu.device().create_buffer_init(
                &wgpu::util::BufferInitDescriptor {
                    label: Some(&format!("Circle Buffer {i}")),
                    contents: bytemuck::cast_slice(&circle_data),
                    usage: wgpu::BufferUsages::VERTEX
                        | wgpu::BufferUsages::STORAGE
                        | wgpu::BufferUsages::COPY_DST,
                },
            ));
        }

        let mut circle_bg = Vec::<wgpu::BindGroup>::new();
        for i in 0..2 {
            circle_bg.push(gpu.device().create_bind_group(&wgpu::BindGroupDescriptor {
                layout: &compute_bg_layout,
                entries: &[
                    wgpu::BindGroupEntry {
                        binding: 0,
                        resource: params_buffer.as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 1,
                        resource: circle_buffers[i].as_entire_binding(),
                    },
                    wgpu::BindGroupEntry {
                        binding: 2,
                        resource: circle_buffers[(i + 1) % 2].as_entire_binding(),
                    },
                ],
                label: None,
            }));
        }

        let work_group_count = ((num_circles as f32) / (circles_per_group as f32)).ceil() as u32;

        Self {
            circle_bg,
            circle_buffers,
            vertex_buffer,
            compute_pipeline,
            render_pipeline,
            work_group_count,
            frame_num: 0,
            num_circles,
            num_vertices: circle.len() as u32,
            scene: Scene::default(),
        }
    }
}

impl Renderer for RendererCustom {
    fn show(
        &mut self,
        gpu: &Gpu,
        _window: &Window,
        _depth_texture: Option<&Texture>,
        _egui: Option<&mut EguiContext>,
    ) {
        let output = gpu.surface().get_current_texture().unwrap();
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());

        let color_attachments = [Some(wgpu::RenderPassColorAttachment {
            view: &view,
            resolve_target: None,
            ops: wgpu::Operations {
                load: wgpu::LoadOp::Load,
                //load: wgpu::LoadOp::Clear(wgpu::Color::BLACK),
                store: true,
            },
        })];

        let render_pass_descriptor = wgpu::RenderPassDescriptor {
            label: None,
            color_attachments: &color_attachments,
            depth_stencil_attachment: None,
        };

        let mut command_encoder = gpu
            .device()
            .create_command_encoder(&wgpu::CommandEncoderDescriptor { label: None });

        command_encoder.push_debug_group("compute circle movement");
        {
            let mut compute_pass =
                command_encoder.begin_compute_pass(&wgpu::ComputePassDescriptor { label: None });
            compute_pass.set_pipeline(&self.compute_pipeline);
            compute_pass.set_bind_group(0, &self.circle_bg[self.frame_num % 2], &[]);
            compute_pass.dispatch_workgroups(self.work_group_count, 1, 1);
        }
        command_encoder.pop_debug_group();

        command_encoder.push_debug_group("render circles");
        {
            let mut render_pass = command_encoder.begin_render_pass(&render_pass_descriptor);
            render_pass.set_pipeline(&self.render_pipeline);
            render_pass
                .set_vertex_buffer(0, self.circle_buffers[(self.frame_num + 1) % 2].slice(..));
            render_pass.set_vertex_buffer(1, self.vertex_buffer.slice(..));
            render_pass.draw(0..self.num_vertices, 0..self.num_circles);
        }
        command_encoder.pop_debug_group();

        self.frame_num += 1;

        gpu.queue()
            .submit(std::iter::once(command_encoder.finish()));
        output.present();
    }

    fn resize(&mut self, _width: u32, _height: u32) {}

    fn active_scene(&self) -> &Scene {
        &self.scene
    }

    fn active_scene_mut(&mut self) -> &mut Scene {
        &mut self.scene
    }

    fn set_active_scene(&mut self, _idx: usize) {}
}

fn create_compute_bg_layout(
    device: &wgpu::Device,
    params_len: usize,
    num_circles: u32,
) -> wgpu::BindGroupLayout {
    device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
        entries: &[
            wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: wgpu::BufferSize::new(
                        (params_len * std::mem::size_of::<f32>()) as _,
                    ),
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 1,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Storage { read_only: true },
                    has_dynamic_offset: false,
                    min_binding_size: wgpu::BufferSize::new((num_circles * 16) as _),
                },
                count: None,
            },
            wgpu::BindGroupLayoutEntry {
                binding: 2,
                visibility: wgpu::ShaderStages::COMPUTE,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Storage { read_only: false },
                    has_dynamic_offset: false,
                    min_binding_size: wgpu::BufferSize::new((num_circles * 16) as _),
                },
                count: None,
            },
        ],
        label: None,
    })
}

fn create_render_pipeline(
    device: &wgpu::Device,
    layout: &wgpu::PipelineLayout,
    color_format: wgpu::TextureFormat,
    shader_name: &str,
) -> wgpu::RenderPipeline {
    let shader = factory::create_shader(device, shader_name);

    device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
        label: Some("Render Pipeline"),
        layout: Some(layout),
        vertex: wgpu::VertexState {
            module: &shader,
            entry_point: "vs_main",
            buffers: &[
                wgpu::VertexBufferLayout {
                    array_stride: 4 * 4,
                    step_mode: wgpu::VertexStepMode::Instance,
                    attributes: &wgpu::vertex_attr_array![
                        0 => Float32x2, 1 => Float32x2],
                },
                wgpu::VertexBufferLayout {
                    array_stride: 2 * 4,
                    step_mode: wgpu::VertexStepMode::Vertex,
                    attributes: &wgpu::vertex_attr_array![2 => Float32x2],
                },
            ],
        },
        fragment: Some(wgpu::FragmentState {
            module: &shader,
            entry_point: "fs_main",
            targets: &[Some(wgpu::ColorTargetState {
                format: color_format,
                blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                write_mask: wgpu::ColorWrites::ALL,
            })],
        }),
        primitive: wgpu::PrimitiveState {
            topology: wgpu::PrimitiveTopology::LineStrip,
            strip_index_format: Some(wgpu::IndexFormat::Uint32),
            ..Default::default()
        },
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
    })
}

fn create_circle_data(num_circles: u32, radius: f32) -> Vec<f32> {
    let mut rng = WyRand::new_seed(42);
    let r = (100.0 * radius).floor() as i32;
    let mut unif = || {
        let range = rng.generate_range((-100 + r)..(100 - r));
        range as f32 / 100.0
    };

    struct Data {
        x: f32,
        y: f32,
        vel_x: f32,
        vel_y: f32,
    }

    impl Data {
        fn contains(&self, x: f32, y: f32, radius: f32) -> bool {
            (x - self.x) * (x - self.x) + (y - self.y) * (y - self.y) <= radius * radius
        }
    }

    let mut data: Vec<Data> = Vec::new();
    let max = 1_000;
    for _ in 0..num_circles {
        let mut i = 0;
        let (mut x, mut y) = (unif(), unif());
        while data.iter().any(|d| d.contains(x, y, radius)) {
            (x, y) = (unif(), unif());
            i += 1;
            if i == max {
                panic!("max x_loop exceeded")
            }
        }
        let vel_x = unif();
        let vel_y = unif();
        data.push(Data { x, y, vel_x, vel_y });
    }

    let mut result = Vec::new();
    for d in data.into_iter() {
        result.push(d.x);
        result.push(d.y);
        result.push(d.vel_x);
        result.push(d.vel_y);
    }
    result
}
